[@tofurama3000/hat-js](README.md) / Exports

# @tofurama3000/hat-js

## Table of contents

### Namespaces

- [arrays](modules/arrays.md)
- [fp](modules/fp.md)
- [imut](modules/imut.md)
- [is](modules/is.md)
- [iterables](modules/iterables.md)
- [logic](modules/logic.md)
- [obj](modules/obj.md)
- [to](modules/to.md)
- [utils](modules/utils.md)
