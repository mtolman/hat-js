[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / utils

# Namespace: utils

Misc utility functions

## Table of contents

### Functions

- [cloneDeep](utils.md#clonedeep)
- [cloneShallow](utils.md#cloneshallow)
- [lazyRange](utils.md#lazyrange)
- [range](utils.md#range)

## Functions

### cloneDeep

▸ **cloneDeep**<`T`\>(`toClone`): `T`

Performs a deep clone
Deep clones will also clone any nested objects, arrays, etc

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `toClone` | `T` | what should be cloned |

#### Returns

`T`

cloned object

#### Defined in

[utils/clone.ts:37](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/utils/clone.ts#L37)

___

### cloneShallow

▸ **cloneShallow**<`T`\>(`toClone`): `T`

Performs a shallow clone
Shallow clones don't copy any nested objects, arrays, etc

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `toClone` | `T` | what should be cloned |

#### Returns

`T`

cloned object

#### Defined in

[utils/clone.ts:21](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/utils/clone.ts#L21)

___

### lazyRange

▸ **lazyRange**(`start?`, `end?`, `step?`): `Iterable`<`number`\>

Generates a range of numbers lazily (can be infinite range)

Note: if start is provided but end is not then it will act as if starting at 0 and ending at the provided number
E.g. lazyRange(4) is the same as lazyRange(0, 4)

Note: if start > end then it will step down from start to end

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `start` | `number` | `Infinity` | (Optional, defaults to 0) the starting number for the range |
| `end` | `any` | `null` | (Optional, defaults to Infinity) the ending number for the range |
| `step` | `number` | `1` | (optional, defaults to 1) the step size to take for the range |

#### Returns

`Iterable`<`number`\>

iterable for the range to be generated

#### Defined in

[utils/lazyRange.ts:22](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/utils/lazyRange.ts#L22)

___

### range

▸ **range**(`start?`, `end?`, `step?`): `number`[]

Generates a range of numbers and returns them as an array

Note: if start is provided but end is not then it will act as if starting at 0 and ending at the provided number
E.g. range(4) is the same as range(0, 4)

Note: if start > end then it will step down from start to end

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `start` | `number` | `0` | (Optional, defaults to 0) the starting number for the range |
| `end` | `any` | `undefined` | the ending number for the range |
| `step` | `number` | `1` | (optional, defaults to 1) the step size to take for the range |

#### Returns

`number`[]

number array for the range

#### Defined in

[utils/range.ts:21](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/utils/range.ts#L21)
