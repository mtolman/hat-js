[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / obj

# Namespace: obj

Functions for operating on objects

## Table of contents

### Interfaces

- [Diff](../interfaces/obj.Diff.md)
- [iCopyInto](../interfaces/obj.iCopyInto.md)
- [iGet](../interfaces/obj.iGet.md)
- [iGetOr](../interfaces/obj.iGetOr.md)
- [iGetPath](../interfaces/obj.iGetPath.md)
- [iGetPathOr](../interfaces/obj.iGetPathOr.md)
- [iSet](../interfaces/obj.iSet.md)
- [iSetPath](../interfaces/obj.iSetPath.md)

### Variables

- [removedDiff](obj.md#removeddiff)

### Functions

- [copyInto](obj.md#copyinto)
- [copyIntoDeep](obj.md#copyintodeep)
- [diff](obj.md#diff)
- [entries](obj.md#entries)
- [get](obj.md#get)
- [getIn](obj.md#getin)
- [getInOr](obj.md#getinor)
- [getOr](obj.md#getor)
- [getPath](obj.md#getpath)
- [getPathOr](obj.md#getpathor)
- [havePathsInCommon](obj.md#havepathsincommon)
- [keys](obj.md#keys)
- [paths](obj.md#paths)
- [set](obj.md#set)
- [setIn](obj.md#setin)
- [setInIfPresent](obj.md#setinifpresent)
- [setPath](obj.md#setpath)
- [setPathIfPresent](obj.md#setpathifpresent)
- [toPairs](obj.md#topairs)
- [values](obj.md#values)

## Variables

### removedDiff

• `Const` **removedDiff**: typeof [`removedDiff`](obj.md#removeddiff)

Symbol indicating that a key was removed

#### Defined in

[obj/diff.ts:18](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/diff.ts#L18)

## Functions

### copyInto

▸ **copyInto**(`source`, `target`): `Object`

Copies attributes from source into a target

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `source` | `Object` | The object to copy attributes from |
| `target` | `Object` | The object to copy attributes into |

#### Returns

`Object`

#### Defined in

[obj/copyInto.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/copyInto.ts#L10)

▸ **copyInto**(`source`): (`target`: `Object`) => `Object`

Copies attributes from source into a target

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `source` | `Object` | The object to copy attributes from |

#### Returns

`fn`

▸ (`target`): `Object`

##### Parameters

| Name | Type |
| :------ | :------ |
| `target` | `Object` |

##### Returns

`Object`

#### Defined in

[obj/copyInto.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/copyInto.ts#L11)

___

### copyIntoDeep

▸ **copyIntoDeep**(`source`, `target`): `Object`

Copies attributes from source into a target
Values are deeply cloned when copied

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `source` | `Object` | The object to copy attributes from |
| `target` | `Object` | The object to copy attributes into |

#### Returns

`Object`

#### Defined in

[obj/copyInto.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/copyInto.ts#L10)

▸ **copyIntoDeep**(`source`): (`target`: `Object`) => `Object`

Copies attributes from source into a target
Values are deeply cloned when copied

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `source` | `Object` | The object to copy attributes from |

#### Returns

`fn`

▸ (`target`): `Object`

##### Parameters

| Name | Type |
| :------ | :------ |
| `target` | `Object` |

##### Returns

`Object`

#### Defined in

[obj/copyInto.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/copyInto.ts#L11)

___

### diff

▸ **diff**(`orig`, `changed`): [`Diff`](../interfaces/obj.Diff.md)

Calculates the structural difference between two objects
For removed keys, will return symbol in removedDiff (@see removedDiff)

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `orig` | `Object` \| `any`[] | Original object |
| `changed` | `Object` \| `any`[] | Changed object |

#### Returns

[`Diff`](../interfaces/obj.Diff.md)

#### Defined in

[obj/diff.ts:41](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/diff.ts#L41)

___

### entries

▸ **entries**(`param`): `any`[]

Grabs entries for an object (for both string and symbol properties)

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `any` |

#### Returns

`any`[]

any[][]

#### Defined in

[obj/entries.ts:17](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/entries.ts#L17)

___

### get

▸ **get**(`path`, `obj`): `any`

Gets a value in an array or object. Returns null if the key is not present or the entity is not an object

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` \| `number` | - |
| `obj` | `any` | Object to grab a value out of |

#### Returns

`any`

#### Defined in

[obj/get.ts:25](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/get.ts#L25)

▸ **get**(`path`): (`obj`: `any`) => `any`

Gets a value in an array or object. Returns null if the key is not present or the entity is not an object

#### Parameters

| Name | Type |
| :------ | :------ |
| `path` | `string` \| `number` |

#### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/get.ts:26](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/get.ts#L26)

___

### getIn

▸ **getIn**(`t1`): [`CF1`](../interfaces/fp.CF1.md)<`any`, `any`\>

Gets a value in an array or object. Does this by following a path and doing a null check at each segment.
Returns null if the key is not present or the path is invalid (e.g. not an object/array)

E.g.
getIn(['a', 'b', 1], {a: {b: [12, 5, 14]}}) will return 5

**`Deprecated`**

**`See`**

getPath

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `string` \| `number` \| `Iterable`<`string` \| `number`\> |

#### Returns

[`CF1`](../interfaces/fp.CF1.md)<`any`, `any`\>

#### Defined in

[fp/curry.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L10)

▸ **getIn**(`t1`, `t2`, ...`args`): `any`

Gets a value in an array or object. Does this by following a path and doing a null check at each segment.
Returns null if the key is not present or the path is invalid (e.g. not an object/array)

E.g.
getIn(['a', 'b', 1], {a: {b: [12, 5, 14]}}) will return 5

**`Deprecated`**

**`See`**

getPath

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `string` \| `number` \| `Iterable`<`string` \| `number`\> |
| `t2` | `any` |
| `...args` | `any`[] |

#### Returns

`any`

#### Defined in

[fp/curry.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L11)

___

### getInOr

▸ **getInOr**(`path`, `defaultValue`, `obj`): `any`

Gets a value in an array or object. Does this by following a path and doing a null check at each segment.
Returns the default value if the key is not present or the path is invalid (e.g. not an object/array)

E.g.
getInOr(['a', 'b', 1], {a: {b: [12, 5, 14]}}) will return 5

**`Deprecated`**

**`See`**

getPathOr

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | Path of keys to follow |
| `defaultValue` | `any` | Default value to return |
| `obj` | `any` | Object to grab a value out of |

#### Returns

`any`

#### Defined in

[obj/get.ts:37](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/get.ts#L37)

▸ **getInOr**(`path`, `defaultValue`): (`obj`: `any`) => `any`

Gets a value in an array or object. Does this by following a path and doing a null check at each segment.
Returns the default value if the key is not present or the path is invalid (e.g. not an object/array)

E.g.
getInOr(['a', 'b', 1], {a: {b: [12, 5, 14]}}) will return 5

**`Deprecated`**

**`See`**

getPathOr

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | Path of keys to follow |
| `defaultValue` | `any` | Default value to return |

#### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/get.ts:38](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/get.ts#L38)

▸ **getInOr**(`path`): (`defaultValue`: `any`, `obj`: `any`) => `any`

Gets a value in an array or object. Does this by following a path and doing a null check at each segment.
Returns the default value if the key is not present or the path is invalid (e.g. not an object/array)

E.g.
getInOr(['a', 'b', 1], {a: {b: [12, 5, 14]}}) will return 5

**`Deprecated`**

**`See`**

getPathOr

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | Path of keys to follow |

#### Returns

`fn`

▸ (`defaultValue`, `obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `defaultValue` | `any` |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/get.ts:41](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/get.ts#L41)

▸ **getInOr**(`path`): (`defaultValue`: `any`) => (`obj`: `any`) => `any`

Gets a value in an array or object. Does this by following a path and doing a null check at each segment.
Returns the default value if the key is not present or the path is invalid (e.g. not an object/array)

E.g.
getInOr(['a', 'b', 1], {a: {b: [12, 5, 14]}}) will return 5

**`Deprecated`**

**`See`**

getPathOr

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | Path of keys to follow |

#### Returns

`fn`

▸ (`defaultValue`): (`obj`: `any`) => `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `defaultValue` | `any` |

##### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/get.ts:45](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/get.ts#L45)

___

### getOr

▸ **getOr**(`path`, `defaultValue`, `obj`): `any`

Gets a value in an array or object. Returns the default value if the key is not present or the entity is not an object

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` \| `number` | - |
| `defaultValue` | `any` | Default value to return |
| `obj` | `any` | Object to grab a value out of |

#### Returns

`any`

#### Defined in

[obj/get.ts:6](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/get.ts#L6)

▸ **getOr**(`path`, `defaultValue`): (`obj`: `any`) => `any`

Gets a value in an array or object. Returns the default value if the key is not present or the entity is not an object

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` \| `number` | - |
| `defaultValue` | `any` | Default value to return |

#### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/get.ts:7](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/get.ts#L7)

▸ **getOr**(`path`): (`defaultValue`: `any`, `obj`: `any`) => `any`

Gets a value in an array or object. Returns the default value if the key is not present or the entity is not an object

#### Parameters

| Name | Type |
| :------ | :------ |
| `path` | `string` \| `number` |

#### Returns

`fn`

▸ (`defaultValue`, `obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `defaultValue` | `any` |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/get.ts:8](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/get.ts#L8)

▸ **getOr**(`path`): (`defaultValue`: `any`) => (`obj`: `any`) => `any`

Gets a value in an array or object. Returns the default value if the key is not present or the entity is not an object

#### Parameters

| Name | Type |
| :------ | :------ |
| `path` | `string` \| `number` |

#### Returns

`fn`

▸ (`defaultValue`): (`obj`: `any`) => `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `defaultValue` | `any` |

##### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/get.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/get.ts#L9)

___

### getPath

▸ **getPath**(`path`, `obj`): `any`

Gets a value in an array or object. Does this by following a path and doing a null check at each segment.
Returns undefined if the key is not present or the path is invalid (e.g. not an object/array)

E.g.
getIn(['a', 'b', 1], {a: {b: [12, 5, 14]}}) will return 5

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | Path of keys to follow |
| `obj` | `any` | Object to grab a value out of |

#### Returns

`any`

#### Defined in

[obj/get.ts:87](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/get.ts#L87)

▸ **getPath**(`path`): (`obj`: `any`) => `any`

Gets a value in an array or object. Does this by following a path and doing a null check at each segment.
Returns undefined if the key is not present or the path is invalid (e.g. not an object/array)

E.g.
getIn(['a', 'b', 1], {a: {b: [12, 5, 14]}}) will return 5

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | Path of keys to follow |

#### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/get.ts:88](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/get.ts#L88)

___

### getPathOr

▸ **getPathOr**(`path`, `defaultValue`, `obj`): `any`

Gets a value in an array or object. Does this by following a path and doing a null check at each segment.
Returns the default value if the key is not present or the path is invalid (e.g. not an object/array)

E.g.
getInOr(['a', 'b', 1], {a: {b: [12, 5, 14]}}) will return 5

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | Path of keys to follow |
| `defaultValue` | `any` | Default value to return |
| `obj` | `any` | Object to grab a value out of |

#### Returns

`any`

#### Defined in

[obj/get.ts:37](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/get.ts#L37)

▸ **getPathOr**(`path`, `defaultValue`): (`obj`: `any`) => `any`

Gets a value in an array or object. Does this by following a path and doing a null check at each segment.
Returns the default value if the key is not present or the path is invalid (e.g. not an object/array)

E.g.
getInOr(['a', 'b', 1], {a: {b: [12, 5, 14]}}) will return 5

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | Path of keys to follow |
| `defaultValue` | `any` | Default value to return |

#### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/get.ts:38](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/get.ts#L38)

▸ **getPathOr**(`path`): (`defaultValue`: `any`, `obj`: `any`) => `any`

Gets a value in an array or object. Does this by following a path and doing a null check at each segment.
Returns the default value if the key is not present or the path is invalid (e.g. not an object/array)

E.g.
getInOr(['a', 'b', 1], {a: {b: [12, 5, 14]}}) will return 5

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | Path of keys to follow |

#### Returns

`fn`

▸ (`defaultValue`, `obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `defaultValue` | `any` |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/get.ts:41](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/get.ts#L41)

▸ **getPathOr**(`path`): (`defaultValue`: `any`) => (`obj`: `any`) => `any`

Gets a value in an array or object. Does this by following a path and doing a null check at each segment.
Returns the default value if the key is not present or the path is invalid (e.g. not an object/array)

E.g.
getInOr(['a', 'b', 1], {a: {b: [12, 5, 14]}}) will return 5

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `path` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | Path of keys to follow |

#### Returns

`fn`

▸ (`defaultValue`): (`obj`: `any`) => `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `defaultValue` | `any` |

##### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/get.ts:45](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/get.ts#L45)

___

### havePathsInCommon

▸ **havePathsInCommon**(`objLeft`, `objRight`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `objLeft` | `Object` \| `any`[] |
| `objRight` | `Object` \| `any`[] |

#### Returns

`boolean`

#### Defined in

[obj/paths.ts:23](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/paths.ts#L23)

___

### keys

▸ **keys**(`obj`): (`string` \| `Symbol`)[]

Returns both string and symbol keys for an object

#### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `Object` |

#### Returns

(`string` \| `Symbol`)[]

Array<string|Symbol>

#### Defined in

[obj/keys.ts:6](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/keys.ts#L6)

___

### paths

▸ **paths**(`obj`, `existingPath?`): `string`[]

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `obj` | `Object` \| `any`[] | `undefined` |
| `existingPath` | `any`[] | `[]` |

#### Returns

`string`[]

#### Defined in

[obj/paths.ts:8](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/paths.ts#L8)

___

### set

▸ **set**(`key`, `value`, `obj`): `any`

Sets a value in an array or object and returns a new copy of the array or object

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` | Key to grab |
| `value` | `any` | Value to set |
| `obj` | `any` | Object to grab a value out of |

#### Returns

`any`

#### Defined in

[obj/set.ts:8](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L8)

▸ **set**(`key`, `value`): (`obj`: `any`) => `any`

Sets a value in an array or object and returns a new copy of the array or object

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` | Key to grab |
| `value` | `any` | Value to set |

#### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/set.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L9)

▸ **set**(`key`): (`value`: `any`, `obj`: `any`) => `any`

Sets a value in an array or object and returns a new copy of the array or object

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` | Key to grab |

#### Returns

`fn`

▸ (`value`, `obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/set.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L10)

▸ **set**(`key`): (`value`: `any`) => (`obj`: `any`) => `any`

Sets a value in an array or object and returns a new copy of the array or object

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` | Key to grab |

#### Returns

`fn`

▸ (`value`): (`obj`: `any`) => `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |

##### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/set.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L11)

___

### setIn

▸ **setIn**(`key`, `value`, `obj`): `any`

Sets a value in an array or object.
If a segment is null, then it adds a new empty object/array and continues. Returns the resulting object.

**`Deprecated`**

**`See`**

setPath

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | - |
| `value` | `any` | Value to set |
| `obj` | `any` | Object to grab a value out of |

#### Returns

`any`

#### Defined in

[obj/set.ts:38](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L38)

▸ **setIn**(`key`, `value`): (`obj`: `any`) => `any`

Sets a value in an array or object.
If a segment is null, then it adds a new empty object/array and continues. Returns the resulting object.

**`Deprecated`**

**`See`**

setPath

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | - |
| `value` | `any` | Value to set |

#### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/set.ts:39](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L39)

▸ **setIn**(`key`): (`value`: `any`, `obj`: `any`) => `any`

Sets a value in an array or object.
If a segment is null, then it adds a new empty object/array and continues. Returns the resulting object.

**`Deprecated`**

**`See`**

setPath

#### Parameters

| Name | Type |
| :------ | :------ |
| `key` | `string` \| `number` \| `Iterable`<`string` \| `number`\> |

#### Returns

`fn`

▸ (`value`, `obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/set.ts:40](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L40)

▸ **setIn**(`key`): (`value`: `any`) => (`obj`: `any`) => `any`

Sets a value in an array or object.
If a segment is null, then it adds a new empty object/array and continues. Returns the resulting object.

**`Deprecated`**

**`See`**

setPath

#### Parameters

| Name | Type |
| :------ | :------ |
| `key` | `string` \| `number` \| `Iterable`<`string` \| `number`\> |

#### Returns

`fn`

▸ (`value`): (`obj`: `any`) => `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |

##### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/set.ts:41](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L41)

___

### setInIfPresent

▸ **setInIfPresent**(`key`, `value`, `obj`): `any`

Sets a value in an array or object only if the path exists.
If the path does not exist, then it returns the original object

**`Deprecated`**

**`See`**

setPathIfPresent

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | - |
| `value` | `any` | Value to set |
| `obj` | `any` | Object to grab a value out of |

#### Returns

`any`

#### Defined in

[obj/set.ts:38](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L38)

▸ **setInIfPresent**(`key`, `value`): (`obj`: `any`) => `any`

Sets a value in an array or object only if the path exists.
If the path does not exist, then it returns the original object

**`Deprecated`**

**`See`**

setPathIfPresent

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | - |
| `value` | `any` | Value to set |

#### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/set.ts:39](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L39)

▸ **setInIfPresent**(`key`): (`value`: `any`, `obj`: `any`) => `any`

Sets a value in an array or object only if the path exists.
If the path does not exist, then it returns the original object

**`Deprecated`**

**`See`**

setPathIfPresent

#### Parameters

| Name | Type |
| :------ | :------ |
| `key` | `string` \| `number` \| `Iterable`<`string` \| `number`\> |

#### Returns

`fn`

▸ (`value`, `obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/set.ts:40](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L40)

▸ **setInIfPresent**(`key`): (`value`: `any`) => (`obj`: `any`) => `any`

Sets a value in an array or object only if the path exists.
If the path does not exist, then it returns the original object

**`Deprecated`**

**`See`**

setPathIfPresent

#### Parameters

| Name | Type |
| :------ | :------ |
| `key` | `string` \| `number` \| `Iterable`<`string` \| `number`\> |

#### Returns

`fn`

▸ (`value`): (`obj`: `any`) => `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |

##### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/set.ts:41](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L41)

___

### setPath

▸ **setPath**(`key`, `value`, `obj`): `any`

Sets a value in an array or object.
If a segment is null, then it adds a new empty object/array and continues. Returns the resulting object.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | - |
| `value` | `any` | Value to set |
| `obj` | `any` | Object to grab a value out of |

#### Returns

`any`

#### Defined in

[obj/set.ts:38](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L38)

▸ **setPath**(`key`, `value`): (`obj`: `any`) => `any`

Sets a value in an array or object.
If a segment is null, then it adds a new empty object/array and continues. Returns the resulting object.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | - |
| `value` | `any` | Value to set |

#### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/set.ts:39](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L39)

▸ **setPath**(`key`): (`value`: `any`, `obj`: `any`) => `any`

Sets a value in an array or object.
If a segment is null, then it adds a new empty object/array and continues. Returns the resulting object.

#### Parameters

| Name | Type |
| :------ | :------ |
| `key` | `string` \| `number` \| `Iterable`<`string` \| `number`\> |

#### Returns

`fn`

▸ (`value`, `obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/set.ts:40](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L40)

▸ **setPath**(`key`): (`value`: `any`) => (`obj`: `any`) => `any`

Sets a value in an array or object.
If a segment is null, then it adds a new empty object/array and continues. Returns the resulting object.

#### Parameters

| Name | Type |
| :------ | :------ |
| `key` | `string` \| `number` \| `Iterable`<`string` \| `number`\> |

#### Returns

`fn`

▸ (`value`): (`obj`: `any`) => `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |

##### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/set.ts:41](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L41)

___

### setPathIfPresent

▸ **setPathIfPresent**(`key`, `value`, `obj`): `any`

Sets a value in an array or object only if the path exists.
If the path does not exist, then it returns the original object

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | - |
| `value` | `any` | Value to set |
| `obj` | `any` | Object to grab a value out of |

#### Returns

`any`

#### Defined in

[obj/set.ts:38](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L38)

▸ **setPathIfPresent**(`key`, `value`): (`obj`: `any`) => `any`

Sets a value in an array or object only if the path exists.
If the path does not exist, then it returns the original object

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` \| `Iterable`<`string` \| `number`\> | - |
| `value` | `any` | Value to set |

#### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/set.ts:39](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L39)

▸ **setPathIfPresent**(`key`): (`value`: `any`, `obj`: `any`) => `any`

Sets a value in an array or object only if the path exists.
If the path does not exist, then it returns the original object

#### Parameters

| Name | Type |
| :------ | :------ |
| `key` | `string` \| `number` \| `Iterable`<`string` \| `number`\> |

#### Returns

`fn`

▸ (`value`, `obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/set.ts:40](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L40)

▸ **setPathIfPresent**(`key`): (`value`: `any`) => (`obj`: `any`) => `any`

Sets a value in an array or object only if the path exists.
If the path does not exist, then it returns the original object

#### Parameters

| Name | Type |
| :------ | :------ |
| `key` | `string` \| `number` \| `Iterable`<`string` \| `number`\> |

#### Returns

`fn`

▸ (`value`): (`obj`: `any`) => `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |

##### Returns

`fn`

▸ (`obj`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

##### Returns

`any`

#### Defined in

[obj/set.ts:41](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/set.ts#L41)

___

### toPairs

▸ **toPairs**(`param`): `any`[]

Grabs entries for an object (for both string and symbol properties)

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `any` |

#### Returns

`any`[]

any[][]

#### Defined in

[obj/entries.ts:17](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/entries.ts#L17)

___

### values

▸ **values**(`obj`): `any`[]

Returns all values for an object (from both strings and symbol keys)

#### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `Object` |

#### Returns

`any`[]

any[]

#### Defined in

[obj/keys.ts:15](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/obj/keys.ts#L15)
