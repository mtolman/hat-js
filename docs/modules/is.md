[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / is

# Namespace: is

Functions for detecting types and properties

## Table of contents

### Functions

- [isArray](is.md#isarray)
- [isBuffer](is.md#isbuffer)
- [isEmpty](is.md#isempty)
- [isEqual](is.md#isequal)
- [isFloat](is.md#isfloat)
- [isFunction](is.md#isfunction)
- [isInfinite](is.md#isinfinite)
- [isInteger](is.md#isinteger)
- [isIterable](is.md#isiterable)
- [isMap](is.md#ismap)
- [isNil](is.md#isnil)
- [isNull](is.md#isnull)
- [isNumber](is.md#isnumber)
- [isObject](is.md#isobject)
- [isSet](is.md#isset)
- [isString](is.md#isstring)
- [isUndefined](is.md#isundefined)

## Functions

### isArray

▸ **isArray**(`obj`): obj is any[]

Returns whether or not something is an array

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `obj` | `any` | the object to test |

#### Returns

obj is any[]

Whether or not it is an array

#### Defined in

[is/isArray.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isArray.ts#L12)

___

### isBuffer

▸ **isBuffer**(`obj`): obj is Buffer

Returns whether or not something is a Buffer

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `obj` | `any` | the object to test |

#### Returns

obj is Buffer

Whether or not it is a buffer

#### Defined in

[is/isBuffer.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isBuffer.ts#L12)

___

### isEmpty

▸ **isEmpty**(`obj`): `boolean`

Returns whether a collection is empty or not

#### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `any` |

#### Returns

`boolean`

#### Defined in

[is/isEmpty.ts:14](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isEmpty.ts#L14)

___

### isEqual

▸ **isEqual**(`left`, `right`): `any`

Performs value equality on the parameters to test for equality

Note: This is slow since it does traverse all elements in a collection

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `left` | `any` | The first object to test |
| `right` | `any` | The second object to test* |

#### Returns

`any`

Whether or not they are equal

#### Defined in

[is/isEqual.ts:31](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isEqual.ts#L31)

___

### isFloat

▸ **isFloat**(`param`): param is number

Returns whether or not something is a number

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `any` |

#### Returns

param is number

Whether or not it is a number

#### Defined in

[is/isNumber.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isNumber.ts#L12)

___

### isFunction

▸ **isFunction**(`param`): param is Function

Returns whether or not something is a function

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `any` |

#### Returns

param is Function

Whether or not it is a function

#### Defined in

[is/isFunction.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isFunction.ts#L12)

___

### isInfinite

▸ **isInfinite**(`param`): `boolean`

Returns whether or not something is an infinite number

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `any` |

#### Returns

`boolean`

Whether or not it is infinite

#### Defined in

[is/isInfinite.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isInfinite.ts#L11)

___

### isInteger

▸ **isInteger**(`param`): param is number

Returns whether or not something is an integer

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `any` |

#### Returns

param is number

Whether or not it is an integer

#### Defined in

[is/isInteger.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isInteger.ts#L12)

___

### isIterable

▸ **isIterable**(`param`): param is Iterable<any\>

Returns whether or not something is an iterable

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `any` |

#### Returns

param is Iterable<any\>

Whether or not it is an iterable

#### Defined in

[is/isIterable.ts:14](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isIterable.ts#L14)

___

### isMap

▸ **isMap**(`obj`): obj is Map<any, any\>

Returns whether or not something is a Map

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `obj` | `any` | the object to test |

#### Returns

obj is Map<any, any\>

Whether or not it is a Map

#### Defined in

[is/isMap.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isMap.ts#L12)

___

### isNil

▸ **isNil**(`param`): param is null

Returns whether or not something is either null or undefined

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `any` |

#### Returns

param is null

Whether or not it is either null or undefined

#### Defined in

[is/isNil.ts:15](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isNil.ts#L15)

___

### isNull

▸ **isNull**(`param`): param is null

Returns whether or not something is null

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `any` |

#### Returns

param is null

Whether or not it is null

#### Defined in

[is/isNull.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isNull.ts#L12)

___

### isNumber

▸ **isNumber**(`param`): param is number

Returns whether or not something is a number

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `any` |

#### Returns

param is number

Whether or not it is a number

#### Defined in

[is/isNumber.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isNumber.ts#L12)

___

### isObject

▸ **isObject**(`param`): param is object

Returns whether or not something is an Object

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `any` |

#### Returns

param is object

Whether or not it is an Object

#### Defined in

[is/isObject.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isObject.ts#L12)

___

### isSet

▸ **isSet**(`obj`): obj is Set<any\>

Returns whether or not something is a Set

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `obj` | `any` | the object to test |

#### Returns

obj is Set<any\>

Whether or not it is a Set

#### Defined in

[is/isSet.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isSet.ts#L12)

___

### isString

▸ **isString**(`param`): param is string

Returns whether or not something is a string

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `any` |

#### Returns

param is string

Whether or not it is a string

#### Defined in

[is/isString.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isString.ts#L12)

___

### isUndefined

▸ **isUndefined**(`param`): param is undefined

Returns whether or not something is undefined

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `any` |

#### Returns

param is undefined

Whether or not it is undefined

#### Defined in

[is/isUndefined.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/is/isUndefined.ts#L12)
