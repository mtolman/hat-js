[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / to

# Namespace: to

Functions for working with arrays

## Table of contents

### Functions

- [toBool](to.md#tobool)
- [toInt](to.md#toint)
- [toNumber](to.md#tonumber)
- [toString](to.md#tostring)

## Functions

### toBool

▸ **toBool**(`value`): `boolean`

Coerces a value to a boolean

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |

#### Returns

`boolean`

#### Defined in

[to/toBool.ts:5](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/to/toBool.ts#L5)

___

### toInt

▸ **toInt**(`value`): `number`

Coerces a value to an integer

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |

#### Returns

`number`

#### Defined in

[to/toInt.ts:5](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/to/toInt.ts#L5)

___

### toNumber

▸ **toNumber**(`value`): `number`

Coerces a value to a number

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |

#### Returns

`number`

#### Defined in

[to/toNumber.ts:5](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/to/toNumber.ts#L5)

___

### toString

▸ **toString**(`value`): `string`

Coerces a value to a string

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |

#### Returns

`string`

#### Defined in

[to/toString.ts:5](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/to/toString.ts#L5)
