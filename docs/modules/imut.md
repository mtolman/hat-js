[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / imut

# Namespace: imut

Collection of immutable data structures

## Table of contents

### Variables

- [List](imut.md#list)
- [Stack](imut.md#stack)
- [Zipper](imut.md#zipper)

## Variables

### List

• `Const` **List**: `__module` = `l`

#### Defined in

[immutable/index.ts:7](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/immutable/index.ts#L7)

___

### Stack

• `Const` **Stack**: `__module` = `s`

#### Defined in

[immutable/index.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/immutable/index.ts#L10)

___

### Zipper

• `Const` **Zipper**: `__module` = `z`

#### Defined in

[immutable/index.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/immutable/index.ts#L13)
