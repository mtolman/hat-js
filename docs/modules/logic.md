[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / logic

# Namespace: logic

Functions for handling boolean logic

## Table of contents

### Functions

- [and](logic.md#and)
- [negate](logic.md#negate)
- [negateAll](logic.md#negateall)
- [or](logic.md#or)
- [xor](logic.md#xor)

## Functions

### and

▸ **and**(...`bools`): `boolean`

Ands a group of booleans

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `...bools` | `boolean`[] | list of booleans |

#### Returns

`boolean`

result of and on all of them

#### Defined in

[logic/basic.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/logic/basic.ts#L13)

___

### negate

▸ **negate**(`bool`): `boolean`

Negates a boolean

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `bool` | `boolean` | boolean to negate |

#### Returns

`boolean`

negation of the boolean

#### Defined in

[logic/basic.ts:41](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/logic/basic.ts#L41)

___

### negateAll

▸ **negateAll**(...`bools`): `boolean`[]

Negates a group of booleans

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `...bools` | `boolean`[] | list of booleans |

#### Returns

`boolean`[]

result of negating each boolean

#### Defined in

[logic/basic.ts:50](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/logic/basic.ts#L50)

___

### or

▸ **or**(...`bools`): `boolean`

Ors a group of booleans

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `...bools` | `boolean`[] | list of booleans |

#### Returns

`boolean`

result of or on all of them

#### Defined in

[logic/basic.ts:23](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/logic/basic.ts#L23)

___

### xor

▸ **xor**(...`bools`): `boolean`

Xors a group of booleans (only returns true if one and only one is true)

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `...bools` | `boolean`[] | list of booleans |

#### Returns

`boolean`

result of xor on all of them

#### Defined in

[logic/basic.ts:32](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/logic/basic.ts#L32)
