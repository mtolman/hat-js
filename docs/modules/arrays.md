[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / arrays

# Namespace: arrays

Functions for working with arrays

## Table of contents

### Interfaces

- [iAll](../interfaces/arrays.iAll.md)
- [iAny](../interfaces/arrays.iAny.md)
- [iAppend](../interfaces/arrays.iAppend.md)
- [iChunk](../interfaces/arrays.iChunk.md)
- [iConcat](../interfaces/arrays.iConcat.md)
- [iContains](../interfaces/arrays.iContains.md)
- [iDifference](../interfaces/arrays.iDifference.md)
- [iFill](../interfaces/arrays.iFill.md)
- [iFillAll](../interfaces/arrays.iFillAll.md)
- [iFillEnd](../interfaces/arrays.iFillEnd.md)
- [iFilter](../interfaces/arrays.iFilter.md)
- [iFind](../interfaces/arrays.iFind.md)
- [iFindOr](../interfaces/arrays.iFindOr.md)
- [iFirstOr](../interfaces/arrays.iFirstOr.md)
- [iFirstOrCall](../interfaces/arrays.iFirstOrCall.md)
- [iFlatMap](../interfaces/arrays.iFlatMap.md)
- [iForEach](../interfaces/arrays.iForEach.md)
- [iGroupBy](../interfaces/arrays.iGroupBy.md)
- [iGroupByKey](../interfaces/arrays.iGroupByKey.md)
- [iIndexBy](../interfaces/arrays.iIndexBy.md)
- [iIndexByKey](../interfaces/arrays.iIndexByKey.md)
- [iIntersect](../interfaces/arrays.iIntersect.md)
- [iJoin](../interfaces/arrays.iJoin.md)
- [iLimit](../interfaces/arrays.iLimit.md)
- [iMap](../interfaces/arrays.iMap.md)
- [iPrepend](../interfaces/arrays.iPrepend.md)
- [iReduce](../interfaces/arrays.iReduce.md)
- [iScan](../interfaces/arrays.iScan.md)
- [iSkip](../interfaces/arrays.iSkip.md)
- [iSplit](../interfaces/arrays.iSplit.md)
- [iTakeWhile](../interfaces/arrays.iTakeWhile.md)
- [iTap](../interfaces/arrays.iTap.md)
- [iUnion](../interfaces/arrays.iUnion.md)
- [iZip](../interfaces/arrays.iZip.md)

### Functions

- [all](arrays.md#all)
- [any](arrays.md#any)
- [append](arrays.md#append)
- [chunk](arrays.md#chunk)
- [concat](arrays.md#concat)
- [contains](arrays.md#contains)
- [containsEquals](arrays.md#containsequals)
- [difference](arrays.md#difference)
- [fill](arrays.md#fill)
- [fillAll](arrays.md#fillall)
- [fillEnd](arrays.md#fillend)
- [fillStart](arrays.md#fillstart)
- [filter](arrays.md#filter)
- [find](arrays.md#find)
- [findOr](arrays.md#findor)
- [findOrNull](arrays.md#findornull)
- [first](arrays.md#first)
- [firstOr](arrays.md#firstor)
- [firstOrCall](arrays.md#firstorcall)
- [firstOrNull](arrays.md#firstornull)
- [flatMap](arrays.md#flatmap)
- [flatten](arrays.md#flatten)
- [forEach](arrays.md#foreach)
- [fromPairs](arrays.md#frompairs)
- [groupBy](arrays.md#groupby)
- [groupByKey](arrays.md#groupbykey)
- [head](arrays.md#head)
- [indexBy](arrays.md#indexby)
- [indexByKey](arrays.md#indexbykey)
- [intersect](arrays.md#intersect)
- [join](arrays.md#join)
- [last](arrays.md#last)
- [limit](arrays.md#limit)
- [map](arrays.md#map)
- [prepend](arrays.md#prepend)
- [reduce](arrays.md#reduce)
- [removeFalsey](arrays.md#removefalsey)
- [removeTruthy](arrays.md#removetruthy)
- [rest](arrays.md#rest)
- [reverse](arrays.md#reverse)
- [scan](arrays.md#scan)
- [skip](arrays.md#skip)
- [some](arrays.md#some)
- [split](arrays.md#split)
- [take](arrays.md#take)
- [takeWhile](arrays.md#takewhile)
- [tap](arrays.md#tap)
- [toArrayOrEmpty](arrays.md#toarrayorempty)
- [union](arrays.md#union)
- [wrapIfNotArray](arrays.md#wrapifnotarray)
- [wrapInArray](arrays.md#wrapinarray)
- [zip](arrays.md#zip)
- [zipLoop](arrays.md#ziploop)

## Functions

### all

▸ **all**(`predicate`, `arr`): `boolean`

Applies a predicate to all elements of an array and ensures they all return true
Will return false as soon as the predicate returns false for any element

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `predicate` | `Predicate`<`any`\> | The predicate to execute for each element |
| `arr` | `any`[] | The array to iterate over |

#### Returns

`boolean`

returns true if the predicate returns true for all elements of the iterable

#### Defined in

[arrays/all.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/all.ts#L11)

▸ **all**(`predicate`): (`arr`: `any`[]) => `boolean`

Applies a predicate to all elements of an array and ensures they all return true
Will return false as soon as the predicate returns false for any element

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `predicate` | `Predicate`<`any`\> | The predicate to execute for each element |

#### Returns

`fn`

returns true if the predicate returns true for all elements of the iterable

▸ (`arr`): `boolean`

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`boolean`

#### Defined in

[arrays/all.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/all.ts#L12)

___

### any

▸ **any**(`predicate`, `arr`): `boolean`

Applies a predicate to all elements of an array and checks if the predicate returns true for any of them
Will return true as soon as the predicate returns true for any element

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `predicate` | `Predicate`<`any`\> | The predicate to execute for each element |
| `arr` | `any`[] | The array to iterate over |

#### Returns

`boolean`

returns true if the predicate returns true for any elements of the iterable

#### Defined in

[arrays/any.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/any.ts#L11)

▸ **any**(`predicate`): (`arr`: `any`[]) => `boolean`

Applies a predicate to all elements of an array and checks if the predicate returns true for any of them
Will return true as soon as the predicate returns true for any element

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `predicate` | `Predicate`<`any`\> | The predicate to execute for each element |

#### Returns

`fn`

returns true if the predicate returns true for any elements of the iterable

▸ (`arr`): `boolean`

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`boolean`

#### Defined in

[arrays/any.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/any.ts#L12)

___

### append

▸ **append**(`arr`, `val`, ...`moreVals`): `any`[]

Adds one or more items to the end of the array

E.g.
`append([1, 2, 3], 4, 5, 6) => [1, 2, 3, 4, 5, 6]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `arr` | `any`[] | The array to append to |
| `val` | `any` | The value to append |
| `...moreVals` | `any`[] | Other values to append |

#### Returns

`any`[]

The resulting array

#### Defined in

[arrays/append.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/append.ts#L9)

▸ **append**(`arr`, `val`): `any`[]

Adds one or more items to the end of the array

E.g.
`append([1, 2, 3], 4, 5, 6) => [1, 2, 3, 4, 5, 6]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `arr` | `any`[] | The array to append to |
| `val` | `any` | The value to append |

#### Returns

`any`[]

The resulting array

#### Defined in

[arrays/append.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/append.ts#L10)

▸ **append**(`arr`): (`val`: `any`, ...`moreVals`: `any`[]) => `any`[]

Adds one or more items to the end of the array

E.g.
`append([1, 2, 3], 4, 5, 6) => [1, 2, 3, 4, 5, 6]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `arr` | `any`[] | The array to append to |

#### Returns

`fn`

The resulting array

▸ (`val`, ...`moreVals`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `val` | `any` |
| `...moreVals` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/append.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/append.ts#L11)

▸ **append**(`arr`): (`val`: `any`) => `any`[]

Adds one or more items to the end of the array

E.g.
`append([1, 2, 3], 4, 5, 6) => [1, 2, 3, 4, 5, 6]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `arr` | `any`[] | The array to append to |

#### Returns

`fn`

The resulting array

▸ (`val`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `val` | `any` |

##### Returns

`any`[]

#### Defined in

[arrays/append.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/append.ts#L12)

___

### chunk

▸ **chunk**(`size`, `array`): `any`[][]

Chunks an array into arrays of a size

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `size` | `number` | Chunk size |
| `array` | `any` | Array to chunk |

#### Returns

`any`[][]

The chunked array

#### Defined in

[arrays/chunk.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/chunk.ts#L10)

▸ **chunk**(`size`): (`array`: `any`) => `any`[][]

Chunks an array into arrays of a size

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `size` | `number` | Chunk size |

#### Returns

`fn`

The chunked array

▸ (`array`): `any`[][]

##### Parameters

| Name | Type |
| :------ | :------ |
| `array` | `any` |

##### Returns

`any`[][]

#### Defined in

[arrays/chunk.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/chunk.ts#L11)

___

### concat

▸ **concat**(`arrLeft`, `arrRight`, ...`moreArrays`): `any`[]

Takes two or more arrays and concatenates them

E.g.
`concat([1, 2, 3], [4, 5, 6]) => [1, 2, 3, 4, 5, 6]`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `arrLeft` | `any`[] | The first array to concatenate |
| `arrRight` | `any`[] | The second array to concatenate |
| `...moreArrays` | `any`[][] | Other arrays to concatenate |

#### Returns

`any`[]

The resulting array

#### Defined in

[arrays/concat.ts:8](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/concat.ts#L8)

▸ **concat**(`arrLeft`, `arrRight`): `any`[]

Takes two or more arrays and concatenates them

E.g.
`concat([1, 2, 3], [4, 5, 6]) => [1, 2, 3, 4, 5, 6]`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `arrLeft` | `any`[] | The first array to concatenate |
| `arrRight` | `any`[] | The second array to concatenate |

#### Returns

`any`[]

The resulting array

#### Defined in

[arrays/concat.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/concat.ts#L9)

▸ **concat**(`arrLeft`): (`arrRight`: `any`[], ...`moreArrays`: `any`[][]) => `any`[]

Takes two or more arrays and concatenates them

E.g.
`concat([1, 2, 3], [4, 5, 6]) => [1, 2, 3, 4, 5, 6]`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `arrLeft` | `any`[] | The first array to concatenate |

#### Returns

`fn`

The resulting array

▸ (`arrRight`, ...`moreArrays`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arrRight` | `any`[] |
| `...moreArrays` | `any`[][] |

##### Returns

`any`[]

#### Defined in

[arrays/concat.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/concat.ts#L10)

▸ **concat**(`arrLeft`): (`arrRight`: `any`[]) => `any`[]

Takes two or more arrays and concatenates them

E.g.
`concat([1, 2, 3], [4, 5, 6]) => [1, 2, 3, 4, 5, 6]`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `arrLeft` | `any`[] | The first array to concatenate |

#### Returns

`fn`

The resulting array

▸ (`arrRight`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arrRight` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/concat.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/concat.ts#L11)

___

### contains

▸ **contains**(`val`, `arr`): `boolean`

Returns whether an array contains a value

E.g.
`contains([1, 2, 3], 2) => true`
`contains([1, 2, 3], 4) => false`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `val` | `any` | The predicate to execute for each element |
| `arr` | `any`[] | The array to iterate over |

#### Returns

`boolean`

returns true if the value exists in the array

#### Defined in

[arrays/contains.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/contains.ts#L9)

▸ **contains**(`val`): (`arr`: `any`[]) => `boolean`

Returns whether an array contains a value

E.g.
`contains([1, 2, 3], 2) => true`
`contains([1, 2, 3], 4) => false`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `val` | `any` | The predicate to execute for each element |

#### Returns

`fn`

returns true if the value exists in the array

▸ (`arr`): `boolean`

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`boolean`

#### Defined in

[arrays/contains.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/contains.ts#L10)

___

### containsEquals

▸ **containsEquals**(`val`, `arr`): `boolean`

Returns whether an array contains a value. Checks using isEqual

E.g.
`containsEquals([1, 2, 3], 2) => true`
`containsEquals([1, 2, 3], 4) => false`

**`Kind`**

function

**`See`**

isEqual

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `val` | `any` | The value to search for |
| `arr` | `any`[] | The array to iterate over |

#### Returns

`boolean`

returns true if the value exists in the array

#### Defined in

[arrays/contains.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/contains.ts#L9)

▸ **containsEquals**(`val`): (`arr`: `any`[]) => `boolean`

Returns whether an array contains a value. Checks using isEqual

E.g.
`containsEquals([1, 2, 3], 2) => true`
`containsEquals([1, 2, 3], 4) => false`

**`Kind`**

function

**`See`**

isEqual

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `val` | `any` | The value to search for |

#### Returns

`fn`

returns true if the value exists in the array

▸ (`arr`): `boolean`

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`boolean`

#### Defined in

[arrays/contains.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/contains.ts#L10)

___

### difference

▸ **difference**(`arrLeft`, `arrRight`, ...`moreArrays`): `any`[]

Returns the items in the first iterable that are not in the other iterables
Can be thought of as performing the Set difference operator (A - B)

Note: It uses the Set has function to find the difference

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `arrLeft` | `any`[] |
| `arrRight` | `any`[] |
| `...moreArrays` | `any`[][] |

#### Returns

`any`[]

values from iterable1 not in the other iterables

#### Defined in

[arrays/difference.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/difference.ts#L9)

▸ **difference**(`arrLeft`, `arrRight`): `any`[]

Returns the items in the first iterable that are not in the other iterables
Can be thought of as performing the Set difference operator (A - B)

Note: It uses the Set has function to find the difference

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `arrLeft` | `any`[] |
| `arrRight` | `any`[] |

#### Returns

`any`[]

values from iterable1 not in the other iterables

#### Defined in

[arrays/difference.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/difference.ts#L10)

▸ **difference**(`arrLeft`): (`arrRight`: `any`[], ...`moreArrays`: `any`[][]) => `any`[]

Returns the items in the first iterable that are not in the other iterables
Can be thought of as performing the Set difference operator (A - B)

Note: It uses the Set has function to find the difference

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `arrLeft` | `any`[] |

#### Returns

`fn`

values from iterable1 not in the other iterables

▸ (`arrRight`, ...`moreArrays`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arrRight` | `any`[] |
| `...moreArrays` | `any`[][] |

##### Returns

`any`[]

#### Defined in

[arrays/difference.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/difference.ts#L11)

▸ **difference**(`arrLeft`): (`arrRight`: `any`[]) => `any`[]

Returns the items in the first iterable that are not in the other iterables
Can be thought of as performing the Set difference operator (A - B)

Note: It uses the Set has function to find the difference

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `arrLeft` | `any`[] |

#### Returns

`fn`

values from iterable1 not in the other iterables

▸ (`arrRight`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arrRight` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/difference.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/difference.ts#L12)

___

### fill

▸ **fill**(`start`, `end`, `val`, `arr`): `any`[]

Takes an array and fills it with a value between [start, end)

E.g.
`fill(1, 3, 5, [1, 2, 3, 4]) => [1, 5, 5, 4]`

#### Parameters

| Name | Type |
| :------ | :------ |
| `start` | `number` |
| `end` | `number` |
| `val` | `any` |
| `arr` | `any`[] |

#### Returns

`any`[]

#### Defined in

[arrays/fill.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/fill.ts#L9)

▸ **fill**(`start`, `end`, `val`): (`arr`: `any`[]) => `any`[]

Takes an array and fills it with a value between [start, end)

E.g.
`fill(1, 3, 5, [1, 2, 3, 4]) => [1, 5, 5, 4]`

#### Parameters

| Name | Type |
| :------ | :------ |
| `start` | `number` |
| `end` | `number` |
| `val` | `any` |

#### Returns

`fn`

▸ (`arr`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/fill.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/fill.ts#L10)

▸ **fill**(`start`, `end`): (`val`: `any`) => (`arr`: `any`[]) => `any`[]

Takes an array and fills it with a value between [start, end)

E.g.
`fill(1, 3, 5, [1, 2, 3, 4]) => [1, 5, 5, 4]`

#### Parameters

| Name | Type |
| :------ | :------ |
| `start` | `number` |
| `end` | `number` |

#### Returns

`fn`

▸ (`val`): (`arr`: `any`[]) => `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `val` | `any` |

##### Returns

`fn`

▸ (`arr`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/fill.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/fill.ts#L11)

▸ **fill**(`start`, `end`): (`val`: `any`, `arr`: `any`[]) => `any`[]

Takes an array and fills it with a value between [start, end)

E.g.
`fill(1, 3, 5, [1, 2, 3, 4]) => [1, 5, 5, 4]`

#### Parameters

| Name | Type |
| :------ | :------ |
| `start` | `number` |
| `end` | `number` |

#### Returns

`fn`

▸ (`val`, `arr`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `val` | `any` |
| `arr` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/fill.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/fill.ts#L12)

▸ **fill**(`start`): (`end`: `number`, `val`: `any`, `arr`: `any`[]) => `any`[]

Takes an array and fills it with a value between [start, end)

E.g.
`fill(1, 3, 5, [1, 2, 3, 4]) => [1, 5, 5, 4]`

#### Parameters

| Name | Type |
| :------ | :------ |
| `start` | `number` |

#### Returns

`fn`

▸ (`end`, `val`, `arr`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `end` | `number` |
| `val` | `any` |
| `arr` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/fill.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/fill.ts#L13)

▸ **fill**(`start`): (`end`: `number`, `val`: `any`) => (`arr`: `any`[]) => `any`[]

Takes an array and fills it with a value between [start, end)

E.g.
`fill(1, 3, 5, [1, 2, 3, 4]) => [1, 5, 5, 4]`

#### Parameters

| Name | Type |
| :------ | :------ |
| `start` | `number` |

#### Returns

`fn`

▸ (`end`, `val`): (`arr`: `any`[]) => `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `end` | `number` |
| `val` | `any` |

##### Returns

`fn`

▸ (`arr`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/fill.ts:14](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/fill.ts#L14)

▸ **fill**(`start`): (`end`: `number`) => (`val`: `any`, `arr`: `any`[]) => `any`[]

Takes an array and fills it with a value between [start, end)

E.g.
`fill(1, 3, 5, [1, 2, 3, 4]) => [1, 5, 5, 4]`

#### Parameters

| Name | Type |
| :------ | :------ |
| `start` | `number` |

#### Returns

`fn`

▸ (`end`): (`val`: `any`, `arr`: `any`[]) => `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `end` | `number` |

##### Returns

`fn`

▸ (`val`, `arr`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `val` | `any` |
| `arr` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/fill.ts:15](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/fill.ts#L15)

▸ **fill**(`start`): (`end`: `number`) => (`val`: `any`) => (`arr`: `any`[]) => `any`[]

Takes an array and fills it with a value between [start, end)

E.g.
`fill(1, 3, 5, [1, 2, 3, 4]) => [1, 5, 5, 4]`

#### Parameters

| Name | Type |
| :------ | :------ |
| `start` | `number` |

#### Returns

`fn`

▸ (`end`): (`val`: `any`) => (`arr`: `any`[]) => `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `end` | `number` |

##### Returns

`fn`

▸ (`val`): (`arr`: `any`[]) => `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `val` | `any` |

##### Returns

`fn`

▸ (`arr`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/fill.ts:16](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/fill.ts#L16)

___

### fillAll

▸ **fillAll**(`val`, `arr`): `any`[]

Takes an array and fills it with a value

E.g.
`fill(4, [1, 2, 3]) => [4, 4, 4]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `val` | `any` | The value to fill |
| `arr` | `any`[] | The array to fill |

#### Returns

`any`[]

The resulting array

#### Defined in

[arrays/fillAll.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/fillAll.ts#L9)

▸ **fillAll**(`val`): (`arr`: `any`[]) => `any`[]

Takes an array and fills it with a value

E.g.
`fill(4, [1, 2, 3]) => [4, 4, 4]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `val` | `any` | The value to fill |

#### Returns

`fn`

The resulting array

▸ (`arr`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/fillAll.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/fillAll.ts#L10)

___

### fillEnd

▸ **fillEnd**(`start`, `val`, `arr`): `any`[]

Takes an array and fills everything after start with the provided value

E.g.
`fill(1, 5, [1, 2, 3, 4]) => [1, 5, 5, 5]`

#### Parameters

| Name | Type |
| :------ | :------ |
| `start` | `number` |
| `val` | `any` |
| `arr` | `any`[] |

#### Returns

`any`[]

#### Defined in

[arrays/fillEnd.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/fillEnd.ts#L9)

▸ **fillEnd**(`start`, `val`): (`arr`: `any`[]) => `any`[]

Takes an array and fills everything after start with the provided value

E.g.
`fill(1, 5, [1, 2, 3, 4]) => [1, 5, 5, 5]`

#### Parameters

| Name | Type |
| :------ | :------ |
| `start` | `number` |
| `val` | `any` |

#### Returns

`fn`

▸ (`arr`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/fillEnd.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/fillEnd.ts#L10)

▸ **fillEnd**(`start`): (`val`: `any`, `arr`: `any`[]) => `any`[]

Takes an array and fills everything after start with the provided value

E.g.
`fill(1, 5, [1, 2, 3, 4]) => [1, 5, 5, 5]`

#### Parameters

| Name | Type |
| :------ | :------ |
| `start` | `number` |

#### Returns

`fn`

▸ (`val`, `arr`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `val` | `any` |
| `arr` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/fillEnd.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/fillEnd.ts#L11)

▸ **fillEnd**(`start`): (`val`: `any`) => (`arr`: `any`[]) => `any`[]

Takes an array and fills everything after start with the provided value

E.g.
`fill(1, 5, [1, 2, 3, 4]) => [1, 5, 5, 5]`

#### Parameters

| Name | Type |
| :------ | :------ |
| `start` | `number` |

#### Returns

`fn`

▸ (`val`): (`arr`: `any`[]) => `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `val` | `any` |

##### Returns

`fn`

▸ (`arr`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/fillEnd.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/fillEnd.ts#L12)

___

### fillStart

▸ **fillStart**(`end`, `val`, `arr`): `any`[]

Takes an array and fills it with a value between [0, end)

E.g.
`fillStart(3, 5, [1, 2, 3, 4]) => [5, 5, 5, 4]`

#### Parameters

| Name | Type |
| :------ | :------ |
| `end` | `number` |
| `val` | `any` |
| `arr` | `any`[] |

#### Returns

`any`[]

#### Defined in

[arrays/fill.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/fill.ts#L13)

___

### filter

▸ **filter**(`predicate`, `arr`): `any`[]

Filters out the elements of an array where the predicate returns false

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `predicate` | `Predicate`<`any`\> |
| `arr` | `any`[] |

#### Returns

`any`[]

The resulting array

#### Defined in

[arrays/filter.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/filter.ts#L12)

▸ **filter**(`predicate`): (`arr`: `any`[]) => `any`[]

Filters out the elements of an array where the predicate returns false

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `predicate` | `Predicate`<`any`\> |

#### Returns

`fn`

The resulting array

▸ (`arr`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/filter.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/filter.ts#L13)

___

### find

▸ **find**(`predicate`, `arr`): [] \| [`any`]

Returns either an array of a single element of the first match or an empty array if there is no match

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `predicate` | `Predicate`<`any`\> |
| `arr` | `any`[] |

#### Returns

[] \| [`any`]

The resulting array

#### Defined in

[arrays/find.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/find.ts#L12)

▸ **find**(`predicate`): (`arr`: `any`[]) => [] \| [`any`]

Returns either an array of a single element of the first match or an empty array if there is no match

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `predicate` | `Predicate`<`any`\> |

#### Returns

`fn`

The resulting array

▸ (`arr`): [] \| [`any`]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

[] \| [`any`]

#### Defined in

[arrays/find.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/find.ts#L13)

___

### findOr

▸ **findOr**(`defaultValue`, `predicate`, `arr`): `any`

Returns either an array of a single element of the first match or an empty array if there is no match

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `defaultValue` | `any` |
| `predicate` | `Predicate`<`any`\> |
| `arr` | `any`[] |

#### Returns

`any`

#### Defined in

[arrays/findOr.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/findOr.ts#L12)

▸ **findOr**(`defaultValue`, `predicate`): (`arr`: `any`[]) => `any`

Returns either an array of a single element of the first match or an empty array if there is no match

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `defaultValue` | `any` |
| `predicate` | `Predicate`<`any`\> |

#### Returns

`fn`

▸ (`arr`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`any`

#### Defined in

[arrays/findOr.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/findOr.ts#L13)

▸ **findOr**(`defaultValue`): (`predicate`: `Predicate`<`any`\>, `arr`: `any`[]) => `any`

Returns either an array of a single element of the first match or an empty array if there is no match

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `defaultValue` | `any` |

#### Returns

`fn`

▸ (`predicate`, `arr`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `predicate` | `Predicate`<`any`\> |
| `arr` | `any`[] |

##### Returns

`any`

#### Defined in

[arrays/findOr.ts:14](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/findOr.ts#L14)

▸ **findOr**(`defaultValue`): (`predicate`: `Predicate`<`any`\>) => (`arr`: `any`[]) => `any`

Returns either an array of a single element of the first match or an empty array if there is no match

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `defaultValue` | `any` |

#### Returns

`fn`

▸ (`predicate`): (`arr`: `any`[]) => `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `predicate` | `Predicate`<`any`\> |

##### Returns

`fn`

▸ (`arr`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`any`

#### Defined in

[arrays/findOr.ts:15](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/findOr.ts#L15)

___

### findOrNull

▸ **findOrNull**(`predicate`, `arr`): `any`

Returns either an array of a single element of the first match or an empty array if there is no match

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `predicate` | `Predicate`<`any`\> |
| `arr` | `any`[] |

#### Returns

`any`

#### Defined in

[arrays/findOr.ts:14](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/findOr.ts#L14)

___

### first

▸ **first**<`T`\>(`param`): `T`

Returns the first element of an array or null if the array is empty

**`Kind`**

function

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `T`[] |

#### Returns

`T`

The first array element

#### Defined in

[arrays/firstOrNull.ts:15](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/firstOrNull.ts#L15)

___

### firstOr

▸ **firstOr**(`defaultValue`, `array`): `any`

Returns the first element of an array or a default value if the array is empty

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `defaultValue` | `any` | The value to return if the array is empty |
| `array` | `any`[] | Array to operate on |

#### Returns

`any`

The first array element or default value

#### Defined in

[arrays/firstOr.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/firstOr.ts#L10)

▸ **firstOr**(`defaultValue`): (`array`: `any`[]) => `any`

Returns the first element of an array or a default value if the array is empty

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `defaultValue` | `any` | The value to return if the array is empty |

#### Returns

`fn`

The first array element or default value

▸ (`array`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `array` | `any`[] |

##### Returns

`any`

#### Defined in

[arrays/firstOr.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/firstOr.ts#L11)

___

### firstOrCall

▸ **firstOrCall**(`defaultFunc`, `array`): `any`

Returns the first element of an array or a default value if the array is empty

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `defaultFunc` | () => `any` | - |
| `array` | `any`[] | Array to operate on |

#### Returns

`any`

The first array element or default value

#### Defined in

[arrays/firstOrCall.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/firstOrCall.ts#L10)

▸ **firstOrCall**(`defaultFunc`): (`array`: `any`[]) => `any`

Returns the first element of an array or a default value if the array is empty

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `defaultFunc` | () => `any` |

#### Returns

`fn`

The first array element or default value

▸ (`array`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `array` | `any`[] |

##### Returns

`any`

#### Defined in

[arrays/firstOrCall.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/firstOrCall.ts#L11)

___

### firstOrNull

▸ **firstOrNull**<`T`\>(`param`): `T`

Returns the first element of an array or null if the array is empty

**`Kind`**

function

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `T`[] |

#### Returns

`T`

The first array element

#### Defined in

[arrays/firstOrNull.ts:15](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/firstOrNull.ts#L15)

___

### flatMap

▸ **flatMap**(`func`, `array`): `any`[]

Calls map and then flatten on the array

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`T`: `any`) => `any` | Function to call map with |
| `array` | `any`[] | Array to operate on |

#### Returns

`any`[]

The resulting array

#### Defined in

[arrays/flatMap.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/flatMap.ts#L13)

▸ **flatMap**(`func`): (`array`: `any`[]) => `any`[]

Calls map and then flatten on the array

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`T`: `any`) => `any` | Function to call map with |

#### Returns

`fn`

The resulting array

▸ (`array`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `array` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/flatMap.ts:14](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/flatMap.ts#L14)

___

### flatten

▸ **flatten**<`T`\>(`array`): `T`[]

Flattens one layer of nested arrays in an array

**`Kind`**

function

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `array` | (`T` \| `T`[])[] | Array to operate on |

#### Returns

`T`[]

The resulting array

#### Defined in

[arrays/flatten.ts:15](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/flatten.ts#L15)

___

### forEach

▸ **forEach**(`t1`): [`CF1`](../interfaces/fp.CF1.md)<`any`[], `void`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | (`any`: `any`) => `void` |

#### Returns

[`CF1`](../interfaces/fp.CF1.md)<`any`[], `void`\>

#### Defined in

[fp/curry.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L10)

▸ **forEach**(`t1`, `t2`, ...`args`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | (`any`: `any`) => `void` |
| `t2` | `any`[] |
| `...args` | `any`[] |

#### Returns

`void`

#### Defined in

[fp/curry.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L11)

___

### fromPairs

▸ **fromPairs**<`T`\>(`pairs`): `Object`

Converts an array of key/value pairs into an object

**`Kind`**

function

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `pairs` | [`StrOrNum`, `T`][] | Key/value pairs |

#### Returns

`Object`

The resulting object

#### Defined in

[arrays/fromPairs.ts:16](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/fromPairs.ts#L16)

___

### groupBy

▸ **groupBy**(`func`, `arr`): `Object`

Takes an array of values and groups them by the result of calling a function

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`val`: `any`) => `string` \| `number` | A function to call to group elements by |
| `arr` | `any`[] | - |

#### Returns

`Object`

An object where the result of func is the key and an array of elements that provided that key is the value

#### Defined in

[arrays/groupBy.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/groupBy.ts#L10)

▸ **groupBy**(`func`): (`arr`: `any`[]) => { `[key: string]`: `any`[];  }

Takes an array of values and groups them by the result of calling a function

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`val`: `any`) => `string` \| `number` | A function to call to group elements by |

#### Returns

`fn`

An object where the result of func is the key and an array of elements that provided that key is the value

▸ (`arr`): `Object`

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`Object`

#### Defined in

[arrays/groupBy.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/groupBy.ts#L11)

___

### groupByKey

▸ **groupByKey**(`key`, `arr`): `Object`

Takes an array of values and groups them by a key

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` | The key to group by |
| `arr` | `any`[] | - |

#### Returns

`Object`

An object where the result of func is the key and an array of elements that provided that key is the value

#### Defined in

[arrays/groupBy.ts:44](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/groupBy.ts#L44)

▸ **groupByKey**(`key`): (`arr`: `any`[]) => { `[key: string]`: `any`[];  }

Takes an array of values and groups them by a key

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` | The key to group by |

#### Returns

`fn`

An object where the result of func is the key and an array of elements that provided that key is the value

▸ (`arr`): `Object`

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`Object`

#### Defined in

[arrays/groupBy.ts:45](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/groupBy.ts#L45)

___

### head

▸ **head**(`array`): `any`[]

Returns an array of the first element in an array or an empty array if the provided array is empty

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `array` | `any`[] | The array to grab the first element of |

#### Returns

`any`[]

The first element of an array if it is not empty or an empty array otherwwise

#### Defined in

[arrays/limit.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/limit.ts#L11)

___

### indexBy

▸ **indexBy**(`func`, `arr`): `Object`

Takes an array of values and indexes them by the result of a function

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`val`: `any`) => `string` \| `number` | The function to index by |
| `arr` | `any`[] | - |

#### Returns

`Object`

An object where the result of func is the key and an array of elements that provided that key is the value

#### Defined in

[arrays/groupBy.ts:68](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/groupBy.ts#L68)

▸ **indexBy**(`func`): (`arr`: `any`[]) => { `[key: string]`: `any`;  }

Takes an array of values and indexes them by the result of a function

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`val`: `any`) => `string` \| `number` | The function to index by |

#### Returns

`fn`

An object where the result of func is the key and an array of elements that provided that key is the value

▸ (`arr`): `Object`

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`Object`

#### Defined in

[arrays/groupBy.ts:69](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/groupBy.ts#L69)

___

### indexByKey

▸ **indexByKey**(`key`, `arr`): `Object`

Takes an array of values and indexes them by a key

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` | The key to index by |
| `arr` | `any`[] | - |

#### Returns

`Object`

An object where the result of func is the key and an array of elements that provided that key is the value

#### Defined in

[arrays/groupBy.ts:99](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/groupBy.ts#L99)

▸ **indexByKey**(`key`): (`arr`: `any`[]) => { `[key: string]`: `any`;  }

Takes an array of values and indexes them by a key

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` | The key to index by |

#### Returns

`fn`

An object where the result of func is the key and an array of elements that provided that key is the value

▸ (`arr`): `Object`

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`Object`

#### Defined in

[arrays/groupBy.ts:100](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/groupBy.ts#L100)

___

### intersect

▸ **intersect**(`arrLeft`, `arrRight`, ...`moreArrays`): `any`[]

Returns only the items that appear in all arrays

Note: It uses the Set.has function to find matches

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `arrLeft` | `any`[] |
| `arrRight` | `any`[] |
| `...moreArrays` | `any`[][] |

#### Returns

`any`[]

values that only occur in all provided arrays

#### Defined in

[arrays/intersect.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/intersect.ts#L9)

▸ **intersect**(`arrLeft`, `arrRight`): `any`[]

Returns only the items that appear in all arrays

Note: It uses the Set.has function to find matches

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `arrLeft` | `any`[] |
| `arrRight` | `any`[] |

#### Returns

`any`[]

values that only occur in all provided arrays

#### Defined in

[arrays/intersect.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/intersect.ts#L10)

▸ **intersect**(`arrLeft`): (`arrRight`: `any`[], ...`moreArrays`: `any`[][]) => `any`[]

Returns only the items that appear in all arrays

Note: It uses the Set.has function to find matches

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `arrLeft` | `any`[] |

#### Returns

`fn`

values that only occur in all provided arrays

▸ (`arrRight`, ...`moreArrays`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arrRight` | `any`[] |
| `...moreArrays` | `any`[][] |

##### Returns

`any`[]

#### Defined in

[arrays/intersect.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/intersect.ts#L11)

▸ **intersect**(`arrLeft`): (`arrRight`: `any`[]) => `any`[]

Returns only the items that appear in all arrays

Note: It uses the Set.has function to find matches

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `arrLeft` | `any`[] |

#### Returns

`fn`

values that only occur in all provided arrays

▸ (`arrRight`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arrRight` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/intersect.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/intersect.ts#L12)

___

### join

▸ **join**(`separator`, `arr`): `string`

Joins elements of an array with a string

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `separator` | `string` | String to join elements with |
| `arr` | `any`[] | - |

#### Returns

`string`

The resulting string

#### Defined in

[arrays/join.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/join.ts#L10)

▸ **join**(`separator`): (`arr`: `any`[]) => `string`

Joins elements of an array with a string

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `separator` | `string` | String to join elements with |

#### Returns

`fn`

The resulting string

▸ (`arr`): `string`

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`string`

#### Defined in

[arrays/join.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/join.ts#L11)

___

### last

▸ **last**<`T`\>(`array`): [`T`] \| []

Returns an array of the last element in an array or an empty array if the provided array is empty

**`Kind`**

function

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `array` | `T`[] | The array to grab the first element of |

#### Returns

[`T`] \| []

The first element of an array if it is not empty or an empty array otherwwise

#### Defined in

[arrays/last.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/last.ts#L10)

___

### limit

▸ **limit**(`max`, `array`): `any`[]

Limits the number of elements in an array

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `max` | `number` | Maximum number of elements in the array |
| `array` | `any`[] | Array to operate on |

#### Returns

`any`[]

The resulting array

#### Defined in

[arrays/limit.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/limit.ts#L10)

▸ **limit**(`max`): (`array`: `any`[]) => `any`[]

Limits the number of elements in an array

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `max` | `number` | Maximum number of elements in the array |

#### Returns

`fn`

The resulting array

▸ (`array`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `array` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/limit.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/limit.ts#L11)

___

### map

▸ **map**(`func`, `array`): `any`[]

Call a function for each element of the array and return the results in a new array

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`val`: `any`) => `any` | The function to call on each element |
| `array` | `any`[] | Array to operate on |

#### Returns

`any`[]

The resulting array

#### Defined in

[arrays/map.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/map.ts#L10)

▸ **map**(`func`): (`array`: `any`[]) => `any`[]

Call a function for each element of the array and return the results in a new array

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`val`: `any`) => `any` | The function to call on each element |

#### Returns

`fn`

The resulting array

▸ (`array`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `array` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/map.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/map.ts#L11)

___

### prepend

▸ **prepend**(`val`, `arr`, ...`moreVals`): `any`[]

Adds one or more items to the end of the array

E.g.
`append([1, 2, 3], 4, 5, 6) => [1, 2, 3, 4, 5, 6]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `val` | `any` | The value to append |
| `arr` | `any`[] | The array to append to |
| `...moreVals` | `any`[] | Other values to append |

#### Returns

`any`[]

The resulting array

#### Defined in

[arrays/prepend.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/prepend.ts#L9)

▸ **prepend**(`val`, `arr`): `any`[]

Adds one or more items to the end of the array

E.g.
`append([1, 2, 3], 4, 5, 6) => [1, 2, 3, 4, 5, 6]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `val` | `any` | The value to append |
| `arr` | `any`[] | The array to append to |

#### Returns

`any`[]

The resulting array

#### Defined in

[arrays/prepend.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/prepend.ts#L10)

▸ **prepend**(`val`): (`arr`: `any`[], ...`moreVals`: `any`[]) => `any`[]

Adds one or more items to the end of the array

E.g.
`append([1, 2, 3], 4, 5, 6) => [1, 2, 3, 4, 5, 6]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `val` | `any` | The value to append |

#### Returns

`fn`

The resulting array

▸ (`arr`, ...`moreVals`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |
| `...moreVals` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/prepend.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/prepend.ts#L11)

▸ **prepend**(`val`): (`arr`: `any`[]) => `any`[]

Adds one or more items to the end of the array

E.g.
`append([1, 2, 3], 4, 5, 6) => [1, 2, 3, 4, 5, 6]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `val` | `any` | The value to append |

#### Returns

`fn`

The resulting array

▸ (`arr`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/prepend.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/prepend.ts#L12)

___

### reduce

▸ **reduce**(`func`, `startAcc`, `array`): `any`

Accumulates the elements in the array using a function

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`acc`: `any`, `val`: `any`) => `any` | Function to accumulate the values with |
| `startAcc` | `any` | - |
| `array` | `any`[] | Array to operate on |

#### Returns

`any`

The resulting accumulation

#### Defined in

[arrays/reduce.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/reduce.ts#L10)

▸ **reduce**(`func`, `startAcc`): (`array`: `any`[]) => `any`

Accumulates the elements in the array using a function

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`acc`: `any`, `val`: `any`) => `any` | Function to accumulate the values with |
| `startAcc` | `any` | - |

#### Returns

`fn`

The resulting accumulation

▸ (`array`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `array` | `any`[] |

##### Returns

`any`

#### Defined in

[arrays/reduce.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/reduce.ts#L11)

▸ **reduce**(`func`): (`startAcc`: `any`, `array`: `any`[]) => `any`

Accumulates the elements in the array using a function

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`acc`: `any`, `val`: `any`) => `any` | Function to accumulate the values with |

#### Returns

`fn`

The resulting accumulation

▸ (`startAcc`, `array`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `startAcc` | `any` |
| `array` | `any`[] |

##### Returns

`any`

#### Defined in

[arrays/reduce.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/reduce.ts#L12)

▸ **reduce**(`func`): (`startAcc`: `any`) => (`array`: `any`[]) => `any`

Accumulates the elements in the array using a function

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`acc`: `any`, `val`: `any`) => `any` | Function to accumulate the values with |

#### Returns

`fn`

The resulting accumulation

▸ (`startAcc`): (`array`: `any`[]) => `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `startAcc` | `any` |

##### Returns

`fn`

▸ (`array`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `array` | `any`[] |

##### Returns

`any`

#### Defined in

[arrays/reduce.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/reduce.ts#L13)

___

### removeFalsey

▸ **removeFalsey**(`arr`): `any`[]

Returns either an array with all falsey elements removed

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

#### Returns

`any`[]

#### Defined in

[arrays/filter.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/filter.ts#L13)

___

### removeTruthy

▸ **removeTruthy**(`arr`): `any`[]

Returns either an array with all truthy elements removed

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

#### Returns

`any`[]

#### Defined in

[arrays/filter.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/filter.ts#L13)

___

### rest

▸ **rest**<`T`\>(`array`): `T`[]

Returns an array of the all the items past the first item

**`Kind`**

function

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `array` | `T`[] | The array to grab the first element of |

#### Returns

`T`[]

#### Defined in

[arrays/rest.ts:8](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/rest.ts#L8)

___

### reverse

▸ **reverse**<`T`\>(`array`): `T`[]

Reverses an array

**`Kind`**

function

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `array` | `T`[] | The array to grab the first element of |

#### Returns

`T`[]

#### Defined in

[arrays/reverse.ts:8](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/reverse.ts#L8)

___

### scan

▸ **scan**(`func`, `startAcc`, `array`): `any`[]

Accumulates the elements in the array using a function and returns an array with each intermediate accumulation

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`acc`: `any`, `val`: `any`) => `any` | Function to accumulate the values with |
| `startAcc` | `any` | - |
| `array` | `any`[] | Array to operate on |

#### Returns

`any`[]

The resulting intermediate accumulation values

#### Defined in

[arrays/scan.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/scan.ts#L10)

▸ **scan**(`func`, `startAcc`): (`array`: `any`[]) => `any`[]

Accumulates the elements in the array using a function and returns an array with each intermediate accumulation

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`acc`: `any`, `val`: `any`) => `any` | Function to accumulate the values with |
| `startAcc` | `any` | - |

#### Returns

`fn`

The resulting intermediate accumulation values

▸ (`array`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `array` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/scan.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/scan.ts#L11)

▸ **scan**(`func`): (`startAcc`: `any`, `array`: `any`[]) => `any`[]

Accumulates the elements in the array using a function and returns an array with each intermediate accumulation

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`acc`: `any`, `val`: `any`) => `any` | Function to accumulate the values with |

#### Returns

`fn`

The resulting intermediate accumulation values

▸ (`startAcc`, `array`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `startAcc` | `any` |
| `array` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/scan.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/scan.ts#L12)

▸ **scan**(`func`): (`startAcc`: `any`) => (`array`: `any`[]) => `any`[]

Accumulates the elements in the array using a function and returns an array with each intermediate accumulation

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`acc`: `any`, `val`: `any`) => `any` | Function to accumulate the values with |

#### Returns

`fn`

The resulting intermediate accumulation values

▸ (`startAcc`): (`array`: `any`[]) => `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `startAcc` | `any` |

##### Returns

`fn`

▸ (`array`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `array` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/scan.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/scan.ts#L13)

___

### skip

▸ **skip**(`n`, `array`): `any`[]

Skips the first n items of the array

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `n` | `number` | Number of elements to skip |
| `array` | `any`[] | Array to operate on |

#### Returns

`any`[]

The resulting array

#### Defined in

[arrays/skip.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/skip.ts#L9)

▸ **skip**(`n`): (`array`: `any`[]) => `any`[]

Skips the first n items of the array

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `n` | `number` | Number of elements to skip |

#### Returns

`fn`

The resulting array

▸ (`array`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `array` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/skip.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/skip.ts#L10)

___

### some

▸ **some**(`predicate`, `arr`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `predicate` | `Predicate`<`any`\> |
| `arr` | `any`[] |

#### Returns

`boolean`

#### Defined in

[arrays/any.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/any.ts#L11)

▸ **some**(`predicate`): (`arr`: `any`[]) => `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `predicate` | `Predicate`<`any`\> |

#### Returns

`fn`

▸ (`arr`): `boolean`

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`boolean`

#### Defined in

[arrays/any.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/any.ts#L12)

___

### split

▸ **split**(`n`, `arr`): [`any`[], `any`[]]

Splits an array into 2 arrays after n elements

#### Parameters

| Name | Type |
| :------ | :------ |
| `n` | `number` |
| `arr` | `any`[] |

#### Returns

[`any`[], `any`[]]

#### Defined in

[arrays/split.ts:6](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/split.ts#L6)

▸ **split**(`n`): (`arr`: `any`[]) => [`any`[], `any`[]]

Splits an array into 2 arrays after n elements

#### Parameters

| Name | Type |
| :------ | :------ |
| `n` | `number` |

#### Returns

`fn`

▸ (`arr`): [`any`[], `any`[]]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

[`any`[], `any`[]]

#### Defined in

[arrays/split.ts:7](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/split.ts#L7)

___

### take

▸ **take**(`max`, `array`): `any`[]

Alias for limit

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `max` | `number` | Maximum number of elements in the array |
| `array` | `any`[] | Array to operate on |

#### Returns

`any`[]

The resulting array

#### Defined in

[arrays/limit.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/limit.ts#L10)

▸ **take**(`max`): (`array`: `any`[]) => `any`[]

Alias for limit

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `max` | `number` | Maximum number of elements in the array |

#### Returns

`fn`

The resulting array

▸ (`array`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `array` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/limit.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/limit.ts#L11)

___

### takeWhile

▸ **takeWhile**(`whileFunc`, `arr`): `any`[]

Takes elements from the array so long as the predicate returns true for each element

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `whileFunc` | `any`[] \| `Predicate`<`any`\> | Predicate to determine when to stop taking elements |
| `arr` | `any`[] | - |

#### Returns

`any`[]

The resulting array

#### Defined in

[arrays/takeWhile.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/takeWhile.ts#L13)

▸ **takeWhile**(`whileFunc`): (`arr`: `any`[]) => `any`[]

Takes elements from the array so long as the predicate returns true for each element

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `whileFunc` | `any`[] \| `Predicate`<`any`\> | Predicate to determine when to stop taking elements |

#### Returns

`fn`

The resulting array

▸ (`arr`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arr` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/takeWhile.ts:14](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/takeWhile.ts#L14)

___

### tap

▸ **tap**(`func`, `array`): `any`[]

Calls a function for each element in the array and returns the array

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`val`: `any`) => `void` | Function to call on the array |
| `array` | `any`[] | Array to operate on |

#### Returns

`any`[]

The original array

#### Defined in

[arrays/tap.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/tap.ts#L10)

▸ **tap**(`func`): (`array`: `any`[]) => `any`[]

Calls a function for each element in the array and returns the array

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`val`: `any`) => `void` | Function to call on the array |

#### Returns

`fn`

The original array

▸ (`array`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `array` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/tap.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/tap.ts#L11)

___

### toArrayOrEmpty

▸ **toArrayOrEmpty**<`T`\>(`obj`): `T`[]

If the element is an array, it returns the array; otherwise it returns an empty array

**`Kind`**

function

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `obj` | `any` | Object to check |

#### Returns

`T`[]

The resulting array

#### Defined in

[arrays/toArrayOrEmpty.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/toArrayOrEmpty.ts#L13)

___

### union

▸ **union**(`arrLeft`, `arrRight`, ...`moreArrays`): `any`[]

Returns only the items that appear in all arrays

Note: It uses the Set.has function to find matches

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `arrLeft` | `any`[] |
| `arrRight` | `any`[] |
| `...moreArrays` | `any`[][] |

#### Returns

`any`[]

values that only occur in all provided arrays

#### Defined in

[arrays/union.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/union.ts#L9)

▸ **union**(`arrLeft`, `arrRight`): `any`[]

Returns only the items that appear in all arrays

Note: It uses the Set.has function to find matches

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `arrLeft` | `any`[] |
| `arrRight` | `any`[] |

#### Returns

`any`[]

values that only occur in all provided arrays

#### Defined in

[arrays/union.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/union.ts#L10)

▸ **union**(`arrLeft`): (`arrRight`: `any`[], ...`moreArrays`: `any`[][]) => `any`[]

Returns only the items that appear in all arrays

Note: It uses the Set.has function to find matches

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `arrLeft` | `any`[] |

#### Returns

`fn`

values that only occur in all provided arrays

▸ (`arrRight`, ...`moreArrays`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arrRight` | `any`[] |
| `...moreArrays` | `any`[][] |

##### Returns

`any`[]

#### Defined in

[arrays/union.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/union.ts#L11)

▸ **union**(`arrLeft`): (`arrRight`: `any`[]) => `any`[]

Returns only the items that appear in all arrays

Note: It uses the Set.has function to find matches

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `arrLeft` | `any`[] |

#### Returns

`fn`

values that only occur in all provided arrays

▸ (`arrRight`): `any`[]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arrRight` | `any`[] |

##### Returns

`any`[]

#### Defined in

[arrays/union.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/union.ts#L12)

___

### wrapIfNotArray

▸ **wrapIfNotArray**<`T`\>(`obj`): `T`[]

If the element is an array, it returns the array; otherwise it returns an empty array

**`Kind`**

function

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `obj` | `T` \| `T`[] | Object to check |

#### Returns

`T`[]

The resulting array

#### Defined in

[arrays/wrapIfNotArray.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/wrapIfNotArray.ts#L13)

___

### wrapInArray

▸ **wrapInArray**<`T`\>(`val`): [`T`]

Wraps a value in an array

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `val` | `T` |

#### Returns

[`T`]

#### Defined in

[arrays/wrapInArray.ts:5](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/wrapInArray.ts#L5)

___

### zip

▸ **zip**(`arrLeft`, `arrRight`, ...`moreArrays`): `any`[][]

Takes two or more arrays and groups elements by index from each array
If arrays are different lengths, it will fill up missing spots with null

E.g.
`zip([1, 2, 3], [4, 5, 6]) => [[1, 4], [2, 5], [3, 6]]`
`zip([1, 2], [4, 5, 6]) => [[1, 4], [2, 5], [null, 6]]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `arrLeft` | `any`[] | The first array to zip |
| `arrRight` | `any`[] | The second array to zip |
| `...moreArrays` | `any`[][] | Other arrays to zip |

#### Returns

`any`[][]

The resulting array

#### Defined in

[arrays/zip.ts:8](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/zip.ts#L8)

▸ **zip**(`arrLeft`, `arrRight`): [`any`, `any`][]

Takes two or more arrays and groups elements by index from each array
If arrays are different lengths, it will fill up missing spots with null

E.g.
`zip([1, 2, 3], [4, 5, 6]) => [[1, 4], [2, 5], [3, 6]]`
`zip([1, 2], [4, 5, 6]) => [[1, 4], [2, 5], [null, 6]]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `arrLeft` | `any`[] | The first array to zip |
| `arrRight` | `any`[] | The second array to zip |

#### Returns

[`any`, `any`][]

The resulting array

#### Defined in

[arrays/zip.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/zip.ts#L9)

▸ **zip**(`arrLeft`): (`arrRight`: `any`[], ...`moreArrays`: `any`[][]) => `any`[][]

Takes two or more arrays and groups elements by index from each array
If arrays are different lengths, it will fill up missing spots with null

E.g.
`zip([1, 2, 3], [4, 5, 6]) => [[1, 4], [2, 5], [3, 6]]`
`zip([1, 2], [4, 5, 6]) => [[1, 4], [2, 5], [null, 6]]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `arrLeft` | `any`[] | The first array to zip |

#### Returns

`fn`

The resulting array

▸ (`arrRight`, ...`moreArrays`): `any`[][]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arrRight` | `any`[] |
| `...moreArrays` | `any`[][] |

##### Returns

`any`[][]

#### Defined in

[arrays/zip.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/zip.ts#L10)

▸ **zip**(`arrLeft`): (`arrRight`: `any`[]) => [`any`, `any`][]

Takes two or more arrays and groups elements by index from each array
If arrays are different lengths, it will fill up missing spots with null

E.g.
`zip([1, 2, 3], [4, 5, 6]) => [[1, 4], [2, 5], [3, 6]]`
`zip([1, 2], [4, 5, 6]) => [[1, 4], [2, 5], [null, 6]]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `arrLeft` | `any`[] | The first array to zip |

#### Returns

`fn`

The resulting array

▸ (`arrRight`): [`any`, `any`][]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arrRight` | `any`[] |

##### Returns

[`any`, `any`][]

#### Defined in

[arrays/zip.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/zip.ts#L11)

___

### zipLoop

▸ **zipLoop**(`arrLeft`, `arrRight`, ...`moreArrays`): `any`[][]

Takes two or more arrays and groups elements by index from each array
If arrays are different lengths, it will fill up missing spots by wrapping around

E.g.
`zip([1, 2, 3], [4, 5, 6]) => [[1, 4], [2, 5], [3, 6]]`
`zip([1, 2], [4, 5, 6]) => [[1, 4], [2, 5], [1, 6]]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `arrLeft` | `any`[] | The first array to zip |
| `arrRight` | `any`[] | The second array to zip |
| `...moreArrays` | `any`[][] | Other arrays to zip |

#### Returns

`any`[][]

The resulting array

#### Defined in

[arrays/zip.ts:8](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/zip.ts#L8)

▸ **zipLoop**(`arrLeft`, `arrRight`): [`any`, `any`][]

Takes two or more arrays and groups elements by index from each array
If arrays are different lengths, it will fill up missing spots by wrapping around

E.g.
`zip([1, 2, 3], [4, 5, 6]) => [[1, 4], [2, 5], [3, 6]]`
`zip([1, 2], [4, 5, 6]) => [[1, 4], [2, 5], [1, 6]]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `arrLeft` | `any`[] | The first array to zip |
| `arrRight` | `any`[] | The second array to zip |

#### Returns

[`any`, `any`][]

The resulting array

#### Defined in

[arrays/zip.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/zip.ts#L9)

▸ **zipLoop**(`arrLeft`): (`arrRight`: `any`[], ...`moreArrays`: `any`[][]) => `any`[][]

Takes two or more arrays and groups elements by index from each array
If arrays are different lengths, it will fill up missing spots by wrapping around

E.g.
`zip([1, 2, 3], [4, 5, 6]) => [[1, 4], [2, 5], [3, 6]]`
`zip([1, 2], [4, 5, 6]) => [[1, 4], [2, 5], [1, 6]]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `arrLeft` | `any`[] | The first array to zip |

#### Returns

`fn`

The resulting array

▸ (`arrRight`, ...`moreArrays`): `any`[][]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arrRight` | `any`[] |
| `...moreArrays` | `any`[][] |

##### Returns

`any`[][]

#### Defined in

[arrays/zip.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/zip.ts#L10)

▸ **zipLoop**(`arrLeft`): (`arrRight`: `any`[]) => [`any`, `any`][]

Takes two or more arrays and groups elements by index from each array
If arrays are different lengths, it will fill up missing spots by wrapping around

E.g.
`zip([1, 2, 3], [4, 5, 6]) => [[1, 4], [2, 5], [3, 6]]`
`zip([1, 2], [4, 5, 6]) => [[1, 4], [2, 5], [1, 6]]`

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `arrLeft` | `any`[] | The first array to zip |

#### Returns

`fn`

The resulting array

▸ (`arrRight`): [`any`, `any`][]

##### Parameters

| Name | Type |
| :------ | :------ |
| `arrRight` | `any`[] |

##### Returns

[`any`, `any`][]

#### Defined in

[arrays/zip.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/arrays/zip.ts#L11)
