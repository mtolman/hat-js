[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / fp

# Namespace: fp

Functions for working with functions

## Table of contents

### Interfaces

- [CF1](../interfaces/fp.CF1.md)
- [CF2](../interfaces/fp.CF2.md)
- [CF3](../interfaces/fp.CF3.md)
- [CF4](../interfaces/fp.CF4.md)
- [CF5](../interfaces/fp.CF5.md)
- [CF6](../interfaces/fp.CF6.md)
- [CF7](../interfaces/fp.CF7.md)
- [CurryFn](../interfaces/fp.CurryFn.md)

### Functions

- [and](fp.md#and)
- [boolToPredicate](fp.md#booltopredicate)
- [curry](fp.md#curry)
- [negate](fp.md#negate)
- [or](fp.md#or)
- [pipe](fp.md#pipe)
- [reverseArgs](fp.md#reverseargs)
- [reverseCurry](fp.md#reversecurry)
- [spread](fp.md#spread)
- [toPredicate](fp.md#topredicate)
- [xor](fp.md#xor)

## Functions

### and

▸ **and**<`T`\>(...`predicates`): (`param`: `T`) => `boolean`

Performs the and operation on a group of predicates
This returns a function that takes a parameter to pass to the predicates

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `...predicates` | `Predicate`<`T`\>[] | Predicates to and |

#### Returns

`fn`

▸ (`param`): `boolean`

##### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `T` |

##### Returns

`boolean`

#### Defined in

[fp/predicate.ts:16](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/predicate.ts#L16)

___

### boolToPredicate

▸ **boolToPredicate**(`b`): `Predicate`<`any`\>

Converts a boolean into a predicate that always returns the same value

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `b` | `boolean` | Boolean to return |

#### Returns

`Predicate`<`any`\>

#### Defined in

[fp/predicate.ts:73](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/predicate.ts#L73)

___

### curry

▸ **curry**<`T1`, `R`\>(`func`): [`CF1`](../interfaces/fp.CF1.md)<`T1`, `R`\>

Auto-curries a function

#### Type parameters

| Name |
| :------ |
| `T1` |
| `R` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`t1`: `T1`) => `R` | Function to auto-curry |

#### Returns

[`CF1`](../interfaces/fp.CF1.md)<`T1`, `R`\>

#### Defined in

[fp/curry.ts:55](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L55)

▸ **curry**<`T1`, `T2`, `R`\>(`func`): [`CF2`](../interfaces/fp.CF2.md)<`T1`, `T2`, `R`\>

Auto-curries a function

#### Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `R` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`t1`: `T1`, `t2`: `T2`) => `R` | Function to auto-curry |

#### Returns

[`CF2`](../interfaces/fp.CF2.md)<`T1`, `T2`, `R`\>

#### Defined in

[fp/curry.ts:56](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L56)

▸ **curry**<`T1`, `T2`, `T3`, `R`\>(`func`): [`CF3`](../interfaces/fp.CF3.md)<`T1`, `T2`, `T3`, `R`\>

Auto-curries a function

#### Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `T3` |
| `R` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`t1`: `T1`, `t2`: `T2`, `t3`: `T3`) => `R` | Function to auto-curry |

#### Returns

[`CF3`](../interfaces/fp.CF3.md)<`T1`, `T2`, `T3`, `R`\>

#### Defined in

[fp/curry.ts:57](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L57)

▸ **curry**<`T1`, `T2`, `T3`, `T4`, `R`\>(`func`): [`CF4`](../interfaces/fp.CF4.md)<`T1`, `T2`, `T3`, `T4`, `R`\>

Auto-curries a function

#### Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `T3` |
| `T4` |
| `R` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`t1`: `T1`, `t2`: `T2`, `t3`: `T3`, `t4`: `T4`) => `R` | Function to auto-curry |

#### Returns

[`CF4`](../interfaces/fp.CF4.md)<`T1`, `T2`, `T3`, `T4`, `R`\>

#### Defined in

[fp/curry.ts:58](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L58)

▸ **curry**<`T1`, `T2`, `T3`, `T4`, `T5`, `R`\>(`func`): [`CF5`](../interfaces/fp.CF5.md)<`T1`, `T2`, `T3`, `T4`, `T5`, `R`\>

Auto-curries a function

#### Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `T3` |
| `T4` |
| `T5` |
| `R` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`t1`: `T1`, `t2`: `T2`, `t3`: `T3`, `t4`: `T4`, `t5`: `T5`) => `R` | Function to auto-curry |

#### Returns

[`CF5`](../interfaces/fp.CF5.md)<`T1`, `T2`, `T3`, `T4`, `T5`, `R`\>

#### Defined in

[fp/curry.ts:59](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L59)

▸ **curry**<`T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `R`\>(`func`): [`CF6`](../interfaces/fp.CF6.md)<`T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `R`\>

Auto-curries a function

#### Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `T3` |
| `T4` |
| `T5` |
| `T6` |
| `R` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`t1`: `T1`, `t2`: `T2`, `t3`: `T3`, `t4`: `T4`, `t5`: `T5`, `t6`: `T6`) => `R` | Function to auto-curry |

#### Returns

[`CF6`](../interfaces/fp.CF6.md)<`T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `R`\>

#### Defined in

[fp/curry.ts:67](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L67)

▸ **curry**<`T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `T7`, `R`\>(`func`): [`CF7`](../interfaces/fp.CF7.md)<`T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `T7`, `R`\>

Auto-curries a function

#### Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `T3` |
| `T4` |
| `T5` |
| `T6` |
| `T7` |
| `R` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`t1`: `T1`, `t2`: `T2`, `t3`: `T3`, `t4`: `T4`, `t5`: `T5`, `t6`: `T6`, `t7`: `T7`) => `R` | Function to auto-curry |

#### Returns

[`CF7`](../interfaces/fp.CF7.md)<`T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `T7`, `R`\>

#### Defined in

[fp/curry.ts:76](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L76)

▸ **curry**(`func`): (...`args`: `any`[]) => `any`

Auto-curries a function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (...`args`: `any`[]) => `any` | Function to auto-curry |

#### Returns

`fn`

▸ (...`args`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `...args` | `any`[] |

##### Returns

`any`

#### Defined in

[fp/curry.ts:79](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L79)

___

### negate

▸ **negate**<`T`\>(`p1`): (`param`: `T`) => `boolean`

Negates a predicate
This returns a function that takes a parameter to pass to the predicates

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `p1` | `Predicate`<`T`\> | Predicate to negate |

#### Returns

`fn`

▸ (`param`): `boolean`

##### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `T` |

##### Returns

`boolean`

#### Defined in

[fp/predicate.ts:52](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/predicate.ts#L52)

___

### or

▸ **or**<`T`\>(...`predicates`): (`param`: `T`) => `boolean`

Performs the and operation on a group of predicates
This returns a function that takes a parameter to pass to the predicates

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `...predicates` | `Predicate`<`T`\>[] | Predicates to and |

#### Returns

`fn`

▸ (`param`): `boolean`

##### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `T` |

##### Returns

`boolean`

#### Defined in

[fp/predicate.ts:27](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/predicate.ts#L27)

___

### pipe

▸ **pipe**(`paramOrFunc`, ...`functions`): `any`

Pipes an input through a series of functions

If the first parameter is a function, then pipe() will return a function
that accepts an input to pipe. Otherwise, it will pipe the first parameter
through the provided functions

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `paramOrFunc` | `any` | Either the input to pipe or a function to pipe through |
| `...functions` | `Function`[] | Functions to pipe through |

#### Returns

`any`

#### Defined in

[fp/pipe.ts:18](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/pipe.ts#L18)

___

### reverseArgs

▸ **reverseArgs**(`func`): (...`args`: `any`[]) => `any`

Reverses the order arguments are passed to a function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | `any` | Function to reverse argument order for |

#### Returns

`fn`

Function that will reverse the arguments

▸ (...`args`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `...args` | `any`[] |

##### Returns

`any`

#### Defined in

[fp/reverseArgs.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/reverseArgs.ts#L11)

___

### reverseCurry

▸ **reverseCurry**(`func`): (...`args`: `any`[]) => `any`

Reverses the order of arguments and auto-curries a function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | `any` | Function to reverse the arguments for and auto-curry |

#### Returns

`fn`

▸ (...`args`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `...args` | `any`[] |

##### Returns

`any`

#### Defined in

[fp/reverseCurry.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/reverseCurry.ts#L12)

___

### spread

▸ **spread**(`t1`): [`CF1`](../interfaces/fp.CF1.md)<`any`, `any`\>

Takes a function and an array of arguments and spreads the array into function arguments

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `any` |

#### Returns

[`CF1`](../interfaces/fp.CF1.md)<`any`, `any`\>

Result of calling func(...args)

#### Defined in

[fp/curry.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L10)

▸ **spread**(`t1`, `t2`, ...`args`): `any`

Takes a function and an array of arguments and spreads the array into function arguments

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `t1` | `any` | - |
| `t2` | `any` | - |
| `...args` | `any`[] | Arguments to pass to func |

#### Returns

`any`

Result of calling func(...args)

#### Defined in

[fp/curry.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L11)

___

### toPredicate

▸ **toPredicate**<`T`\>(`func`): `Predicate`<`T`\>

Converts a function into a predicate
(Predicates are functions that take one input and return either true or false)

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`param`: `T`) => `any` | Function to transform |

#### Returns

`Predicate`<`T`\>

#### Defined in

[fp/predicate.ts:63](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/predicate.ts#L63)

___

### xor

▸ **xor**<`T`\>(...`predicates`): (`param`: `T`) => `boolean`

Performs the and operation on a group of predicates
This returns a function that takes a parameter to pass to the predicates

Will return true if only one predicate returns true, false otherwise

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `...predicates` | `Predicate`<`T`\>[] | Predicates to and |

#### Returns

`fn`

▸ (`param`): `boolean`

##### Parameters

| Name | Type |
| :------ | :------ |
| `param` | `T` |

##### Returns

`boolean`

#### Defined in

[fp/predicate.ts:41](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/predicate.ts#L41)
