[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / iterables

# Namespace: iterables

Functions for working with iterables

## Table of contents

### Interfaces

- [iAll](../interfaces/iterables.iAll.md)
- [iAny](../interfaces/iterables.iAny.md)
- [iChunk](../interfaces/iterables.iChunk.md)
- [iConcat](../interfaces/iterables.iConcat.md)
- [iContains](../interfaces/iterables.iContains.md)
- [iFill](../interfaces/iterables.iFill.md)
- [iFillAll](../interfaces/iterables.iFillAll.md)
- [iFillEnd](../interfaces/iterables.iFillEnd.md)
- [iFilter](../interfaces/iterables.iFilter.md)
- [iFind](../interfaces/iterables.iFind.md)
- [iFindOr](../interfaces/iterables.iFindOr.md)
- [iFlatMap](../interfaces/iterables.iFlatMap.md)
- [iForEach](../interfaces/iterables.iForEach.md)
- [iGroupBy](../interfaces/iterables.iGroupBy.md)
- [iGroupByKey](../interfaces/iterables.iGroupByKey.md)
- [iIndexBy](../interfaces/iterables.iIndexBy.md)
- [iIndexByKey](../interfaces/iterables.iIndexByKey.md)
- [iJoin](../interfaces/iterables.iJoin.md)
- [iLimit](../interfaces/iterables.iLimit.md)
- [iMap](../interfaces/iterables.iMap.md)
- [iReduce](../interfaces/iterables.iReduce.md)
- [iScan](../interfaces/iterables.iScan.md)
- [iSetOperation](../interfaces/iterables.iSetOperation.md)
- [iSkip](../interfaces/iterables.iSkip.md)
- [iTake](../interfaces/iterables.iTake.md)
- [iTakeWhile](../interfaces/iterables.iTakeWhile.md)
- [iTap](../interfaces/iterables.iTap.md)
- [iZip](../interfaces/iterables.iZip.md)

### Functions

- [all](iterables.md#all)
- [any](iterables.md#any)
- [chunk](iterables.md#chunk)
- [collectToArray](iterables.md#collecttoarray)
- [collectToList](iterables.md#collecttolist)
- [collectToSet](iterables.md#collecttoset)
- [concat](iterables.md#concat)
- [contains](iterables.md#contains)
- [containsEquals](iterables.md#containsequals)
- [difference](iterables.md#difference)
- [fill](iterables.md#fill)
- [fillAll](iterables.md#fillall)
- [fillEnd](iterables.md#fillend)
- [fillStart](iterables.md#fillstart)
- [filter](iterables.md#filter)
- [find](iterables.md#find)
- [first](iterables.md#first)
- [firstOr](iterables.md#firstor)
- [firstOrNull](iterables.md#firstornull)
- [flatMap](iterables.md#flatmap)
- [flatten](iterables.md#flatten)
- [forEach](iterables.md#foreach)
- [fromPairs](iterables.md#frompairs)
- [groupBy](iterables.md#groupby)
- [groupByKey](iterables.md#groupbykey)
- [head](iterables.md#head)
- [indexBy](iterables.md#indexby)
- [indexByKey](iterables.md#indexbykey)
- [intersect](iterables.md#intersect)
- [join](iterables.md#join)
- [last](iterables.md#last)
- [limit](iterables.md#limit)
- [map](iterables.md#map)
- [reduce](iterables.md#reduce)
- [removeFalsey](iterables.md#removefalsey)
- [removeTruthy](iterables.md#removetruthy)
- [reverse](iterables.md#reverse)
- [scan](iterables.md#scan)
- [skip](iterables.md#skip)
- [some](iterables.md#some)
- [take](iterables.md#take)
- [takeWhile](iterables.md#takewhile)
- [takeWhilePullPush](iterables.md#takewhilepullpush)
- [tap](iterables.md#tap)
- [toIterableOrEmpty](iterables.md#toiterableorempty)
- [union](iterables.md#union)
- [zip](iterables.md#zip)

## Functions

### all

▸ **all**(`predicate`, `iterable`): `boolean`

Applies a predicate to all elements of an iterable and ensures they all return true
Will return false as soon as the predicate returns false for any element

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `predicate` | `Predicate`<`any`\> | The predicate to execute for each element |
| `iterable` | `Iterable`<`any`\> | The iterable to iterate over |

#### Returns

`boolean`

returns true if the predicate returns true for all elements of the iterable

#### Defined in

[iterables/all.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/all.ts#L11)

▸ **all**(`predicate`): (`iterable`: `Iterable`<`any`\>) => `boolean`

Applies a predicate to all elements of an iterable and ensures they all return true
Will return false as soon as the predicate returns false for any element

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `predicate` | `Predicate`<`any`\> | The predicate to execute for each element |

#### Returns

`fn`

returns true if the predicate returns true for all elements of the iterable

▸ (`iterable`): `boolean`

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`boolean`

#### Defined in

[iterables/all.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/all.ts#L12)

___

### any

▸ **any**(`predicate`, `iterable`): `boolean`

Applies a predicate to elements of an iterable and returns true if any element returns true
Will return true as soon as the predicate returns true for any element

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `predicate` | `Predicate`<`any`\> | The predicate to execute for each element |
| `iterable` | `Iterable`<`any`\> | The iterable to iterate over |

#### Returns

`boolean`

returns true if the predicate returns true for any elements of the iterable

#### Defined in

[iterables/any.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/any.ts#L11)

▸ **any**(`predicate`): (`iterable`: `Iterable`<`any`\>) => `boolean`

Applies a predicate to elements of an iterable and returns true if any element returns true
Will return true as soon as the predicate returns true for any element

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `predicate` | `Predicate`<`any`\> | The predicate to execute for each element |

#### Returns

`fn`

returns true if the predicate returns true for any elements of the iterable

▸ (`iterable`): `boolean`

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`boolean`

#### Defined in

[iterables/any.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/any.ts#L12)

___

### chunk

▸ **chunk**(`size`, `array`): `Iterable`<`any`[]\>

Chuncks an iterable into groups of a specific size.
This is itself an iterable and will return each chunk lazily as needed

If the chunk size does not divide fully into the iterable size, the last chunk returned will be the remaining items (no padding)

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `size` | `number` | The maximum size for all chunks |
| `array` | `any` | - |

#### Returns

`Iterable`<`any`[]\>

returns an iterable of arrays of chunked elements

#### Defined in

[iterables/chunk.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/chunk.ts#L9)

▸ **chunk**(`size`): (`array`: `any`) => `Iterable`<`any`[]\>

Chuncks an iterable into groups of a specific size.
This is itself an iterable and will return each chunk lazily as needed

If the chunk size does not divide fully into the iterable size, the last chunk returned will be the remaining items (no padding)

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `size` | `number` | The maximum size for all chunks |

#### Returns

`fn`

returns an iterable of arrays of chunked elements

▸ (`array`): `Iterable`<`any`[]\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `array` | `any` |

##### Returns

`Iterable`<`any`[]\>

#### Defined in

[iterables/chunk.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/chunk.ts#L10)

___

### collectToArray

▸ **collectToArray**<`T`\>(`iterable`, `max?`): `T`[]

Converts an iterable into an array

The maximum number of elements to iterate over can optionally be provided as well

**`Kind`**

function

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `iterable` | `Iterable`<`T`\> | `undefined` | The iterable to iterate over |
| `max` | `number` | `Infinity` | (Optional, default Infinity) the maximum size of the resulting array |

#### Returns

`T`[]

returns the iterable as an array

#### Defined in

[iterables/collectToArray.ts:19](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/collectToArray.ts#L19)

___

### collectToList

▸ **collectToList**<`T`\>(`iterable`, `max?`): `List`<`T`\>

Converts an iterable into an immutable List

The maximum number of elements to iterate over can optionally be provided as well

**`Kind`**

function

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `iterable` | `Iterable`<`T`\> | `undefined` | The iterable to iterate over |
| `max` | `number` | `Infinity` | (Optional, default Infinity) the maximum size of the resulting array |

#### Returns

`List`<`T`\>

returns the iterable as an immutable list

#### Defined in

[iterables/collectToList.ts:21](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/collectToList.ts#L21)

___

### collectToSet

▸ **collectToSet**<`T`\>(`iterable`, `max?`): `Set`<`T`\>

Converts an iterable into a native Set

The maximum number of elements to iterate over can optionally be provided as well

**`Kind`**

function

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `iterable` | `Iterable`<`T`\> | `undefined` | The iterable to iterate over |
| `max` | `number` | `Infinity` | (Optional, default Infinity) the maximum size of the resulting array |

#### Returns

`Set`<`T`\>

returns the iterable as a Set

#### Defined in

[iterables/collectToSet.ts:19](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/collectToSet.ts#L19)

___

### concat

▸ **concat**(`iterableLeft`, `iterableRight`, ...`moreIterables`): `Iterable`<`any`\>

Concatenates the provided iterables

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `iterableLeft` | `Iterable`<`any`\> |
| `iterableRight` | `Iterable`<`any`\> |
| `...moreIterables` | `Iterable`<`any`\>[] |

#### Returns

`Iterable`<`any`\>

the resulting concatenation of the provided iterables

#### Defined in

[iterables/concat.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/concat.ts#L9)

▸ **concat**(`iterableLeft`, `iterableRight`): `Iterable`<`any`\>

Concatenates the provided iterables

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `iterableLeft` | `Iterable`<`any`\> |
| `iterableRight` | `Iterable`<`any`\> |

#### Returns

`Iterable`<`any`\>

the resulting concatenation of the provided iterables

#### Defined in

[iterables/concat.ts:14](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/concat.ts#L14)

▸ **concat**(`iterableLeft`): (`iterableRight`: `Iterable`<`any`\>, ...`moreIterables`: `Iterable`<`any`\>[]) => `Iterable`<`any`\>

Concatenates the provided iterables

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `iterableLeft` | `Iterable`<`any`\> |

#### Returns

`fn`

the resulting concatenation of the provided iterables

▸ (`iterableRight`, ...`moreIterables`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterableRight` | `Iterable`<`any`\> |
| `...moreIterables` | `Iterable`<`any`\>[] |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/concat.ts:15](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/concat.ts#L15)

▸ **concat**(`iterableLeft`): (`iterableRight`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Concatenates the provided iterables

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `iterableLeft` | `Iterable`<`any`\> |

#### Returns

`fn`

the resulting concatenation of the provided iterables

▸ (`iterableRight`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterableRight` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/concat.ts:19](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/concat.ts#L19)

___

### contains

▸ **contains**(`value`, `iterable`): `boolean`

Returns true if the value is identical to an element in the iterable

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `any` | The value to search for |
| `iterable` | `Iterable`<`any`\> | The iterable to iterate over |

#### Returns

`boolean`

whether or not the value was found

#### Defined in

[iterables/contains.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/contains.ts#L9)

▸ **contains**(`value`): (`iterable`: `Iterable`<`any`\>) => `boolean`

Returns true if the value is identical to an element in the iterable

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `any` | The value to search for |

#### Returns

`fn`

whether or not the value was found

▸ (`iterable`): `boolean`

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`boolean`

#### Defined in

[iterables/contains.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/contains.ts#L10)

___

### containsEquals

▸ **containsEquals**(`value`, `iterable`): `boolean`

Returns true if the value is in the iterable by checking using isEqual

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `any` | The value to search for |
| `iterable` | `Iterable`<`any`\> | The iterable to iterate over |

#### Returns

`boolean`

whether or not the value was found

#### Defined in

[iterables/contains.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/contains.ts#L9)

▸ **containsEquals**(`value`): (`iterable`: `Iterable`<`any`\>) => `boolean`

Returns true if the value is in the iterable by checking using isEqual

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `any` | The value to search for |

#### Returns

`fn`

whether or not the value was found

▸ (`iterable`): `boolean`

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`boolean`

#### Defined in

[iterables/contains.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/contains.ts#L10)

___

### difference

▸ **difference**(`iterable1`, `iterable2`, ...`iterables`): `Iterable`<`any`\>

Returns the items in the first iterable that are not in the other iterables
Can be thought of as performing the Set difference operator (A - B)

Note: It uses the Set has function to find the difference

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable1` | `Iterable`<`any`\> | The iterable that will provide values |
| `iterable2` | `Iterable`<`any`\> | An iterable with values to remove from iterable1 |
| `...iterables` | `Iterable`<`any`\>[] | Other iterables with values to remove from iterable1 |

#### Returns

`Iterable`<`any`\>

values from iterable1 not in the other iterables

#### Defined in

[iterables/difference.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/difference.ts#L10)

▸ **difference**(`iterable1`, `iterable2`): `Iterable`<`any`\>

Returns the items in the first iterable that are not in the other iterables
Can be thought of as performing the Set difference operator (A - B)

Note: It uses the Set has function to find the difference

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable1` | `Iterable`<`any`\> | The iterable that will provide values |
| `iterable2` | `Iterable`<`any`\> | An iterable with values to remove from iterable1 |

#### Returns

`Iterable`<`any`\>

values from iterable1 not in the other iterables

#### Defined in

[iterables/difference.ts:15](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/difference.ts#L15)

▸ **difference**(`iterable1`): (`iterable2`: `Iterable`<`any`\>, ...`iterables`: `Iterable`<`any`\>[]) => `Iterable`<`any`\>

Returns the items in the first iterable that are not in the other iterables
Can be thought of as performing the Set difference operator (A - B)

Note: It uses the Set has function to find the difference

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable1` | `Iterable`<`any`\> | The iterable that will provide values |

#### Returns

`fn`

values from iterable1 not in the other iterables

▸ (`iterable2`, ...`iterables`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable2` | `Iterable`<`any`\> |
| `...iterables` | `Iterable`<`any`\>[] |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/difference.ts:16](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/difference.ts#L16)

▸ **difference**(`iterable1`): (`iterable2`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Returns the items in the first iterable that are not in the other iterables
Can be thought of as performing the Set difference operator (A - B)

Note: It uses the Set has function to find the difference

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable1` | `Iterable`<`any`\> | The iterable that will provide values |

#### Returns

`fn`

values from iterable1 not in the other iterables

▸ (`iterable2`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable2` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/difference.ts:20](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/difference.ts#L20)

___

### fill

▸ **fill**(`start`, `end`, `value`, `iterable`): `Iterable`<`any`\>

Fills an iterable between the start and end indexes with a value

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `start` | `number` | The starting index to begin filling values |
| `end` | `number` | The exclusive ending index to stop filling values |
| `value` | `any` | The value to fill the iterable with |
| `iterable` | `Iterable`<`any`\> | The iterable to fill |

#### Returns

`Iterable`<`any`\>

An iterable with some values filled

#### Defined in

[iterables/fill.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/fill.ts#L9)

▸ **fill**(`start`, `end`, `value`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Fills an iterable between the start and end indexes with a value

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `start` | `number` | The starting index to begin filling values |
| `end` | `number` | The exclusive ending index to stop filling values |
| `value` | `any` | The value to fill the iterable with |

#### Returns

`fn`

An iterable with some values filled

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/fill.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/fill.ts#L10)

▸ **fill**(`start`, `end`): (`value`: `any`) => (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Fills an iterable between the start and end indexes with a value

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `start` | `number` | The starting index to begin filling values |
| `end` | `number` | The exclusive ending index to stop filling values |

#### Returns

`fn`

An iterable with some values filled

▸ (`value`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |

##### Returns

`fn`

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/fill.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/fill.ts#L11)

▸ **fill**(`start`): (`end`: `number`, `value`: `any`, `iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Fills an iterable between the start and end indexes with a value

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `start` | `number` | The starting index to begin filling values |

#### Returns

`fn`

An iterable with some values filled

▸ (`end`, `value`, `iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `end` | `number` |
| `value` | `any` |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/fill.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/fill.ts#L12)

▸ **fill**(`start`): (`end`: `number`, `value`: `any`) => (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Fills an iterable between the start and end indexes with a value

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `start` | `number` | The starting index to begin filling values |

#### Returns

`fn`

An iterable with some values filled

▸ (`end`, `value`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `end` | `number` |
| `value` | `any` |

##### Returns

`fn`

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/fill.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/fill.ts#L13)

▸ **fill**(`start`): (`end`: `number`) => (`value`: `any`, `iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Fills an iterable between the start and end indexes with a value

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `start` | `number` | The starting index to begin filling values |

#### Returns

`fn`

An iterable with some values filled

▸ (`end`): (`value`: `any`, `iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `end` | `number` |

##### Returns

`fn`

▸ (`value`, `iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/fill.ts:14](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/fill.ts#L14)

▸ **fill**(`start`): (`end`: `number`) => (`value`: `any`) => (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Fills an iterable between the start and end indexes with a value

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `start` | `number` | The starting index to begin filling values |

#### Returns

`fn`

An iterable with some values filled

▸ (`end`): (`value`: `any`) => (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `end` | `number` |

##### Returns

`fn`

▸ (`value`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |

##### Returns

`fn`

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/fill.ts:15](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/fill.ts#L15)

___

### fillAll

▸ **fillAll**(`value`, `iterable`): `Iterable`<`any`\>

Fills all values of an iterable with a provided value

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `any` | The value to fill the iterable with |
| `iterable` | `Iterable`<`any`\> | The iterable to fill |

#### Returns

`Iterable`<`any`\>

An iterable with it's values filled

#### Defined in

[iterables/fillAll.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/fillAll.ts#L9)

▸ **fillAll**(`value`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Fills all values of an iterable with a provided value

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `any` | The value to fill the iterable with |

#### Returns

`fn`

An iterable with it's values filled

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/fillAll.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/fillAll.ts#L10)

___

### fillEnd

▸ **fillEnd**(`end`, `value`, `iterable`): `Iterable`<`any`\>

Replaces all values in an iterable with a specified value after a provided index

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `end` | `number` | - |
| `value` | `any` | The value to fill the iterable with |
| `iterable` | `Iterable`<`any`\> | The iterable to fill |

#### Returns

`Iterable`<`any`\>

The resulting iterable

#### Defined in

[iterables/fillEnd.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/fillEnd.ts#L9)

▸ **fillEnd**(`end`, `value`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Replaces all values in an iterable with a specified value after a provided index

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `end` | `number` | - |
| `value` | `any` | The value to fill the iterable with |

#### Returns

`fn`

The resulting iterable

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/fillEnd.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/fillEnd.ts#L10)

▸ **fillEnd**(`end`): (`value`: `any`, `iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Replaces all values in an iterable with a specified value after a provided index

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `end` | `number` |

#### Returns

`fn`

The resulting iterable

▸ (`value`, `iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/fillEnd.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/fillEnd.ts#L11)

▸ **fillEnd**(`end`): (`value`: `any`) => (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Replaces all values in an iterable with a specified value after a provided index

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `end` | `number` |

#### Returns

`fn`

The resulting iterable

▸ (`value`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |

##### Returns

`fn`

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/fillEnd.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/fillEnd.ts#L12)

___

### fillStart

▸ **fillStart**(`end`, `value`, `iterable`): `Iterable`<`any`\>

Replaces all values in an iterable with a specified value before a provided index

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `end` | `number` | The exclusive ending index to stop filling values |
| `value` | `any` | The value to fill the iterable with |
| `iterable` | `Iterable`<`any`\> | The iterable to fill |

#### Returns

`Iterable`<`any`\>

The resulting iterable

#### Defined in

[iterables/fill.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/fill.ts#L12)

___

### filter

▸ **filter**(`predicate`, `iterable`): `Iterable`<`any`\>

Filters values from an iterable where a predicate returns false

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `predicate` | `Predicate`<`any`\> | The predicate to test values with |
| `iterable` | `Iterable`<`any`\> | The iterable to filter |

#### Returns

`Iterable`<`any`\>

The resulting iterable

#### Defined in

[iterables/filter.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/filter.ts#L10)

▸ **filter**(`predicate`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Filters values from an iterable where a predicate returns false

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `predicate` | `Predicate`<`any`\> | The predicate to test values with |

#### Returns

`fn`

The resulting iterable

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/filter.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/filter.ts#L11)

___

### find

▸ **find**(`predicate`, `iterable`): `Iterable`<`any`\>

Returns an iterable holding first element in an iterable for which a predicate returns true or an empty iterable if none is found

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `predicate` | `Predicate`<`any`\> | The predicate to test values with |
| `iterable` | `Iterable`<`any`\> | The iterable to filter |

#### Returns

`Iterable`<`any`\>

An iterable of one element if a match is found or an empty iterable otherwise

#### Defined in

[iterables/find.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/find.ts#L12)

▸ **find**(`predicate`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Returns an iterable holding first element in an iterable for which a predicate returns true or an empty iterable if none is found

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `predicate` | `Predicate`<`any`\> | The predicate to test values with |

#### Returns

`fn`

An iterable of one element if a match is found or an empty iterable otherwise

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/find.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/find.ts#L13)

___

### first

▸ **first**(`iterable`): `Iterable`<`any`\>

Alias for head

#### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

#### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/take.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/take.ts#L10)

___

### firstOr

▸ **firstOr**(`t1`): [`CF1`](../interfaces/fp.CF1.md)<`Iterable`<`any`\>, `any`\>

Returns the first element of an iterable or a default value if the iterable is empty

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `any` |

#### Returns

[`CF1`](../interfaces/fp.CF1.md)<`Iterable`<`any`\>, `any`\>

The first element of the iterable or defaultValue if it is empty

#### Defined in

[fp/curry.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L10)

▸ **firstOr**(`t1`, `t2`, ...`args`): `any`

Returns the first element of an iterable or a default value if the iterable is empty

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `any` |
| `t2` | `Iterable`<`any`\> |
| `...args` | `any`[] |

#### Returns

`any`

The first element of the iterable or defaultValue if it is empty

#### Defined in

[fp/curry.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L11)

___

### firstOrNull

▸ **firstOrNull**(`t1`, ...`args`): `any`

Returns the first element of an iterable or a null if the iterable is empty

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `Iterable`<`any`\> |
| `...args` | `any`[] |

#### Returns

`any`

The first element of the iterable or null if it is empty

#### Defined in

[fp/curry.ts:6](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L6)

___

### flatMap

▸ **flatMap**(`func`, `iterable`): `Iterable`<`any`\>

Performs map(func, iterable) followed by flatten(iterable) on an iterable

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`v`: `any`) => `any` | A function to apply to each element of the iterable |
| `iterable` | `Iterable`<`any`\> | The iterable to iterate over |

#### Returns

`Iterable`<`any`\>

The resulting iterable

#### Defined in

[iterables/flatMap.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/flatMap.ts#L10)

▸ **flatMap**(`func`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Performs map(func, iterable) followed by flatten(iterable) on an iterable

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`v`: `any`) => `any` | A function to apply to each element of the iterable |

#### Returns

`fn`

The resulting iterable

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/flatMap.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/flatMap.ts#L11)

___

### flatten

▸ **flatten**(`iterable`): `Iterable`<`any`\>

Takes an iterable of iterables and concatenates the inner iterables into one iterable

E.g.
[[1, 2, 3], [4, 5, 6]] => [1, 2, 3, 4, 5, 6]

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable` | `Iterable`<`any`\> | The iterable to flatten |

#### Returns

`Iterable`<`any`\>

The resulting iterable

#### Defined in

[iterables/flatten.ts:20](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/flatten.ts#L20)

___

### forEach

▸ **forEach**(`t1`): [`CF1`](../interfaces/fp.CF1.md)<`Iterable`<`any`\>, `void`\>

Calls a function on each element of an iterable

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | (`v`: `any`) => `void` |

#### Returns

[`CF1`](../interfaces/fp.CF1.md)<`Iterable`<`any`\>, `void`\>

Does not return

#### Defined in

[fp/curry.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L10)

▸ **forEach**(`t1`, `t2`, ...`args`): `void`

Calls a function on each element of an iterable

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | (`v`: `any`) => `void` |
| `t2` | `Iterable`<`any`\> |
| `...args` | `any`[] |

#### Returns

`void`

Does not return

#### Defined in

[fp/curry.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L11)

___

### fromPairs

▸ **fromPairs**<`T`\>(`pairs`): `Object`

Takes an iterable of key value pairs and returns an object formed from those key-value pairs

E.g.
[['a', 5], ['b', 6]] becomes [{a: 5, b: 6}]

**`Generator`**

**`Kind`**

function

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `pairs` | `Iterable`<[`StrOrNum`, `T`]\> | An iterable of key-value pairs |

#### Returns

`Object`

An object

#### Defined in

[iterables/fromPairs.ts:23](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/fromPairs.ts#L23)

___

### groupBy

▸ **groupBy**(`func`, `iterable`): `Object`

Takes an iterable of values and groups them by the result of calling a function

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`val`: `any`) => `string` \| `number` | A function to call to group elements by |
| `iterable` | `Iterable`<`any`\> | An iterable to iterate over |

#### Returns

`Object`

An object where the result of func is the key and an array of elements that provided that key is the value

#### Defined in

[iterables/groupBy.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/groupBy.ts#L10)

▸ **groupBy**(`func`): (`iterable`: `Iterable`<`any`\>) => { `[key: string]`: `any`[];  }

Takes an iterable of values and groups them by the result of calling a function

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`val`: `any`) => `string` \| `number` | A function to call to group elements by |

#### Returns

`fn`

An object where the result of func is the key and an array of elements that provided that key is the value

▸ (`iterable`): `Object`

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Object`

#### Defined in

[iterables/groupBy.ts:14](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/groupBy.ts#L14)

___

### groupByKey

▸ **groupByKey**(`key`, `iterable`): `Object`

Takes an iterable of values and groups them by a key

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` | The key to group by |
| `iterable` | `Iterable`<`any`\> | An iterable to iterate over |

#### Returns

`Object`

An object where the result of func is the key and an array of elements that provided that key is the value

#### Defined in

[iterables/groupBy.ts:47](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/groupBy.ts#L47)

▸ **groupByKey**(`key`): (`iterable`: `Iterable`<`any`\>) => { `[key: string]`: `any`[];  }

Takes an iterable of values and groups them by a key

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` | The key to group by |

#### Returns

`fn`

An object where the result of func is the key and an array of elements that provided that key is the value

▸ (`iterable`): `Object`

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Object`

#### Defined in

[iterables/groupBy.ts:48](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/groupBy.ts#L48)

___

### head

▸ **head**(`iterable`): `Iterable`<`any`\>

Returns an iterable of the first element in an iterable or an empty iterable if the provided iterable is empty

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable` | `Iterable`<`any`\> | The iterable to grab the first element of |

#### Returns

`Iterable`<`any`\>

The first element of an iterable if it is not empty or an empty iterable otherwwise

#### Defined in

[iterables/take.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/take.ts#L10)

___

### indexBy

▸ **indexBy**(`func`, `iterable`): `Object`

Takes an iterable of values and indexes them by the result of a function

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`val`: `any`) => `string` \| `number` | The function to index by |
| `iterable` | `Iterable`<`any`\> | An iterable to iterate over |

#### Returns

`Object`

An object where the result of func is the key and an array of elements that provided that key is the value

#### Defined in

[iterables/groupBy.ts:74](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/groupBy.ts#L74)

▸ **indexBy**(`func`): (`iterable`: `Iterable`<`any`\>) => { `[key: string]`: `any`;  }

Takes an iterable of values and indexes them by the result of a function

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`val`: `any`) => `string` \| `number` | The function to index by |

#### Returns

`fn`

An object where the result of func is the key and an array of elements that provided that key is the value

▸ (`iterable`): `Object`

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Object`

#### Defined in

[iterables/groupBy.ts:78](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/groupBy.ts#L78)

___

### indexByKey

▸ **indexByKey**(`key`, `iterable`): `Object`

Takes an iterable of values and indexes them by a key

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` | The key to index by |
| `iterable` | `Iterable`<`any`\> | An iterable to iterate over |

#### Returns

`Object`

An object where the result of func is the key and an array of elements that provided that key is the value

#### Defined in

[iterables/groupBy.ts:111](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/groupBy.ts#L111)

▸ **indexByKey**(`key`): (`iterable`: `Iterable`<`any`\>) => { `[key: string]`: `any`;  }

Takes an iterable of values and indexes them by a key

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `key` | `string` \| `number` | The key to index by |

#### Returns

`fn`

An object where the result of func is the key and an array of elements that provided that key is the value

▸ (`iterable`): `Object`

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Object`

#### Defined in

[iterables/groupBy.ts:112](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/groupBy.ts#L112)

___

### intersect

▸ **intersect**(`iterable1`, `iterable2`, ...`iterables`): `Iterable`<`any`\>

Returns only the items that appear in all iterables

Note: It uses the Set.has function to find matches

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable1` | `Iterable`<`any`\> | The first iterable to get values from |
| `iterable2` | `Iterable`<`any`\> | The second iterable to get values from |
| `...iterables` | `Iterable`<`any`\>[] | Other iterables |

#### Returns

`Iterable`<`any`\>

values that only occur in all provided iterables

#### Defined in

[iterables/difference.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/difference.ts#L10)

▸ **intersect**(`iterable1`, `iterable2`): `Iterable`<`any`\>

Returns only the items that appear in all iterables

Note: It uses the Set.has function to find matches

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable1` | `Iterable`<`any`\> | The first iterable to get values from |
| `iterable2` | `Iterable`<`any`\> | The second iterable to get values from |

#### Returns

`Iterable`<`any`\>

values that only occur in all provided iterables

#### Defined in

[iterables/difference.ts:15](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/difference.ts#L15)

▸ **intersect**(`iterable1`): (`iterable2`: `Iterable`<`any`\>, ...`iterables`: `Iterable`<`any`\>[]) => `Iterable`<`any`\>

Returns only the items that appear in all iterables

Note: It uses the Set.has function to find matches

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable1` | `Iterable`<`any`\> | The first iterable to get values from |

#### Returns

`fn`

values that only occur in all provided iterables

▸ (`iterable2`, ...`iterables`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable2` | `Iterable`<`any`\> |
| `...iterables` | `Iterable`<`any`\>[] |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/difference.ts:16](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/difference.ts#L16)

▸ **intersect**(`iterable1`): (`iterable2`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Returns only the items that appear in all iterables

Note: It uses the Set.has function to find matches

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable1` | `Iterable`<`any`\> | The first iterable to get values from |

#### Returns

`fn`

values that only occur in all provided iterables

▸ (`iterable2`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable2` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/difference.ts:20](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/difference.ts#L20)

___

### join

▸ **join**(`separator`, `iterable`): `string`

Returns the elements of an iterable as a string joined by the separator

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `separator` | `string` | The string to join elements with |
| `iterable` | `Iterable`<`any`\> | The iterable to iterate over |

#### Returns

`string`

The resulting string

#### Defined in

[iterables/join.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/join.ts#L10)

▸ **join**(`separator`): (`iterable`: `Iterable`<`any`\>) => `string`

Returns the elements of an iterable as a string joined by the separator

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `separator` | `string` | The string to join elements with |

#### Returns

`fn`

The resulting string

▸ (`iterable`): `string`

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`string`

#### Defined in

[iterables/join.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/join.ts#L11)

___

### last

▸ **last**<`T`\>(`iterable`): `T`

Returns the last element of an iterable

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable` | `Iterable`<`T`\> | The iterable to iterate over |

#### Returns

`T`

#### Defined in

[iterables/last.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/last.ts#L11)

___

### limit

▸ **limit**(`max`, `iterable`): `Iterable`<`any`\>

Returns an iterable with no more than a provided maximum number of elements

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `max` | `number` | The maximum number of elements in the resulting iterable |
| `iterable` | `Iterable`<`any`\> | The iterable to iterate over |

#### Returns

`Iterable`<`any`\>

The resulting iterable

#### Defined in

[iterables/limit.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/limit.ts#L9)

▸ **limit**(`max`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Returns an iterable with no more than a provided maximum number of elements

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `max` | `number` | The maximum number of elements in the resulting iterable |

#### Returns

`fn`

The resulting iterable

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/limit.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/limit.ts#L10)

___

### map

▸ **map**(`func`, `iterable`): `Iterable`<`any`\>

Calls a function on each element of an iterable and returns an iterable of those results

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`v`: `any`) => `any` | The funciton to call on each element of an iterable |
| `iterable` | `Iterable`<`any`\> | The iterable to iterate over |

#### Returns

`Iterable`<`any`\>

The resulting iterable

#### Defined in

[iterables/map.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/map.ts#L9)

▸ **map**(`func`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Calls a function on each element of an iterable and returns an iterable of those results

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`v`: `any`) => `any` | The funciton to call on each element of an iterable |

#### Returns

`fn`

The resulting iterable

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/map.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/map.ts#L10)

___

### reduce

▸ **reduce**(`func`, `startAccumulator`, `iterable`): `any`

Accumulates the values of an iterable by calling a function and passing in the last accumulate and current value

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`accumulation`: `any`, `currentElement`: `any`) => `any` | The reducer function |
| `startAccumulator` | `any` | - |
| `iterable` | `Iterable`<`any`\> | The iterable to iterate over |

#### Returns

`any`

The resulting accumulation

#### Defined in

[iterables/reduce.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/reduce.ts#L9)

▸ **reduce**(`func`, `startAccumulator`): (`iterable`: `Iterable`<`any`\>) => `any`

Accumulates the values of an iterable by calling a function and passing in the last accumulate and current value

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`accumulation`: `any`, `currentElement`: `any`) => `any` | The reducer function |
| `startAccumulator` | `any` | - |

#### Returns

`fn`

The resulting accumulation

▸ (`iterable`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`any`

#### Defined in

[iterables/reduce.ts:14](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/reduce.ts#L14)

▸ **reduce**(`func`): (`startAccumulator`: `any`, `iterable`: `Iterable`<`any`\>) => `any`

Accumulates the values of an iterable by calling a function and passing in the last accumulate and current value

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`accumulation`: `any`, `currentElement`: `any`) => `any` | The reducer function |

#### Returns

`fn`

The resulting accumulation

▸ (`startAccumulator`, `iterable`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `startAccumulator` | `any` |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`any`

#### Defined in

[iterables/reduce.ts:17](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/reduce.ts#L17)

▸ **reduce**(`func`): (`startAccumulator`: `any`) => (`iterable`: `Iterable`<`any`\>) => `any`

Accumulates the values of an iterable by calling a function and passing in the last accumulate and current value

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`accumulation`: `any`, `currentElement`: `any`) => `any` | The reducer function |

#### Returns

`fn`

The resulting accumulation

▸ (`startAccumulator`): (`iterable`: `Iterable`<`any`\>) => `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `startAccumulator` | `any` |

##### Returns

`fn`

▸ (`iterable`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`any`

#### Defined in

[iterables/reduce.ts:21](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/reduce.ts#L21)

___

### removeFalsey

▸ **removeFalsey**<`T`\>(`iterable`): `Iterable`<`T`\>

Removes all falsey values from an iterable

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable` | `Iterable`<`T`\> | The iterable to iterate over |

#### Returns

`Iterable`<`T`\>

The resulting iterable

#### Defined in

[iterables/removeFalsey.ts:14](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/removeFalsey.ts#L14)

___

### removeTruthy

▸ **removeTruthy**<`T`\>(`iterable`): `Iterable`<`T`\>

Removes all truthy values from an iterable

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable` | `Iterable`<`T`\> | The iterable to iterate over |

#### Returns

`Iterable`<`T`\>

The resulting iterable

#### Defined in

[iterables/removeTruthy.ts:14](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/removeTruthy.ts#L14)

___

### reverse

▸ **reverse**<`T`\>(`iterable`): `Iterable`<`T`\>

Reverses the elements of an iterable

**`Generator`**

**`Kind`**

function

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable` | `Iterable`<`T`\> | The iterable to iterate over |

#### Returns

`Iterable`<`T`\>

The resulting iterable

#### Defined in

[iterables/reverse.ts:16](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/reverse.ts#L16)

___

### scan

▸ **scan**(`func`, `startAccumulator`, `iterable`): `Iterable`<`any`\>

Accumulates the values of an iterable by calling a function and returns all intermediate results

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`accumulation`: `any`, `currentElement`: `any`) => `any` | The reducer function |
| `startAccumulator` | `any` | - |
| `iterable` | `Iterable`<`any`\> | The iterable to iterate over |

#### Returns

`Iterable`<`any`\>

An iterable of all intermediate accumulations

#### Defined in

[iterables/scan.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/scan.ts#L9)

▸ **scan**(`func`, `startAccumulator`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Accumulates the values of an iterable by calling a function and returns all intermediate results

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`accumulation`: `any`, `currentElement`: `any`) => `any` | The reducer function |
| `startAccumulator` | `any` | - |

#### Returns

`fn`

An iterable of all intermediate accumulations

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/scan.ts:14](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/scan.ts#L14)

▸ **scan**(`func`): (`startAccumulator`: `any`, `iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Accumulates the values of an iterable by calling a function and returns all intermediate results

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`accumulation`: `any`, `currentElement`: `any`) => `any` | The reducer function |

#### Returns

`fn`

An iterable of all intermediate accumulations

▸ (`startAccumulator`, `iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `startAccumulator` | `any` |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/scan.ts:17](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/scan.ts#L17)

▸ **scan**(`func`): (`startAccumulator`: `any`) => (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Accumulates the values of an iterable by calling a function and returns all intermediate results

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`accumulation`: `any`, `currentElement`: `any`) => `any` | The reducer function |

#### Returns

`fn`

An iterable of all intermediate accumulations

▸ (`startAccumulator`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `startAccumulator` | `any` |

##### Returns

`fn`

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/scan.ts:21](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/scan.ts#L21)

___

### skip

▸ **skip**(`n`, `iterable`): `Iterable`<`any`\>

Skips the first n values of an iterable

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `n` | `number` | The number of elements to skip |
| `iterable` | `Iterable`<`any`\> | The iterable to iterate over |

#### Returns

`Iterable`<`any`\>

The resulting iterable

#### Defined in

[iterables/skip.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/skip.ts#L9)

▸ **skip**(`n`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Skips the first n values of an iterable

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `n` | `number` | The number of elements to skip |

#### Returns

`fn`

The resulting iterable

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/skip.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/skip.ts#L10)

___

### some

▸ **some**(`predicate`, `iterable`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `predicate` | `Predicate`<`any`\> |
| `iterable` | `Iterable`<`any`\> |

#### Returns

`boolean`

#### Defined in

[iterables/any.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/any.ts#L11)

▸ **some**(`predicate`): (`iterable`: `Iterable`<`any`\>) => `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `predicate` | `Predicate`<`any`\> |

#### Returns

`fn`

▸ (`iterable`): `boolean`

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`boolean`

#### Defined in

[iterables/any.ts:12](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/any.ts#L12)

___

### take

▸ **take**(`n`, `iterable`): `Iterable`<`any`\>

Keeps only the first n values of an iterable

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `n` | `number` | The number of elements to keep |
| `iterable` | `Iterable`<`any`\> | The iterable to iterate over |

#### Returns

`Iterable`<`any`\>

The resulting iterable

#### Defined in

[iterables/take.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/take.ts#L9)

▸ **take**(`n`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Keeps only the first n values of an iterable

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `n` | `number` | The number of elements to keep |

#### Returns

`fn`

The resulting iterable

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/take.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/take.ts#L10)

___

### takeWhile

▸ **takeWhile**(`whileFunc`, `iterable`): `Iterable`<`any`\>

Keeps elements from an iterable until the provided function returns a falsey value or terminates (if it is an iterable)

If whileFunc is a function, the current value will be passed in each time

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `whileFunc` | `Predicate`<`any`\> \| `Iterable`<`any`\> | Used to determine when to stop returning elements from iterable |
| `iterable` | `Iterable`<`any`\> | The iterable to iterate over |

#### Returns

`Iterable`<`any`\>

The resulting iterable

#### Defined in

[iterables/takeWhile.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/takeWhile.ts#L10)

▸ **takeWhile**(`whileFunc`): (`iterable`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Keeps elements from an iterable until the provided function returns a falsey value or terminates (if it is an iterable)

If whileFunc is a function, the current value will be passed in each time

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `whileFunc` | `Predicate`<`any`\> \| `Iterable`<`any`\> | Used to determine when to stop returning elements from iterable |

#### Returns

`fn`

The resulting iterable

▸ (`iterable`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/takeWhile.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/takeWhile.ts#L11)

___

### takeWhilePullPush

▸ **takeWhilePullPush**(`t1`): [`CF1`](../interfaces/fp.CF1.md)<`Iterable`<`any`\>, `Iterable`<`any`\>\>

Keeps elements from an iterable until the provided generator returns a falsey value
After each step of pulling a value from the while generator, a value will be passed into the while generator

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `Iterable`<`any`\> |

#### Returns

[`CF1`](../interfaces/fp.CF1.md)<`Iterable`<`any`\>, `Iterable`<`any`\>\>

The resulting iterable

#### Defined in

[fp/curry.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L10)

▸ **takeWhilePullPush**(`t1`, `t2`, ...`args`): `Iterable`<`any`\>

Keeps elements from an iterable until the provided generator returns a falsey value
After each step of pulling a value from the while generator, a value will be passed into the while generator

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `Iterable`<`any`\> |
| `t2` | `Iterable`<`any`\> |
| `...args` | `any`[] |

#### Returns

`Iterable`<`any`\>

The resulting iterable

#### Defined in

[fp/curry.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L11)

___

### tap

▸ **tap**(`func`, `array`): `Iterable`<`any`\>

Calls a function for each element of an iterable and then passes the elmenet on

Often used for creating side effects (such as logging) while in the middle of processing an iterable

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`val`: `any`) => `void` | Function to call on each element |
| `array` | `Iterable`<`any`\> | - |

#### Returns

`Iterable`<`any`\>

An iterable with the values passed through

#### Defined in

[iterables/tap.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/tap.ts#L9)

▸ **tap**(`func`): (`array`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Calls a function for each element of an iterable and then passes the elmenet on

Often used for creating side effects (such as logging) while in the middle of processing an iterable

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `func` | (`val`: `any`) => `void` | Function to call on each element |

#### Returns

`fn`

An iterable with the values passed through

▸ (`array`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `array` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/tap.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/tap.ts#L10)

___

### toIterableOrEmpty

▸ **toIterableOrEmpty**<`T`\>(`param`): `Iterable`<`T`\>

If the parameter is an iterable it is returned, otherwise an empty array is returned

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `param` | `any` | An object to check if it is an iterable |

#### Returns

`Iterable`<`T`\>

#### Defined in

[iterables/toIterableOrEmpty.ts:13](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/toIterableOrEmpty.ts#L13)

___

### union

▸ **union**(`iterable1`, `iterable2`, ...`iterables`): `Iterable`<`any`\>

Returns all the items in any iterable at most once

Note: It uses the Set.has function to make occurences unique

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable1` | `Iterable`<`any`\> | The first iterable to get values from |
| `iterable2` | `Iterable`<`any`\> | The second iterable to get values from |
| `...iterables` | `Iterable`<`any`\>[] | Other iterables |

#### Returns

`Iterable`<`any`\>

values from all provided iterables

#### Defined in

[iterables/difference.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/difference.ts#L10)

▸ **union**(`iterable1`, `iterable2`): `Iterable`<`any`\>

Returns all the items in any iterable at most once

Note: It uses the Set.has function to make occurences unique

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable1` | `Iterable`<`any`\> | The first iterable to get values from |
| `iterable2` | `Iterable`<`any`\> | The second iterable to get values from |

#### Returns

`Iterable`<`any`\>

values from all provided iterables

#### Defined in

[iterables/difference.ts:15](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/difference.ts#L15)

▸ **union**(`iterable1`): (`iterable2`: `Iterable`<`any`\>, ...`iterables`: `Iterable`<`any`\>[]) => `Iterable`<`any`\>

Returns all the items in any iterable at most once

Note: It uses the Set.has function to make occurences unique

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable1` | `Iterable`<`any`\> | The first iterable to get values from |

#### Returns

`fn`

values from all provided iterables

▸ (`iterable2`, ...`iterables`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable2` | `Iterable`<`any`\> |
| `...iterables` | `Iterable`<`any`\>[] |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/difference.ts:16](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/difference.ts#L16)

▸ **union**(`iterable1`): (`iterable2`: `Iterable`<`any`\>) => `Iterable`<`any`\>

Returns all the items in any iterable at most once

Note: It uses the Set.has function to make occurences unique

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `iterable1` | `Iterable`<`any`\> | The first iterable to get values from |

#### Returns

`fn`

values from all provided iterables

▸ (`iterable2`): `Iterable`<`any`\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterable2` | `Iterable`<`any`\> |

##### Returns

`Iterable`<`any`\>

#### Defined in

[iterables/difference.ts:20](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/difference.ts#L20)

___

### zip

▸ **zip**(`iterableLeft`, `iterableRight`, ...`moreIterables`): `Iterable`<`any`[]\>

Takes the corresponding elements of each iterable based on index and groups them

E.g.
`zip([1, 2, 3], ['a', 'b', 'c'], [4, 5, 6]) => [[1, 'a', 4], [2, 'b', 5], [3, 'c', 6]]`

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `iterableLeft` | `Iterable`<`any`\> |
| `iterableRight` | `Iterable`<`any`\> |
| `...moreIterables` | `Iterable`<`any`\>[] |

#### Returns

`Iterable`<`any`[]\>

values from all provided iterables grouped by index

#### Defined in

[iterables/zip.ts:9](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/zip.ts#L9)

▸ **zip**(`iterableLeft`, `iterableRight`): `Iterable`<[`any`, `any`]\>

Takes the corresponding elements of each iterable based on index and groups them

E.g.
`zip([1, 2, 3], ['a', 'b', 'c'], [4, 5, 6]) => [[1, 'a', 4], [2, 'b', 5], [3, 'c', 6]]`

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `iterableLeft` | `Iterable`<`any`\> |
| `iterableRight` | `Iterable`<`any`\> |

#### Returns

`Iterable`<[`any`, `any`]\>

values from all provided iterables grouped by index

#### Defined in

[iterables/zip.ts:14](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/zip.ts#L14)

▸ **zip**(`iterableLeft`): (`iterableRight`: `Iterable`<`any`\>, ...`moreIterables`: `Iterable`<`any`\>[]) => `Iterable`<`any`[]\>

Takes the corresponding elements of each iterable based on index and groups them

E.g.
`zip([1, 2, 3], ['a', 'b', 'c'], [4, 5, 6]) => [[1, 'a', 4], [2, 'b', 5], [3, 'c', 6]]`

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `iterableLeft` | `Iterable`<`any`\> |

#### Returns

`fn`

values from all provided iterables grouped by index

▸ (`iterableRight`, ...`moreIterables`): `Iterable`<`any`[]\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterableRight` | `Iterable`<`any`\> |
| `...moreIterables` | `Iterable`<`any`\>[] |

##### Returns

`Iterable`<`any`[]\>

#### Defined in

[iterables/zip.ts:15](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/zip.ts#L15)

▸ **zip**(`iterableLeft`): (`iterableRight`: `Iterable`<`any`\>) => `Iterable`<[`any`, `any`]\>

Takes the corresponding elements of each iterable based on index and groups them

E.g.
`zip([1, 2, 3], ['a', 'b', 'c'], [4, 5, 6]) => [[1, 'a', 4], [2, 'b', 5], [3, 'c', 6]]`

**`Generator`**

**`Kind`**

function

#### Parameters

| Name | Type |
| :------ | :------ |
| `iterableLeft` | `Iterable`<`any`\> |

#### Returns

`fn`

values from all provided iterables grouped by index

▸ (`iterableRight`): `Iterable`<[`any`, `any`]\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `iterableRight` | `Iterable`<`any`\> |

##### Returns

`Iterable`<[`any`, `any`]\>

#### Defined in

[iterables/zip.ts:19](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/iterables/zip.ts#L19)
