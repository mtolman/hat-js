[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / [fp](../modules/fp.md) / CF3

# Interface: CF3<T1, T2, T3, R\>

[fp](../modules/fp.md).CF3

## Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `T3` |
| `R` |

## Callable

### CF3

▸ **CF3**(`t1`): [`CF2`](fp.CF2.md)<`T2`, `T3`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |

#### Returns

[`CF2`](fp.CF2.md)<`T2`, `T3`, `R`\>

#### Defined in

[fp/curry.ts:15](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L15)

### CF3

▸ **CF3**(`t1`, `t2`): [`CF1`](fp.CF1.md)<`T3`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |

#### Returns

[`CF1`](fp.CF1.md)<`T3`, `R`\>

#### Defined in

[fp/curry.ts:16](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L16)

### CF3

▸ **CF3**(`t1`, `t2`, `t3`, ...`args`): `R`

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |
| `t3` | `T3` |
| `...args` | `any`[] |

#### Returns

`R`

#### Defined in

[fp/curry.ts:17](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L17)
