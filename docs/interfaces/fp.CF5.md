[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / [fp](../modules/fp.md) / CF5

# Interface: CF5<T1, T2, T3, T4, T5, R\>

[fp](../modules/fp.md).CF5

## Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `T3` |
| `T4` |
| `T5` |
| `R` |

## Callable

### CF5

▸ **CF5**(`t1`): [`CF4`](fp.CF4.md)<`T2`, `T3`, `T4`, `T5`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |

#### Returns

[`CF4`](fp.CF4.md)<`T2`, `T3`, `T4`, `T5`, `R`\>

#### Defined in

[fp/curry.ts:28](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L28)

### CF5

▸ **CF5**(`t1`, `t2`): [`CF3`](fp.CF3.md)<`T3`, `T4`, `T5`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |

#### Returns

[`CF3`](fp.CF3.md)<`T3`, `T4`, `T5`, `R`\>

#### Defined in

[fp/curry.ts:29](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L29)

### CF5

▸ **CF5**(`t1`, `t2`, `t3`): [`CF2`](fp.CF2.md)<`T4`, `T5`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |
| `t3` | `T3` |

#### Returns

[`CF2`](fp.CF2.md)<`T4`, `T5`, `R`\>

#### Defined in

[fp/curry.ts:30](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L30)

### CF5

▸ **CF5**(`t1`, `t2`, `t3`, `t4`): [`CF1`](fp.CF1.md)<`T5`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |
| `t3` | `T3` |
| `t4` | `T4` |

#### Returns

[`CF1`](fp.CF1.md)<`T5`, `R`\>

#### Defined in

[fp/curry.ts:31](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L31)

### CF5

▸ **CF5**(`t1`, `t2`, `t3`, `t4`, `t5`, ...`args`): `R`

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |
| `t3` | `T3` |
| `t4` | `T4` |
| `t5` | `T5` |
| `...args` | `any`[] |

#### Returns

`R`

#### Defined in

[fp/curry.ts:32](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L32)
