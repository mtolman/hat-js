[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / [fp](../modules/fp.md) / CF6

# Interface: CF6<T1, T2, T3, T4, T5, T6, R\>

[fp](../modules/fp.md).CF6

## Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `T3` |
| `T4` |
| `T5` |
| `T6` |
| `R` |

## Callable

### CF6

▸ **CF6**(`t1`): [`CF5`](fp.CF5.md)<`T2`, `T3`, `T4`, `T5`, `T6`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |

#### Returns

[`CF5`](fp.CF5.md)<`T2`, `T3`, `T4`, `T5`, `T6`, `R`\>

#### Defined in

[fp/curry.ts:36](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L36)

### CF6

▸ **CF6**(`t1`, `t2`): [`CF4`](fp.CF4.md)<`T3`, `T4`, `T5`, `T6`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |

#### Returns

[`CF4`](fp.CF4.md)<`T3`, `T4`, `T5`, `T6`, `R`\>

#### Defined in

[fp/curry.ts:37](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L37)

### CF6

▸ **CF6**(`t1`, `t2`, `t3`): [`CF3`](fp.CF3.md)<`T4`, `T5`, `T6`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |
| `t3` | `T3` |

#### Returns

[`CF3`](fp.CF3.md)<`T4`, `T5`, `T6`, `R`\>

#### Defined in

[fp/curry.ts:38](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L38)

### CF6

▸ **CF6**(`t1`, `t2`, `t3`, `t4`): [`CF2`](fp.CF2.md)<`T5`, `T6`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |
| `t3` | `T3` |
| `t4` | `T4` |

#### Returns

[`CF2`](fp.CF2.md)<`T5`, `T6`, `R`\>

#### Defined in

[fp/curry.ts:39](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L39)

### CF6

▸ **CF6**(`t1`, `t2`, `t3`, `t4`, `t5`): [`CF1`](fp.CF1.md)<`T6`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |
| `t3` | `T3` |
| `t4` | `T4` |
| `t5` | `T5` |

#### Returns

[`CF1`](fp.CF1.md)<`T6`, `R`\>

#### Defined in

[fp/curry.ts:40](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L40)

### CF6

▸ **CF6**(`t1`, `t2`, `t3`, `t4`, `t5`, `t6`, ...`args`): `R`

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |
| `t3` | `T3` |
| `t4` | `T4` |
| `t5` | `T5` |
| `t6` | `T6` |
| `...args` | `any`[] |

#### Returns

`R`

#### Defined in

[fp/curry.ts:41](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L41)
