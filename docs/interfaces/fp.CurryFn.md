[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / [fp](../modules/fp.md) / CurryFn

# Interface: CurryFn

[fp](../modules/fp.md).CurryFn

## Callable

### CurryFn

▸ **CurryFn**<`T1`, `R`\>(`func`): [`CF1`](fp.CF1.md)<`T1`, `R`\>

#### Type parameters

| Name |
| :------ |
| `T1` |
| `R` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `func` | (`t1`: `T1`) => `R` |

#### Returns

[`CF1`](fp.CF1.md)<`T1`, `R`\>

#### Defined in

[fp/curry.ts:55](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L55)

### CurryFn

▸ **CurryFn**<`T1`, `T2`, `R`\>(`func`): [`CF2`](fp.CF2.md)<`T1`, `T2`, `R`\>

#### Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `R` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `func` | (`t1`: `T1`, `t2`: `T2`) => `R` |

#### Returns

[`CF2`](fp.CF2.md)<`T1`, `T2`, `R`\>

#### Defined in

[fp/curry.ts:56](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L56)

### CurryFn

▸ **CurryFn**<`T1`, `T2`, `T3`, `R`\>(`func`): [`CF3`](fp.CF3.md)<`T1`, `T2`, `T3`, `R`\>

#### Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `T3` |
| `R` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `func` | (`t1`: `T1`, `t2`: `T2`, `t3`: `T3`) => `R` |

#### Returns

[`CF3`](fp.CF3.md)<`T1`, `T2`, `T3`, `R`\>

#### Defined in

[fp/curry.ts:57](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L57)

### CurryFn

▸ **CurryFn**<`T1`, `T2`, `T3`, `T4`, `R`\>(`func`): [`CF4`](fp.CF4.md)<`T1`, `T2`, `T3`, `T4`, `R`\>

#### Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `T3` |
| `T4` |
| `R` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `func` | (`t1`: `T1`, `t2`: `T2`, `t3`: `T3`, `t4`: `T4`) => `R` |

#### Returns

[`CF4`](fp.CF4.md)<`T1`, `T2`, `T3`, `T4`, `R`\>

#### Defined in

[fp/curry.ts:58](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L58)

### CurryFn

▸ **CurryFn**<`T1`, `T2`, `T3`, `T4`, `T5`, `R`\>(`func`): [`CF5`](fp.CF5.md)<`T1`, `T2`, `T3`, `T4`, `T5`, `R`\>

#### Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `T3` |
| `T4` |
| `T5` |
| `R` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `func` | (`t1`: `T1`, `t2`: `T2`, `t3`: `T3`, `t4`: `T4`, `t5`: `T5`) => `R` |

#### Returns

[`CF5`](fp.CF5.md)<`T1`, `T2`, `T3`, `T4`, `T5`, `R`\>

#### Defined in

[fp/curry.ts:59](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L59)

### CurryFn

▸ **CurryFn**<`T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `R`\>(`func`): [`CF6`](fp.CF6.md)<`T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `R`\>

#### Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `T3` |
| `T4` |
| `T5` |
| `T6` |
| `R` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `func` | (`t1`: `T1`, `t2`: `T2`, `t3`: `T3`, `t4`: `T4`, `t5`: `T5`, `t6`: `T6`) => `R` |

#### Returns

[`CF6`](fp.CF6.md)<`T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `R`\>

#### Defined in

[fp/curry.ts:67](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L67)

### CurryFn

▸ **CurryFn**<`T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `T7`, `R`\>(`func`): [`CF7`](fp.CF7.md)<`T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `T7`, `R`\>

#### Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `T3` |
| `T4` |
| `T5` |
| `T6` |
| `T7` |
| `R` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `func` | (`t1`: `T1`, `t2`: `T2`, `t3`: `T3`, `t4`: `T4`, `t5`: `T5`, `t6`: `T6`, `t7`: `T7`) => `R` |

#### Returns

[`CF7`](fp.CF7.md)<`T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `T7`, `R`\>

#### Defined in

[fp/curry.ts:76](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L76)

### CurryFn

▸ **CurryFn**(`func`): (...`args`: `any`[]) => `any`

#### Parameters

| Name | Type |
| :------ | :------ |
| `func` | (...`args`: `any`[]) => `any` |

#### Returns

`fn`

▸ (...`args`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `...args` | `any`[] |

##### Returns

`any`

#### Defined in

[fp/curry.ts:79](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L79)
