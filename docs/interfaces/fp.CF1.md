[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / [fp](../modules/fp.md) / CF1

# Interface: CF1<T1, R\>

[fp](../modules/fp.md).CF1

## Type parameters

| Name |
| :------ |
| `T1` |
| `R` |

## Callable

### CF1

▸ **CF1**(`t1`, ...`args`): `R`

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `...args` | `any`[] |

#### Returns

`R`

#### Defined in

[fp/curry.ts:6](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L6)
