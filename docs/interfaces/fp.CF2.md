[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / [fp](../modules/fp.md) / CF2

# Interface: CF2<T1, T2, R\>

[fp](../modules/fp.md).CF2

## Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `R` |

## Callable

### CF2

▸ **CF2**(`t1`): [`CF1`](fp.CF1.md)<`T2`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |

#### Returns

[`CF1`](fp.CF1.md)<`T2`, `R`\>

#### Defined in

[fp/curry.ts:10](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L10)

### CF2

▸ **CF2**(`t1`, `t2`, ...`args`): `R`

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |
| `...args` | `any`[] |

#### Returns

`R`

#### Defined in

[fp/curry.ts:11](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L11)
