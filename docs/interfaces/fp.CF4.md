[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / [fp](../modules/fp.md) / CF4

# Interface: CF4<T1, T2, T3, T4, R\>

[fp](../modules/fp.md).CF4

## Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `T3` |
| `T4` |
| `R` |

## Callable

### CF4

▸ **CF4**(`t1`): [`CF3`](fp.CF3.md)<`T2`, `T3`, `T4`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |

#### Returns

[`CF3`](fp.CF3.md)<`T2`, `T3`, `T4`, `R`\>

#### Defined in

[fp/curry.ts:21](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L21)

### CF4

▸ **CF4**(`t1`, `t2`): [`CF2`](fp.CF2.md)<`T3`, `T4`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |

#### Returns

[`CF2`](fp.CF2.md)<`T3`, `T4`, `R`\>

#### Defined in

[fp/curry.ts:22](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L22)

### CF4

▸ **CF4**(`t1`, `t2`, `t3`): [`CF1`](fp.CF1.md)<`T4`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |
| `t3` | `T3` |

#### Returns

[`CF1`](fp.CF1.md)<`T4`, `R`\>

#### Defined in

[fp/curry.ts:23](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L23)

### CF4

▸ **CF4**(`t1`, `t2`, `t3`, `t4`, ...`args`): `R`

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |
| `t3` | `T3` |
| `t4` | `T4` |
| `...args` | `any`[] |

#### Returns

`R`

#### Defined in

[fp/curry.ts:24](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L24)
