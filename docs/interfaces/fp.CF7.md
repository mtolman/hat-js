[@tofurama3000/hat-js](../README.md) / [Exports](../modules.md) / [fp](../modules/fp.md) / CF7

# Interface: CF7<T1, T2, T3, T4, T5, T6, T7, R\>

[fp](../modules/fp.md).CF7

## Type parameters

| Name |
| :------ |
| `T1` |
| `T2` |
| `T3` |
| `T4` |
| `T5` |
| `T6` |
| `T7` |
| `R` |

## Callable

### CF7

▸ **CF7**(`t1`): [`CF6`](fp.CF6.md)<`T2`, `T3`, `T4`, `T5`, `T6`, `T7`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |

#### Returns

[`CF6`](fp.CF6.md)<`T2`, `T3`, `T4`, `T5`, `T6`, `T7`, `R`\>

#### Defined in

[fp/curry.ts:45](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L45)

### CF7

▸ **CF7**(`t1`, `t2`): [`CF5`](fp.CF5.md)<`T3`, `T4`, `T5`, `T6`, `T7`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |

#### Returns

[`CF5`](fp.CF5.md)<`T3`, `T4`, `T5`, `T6`, `T7`, `R`\>

#### Defined in

[fp/curry.ts:46](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L46)

### CF7

▸ **CF7**(`t1`, `t2`, `t3`): [`CF4`](fp.CF4.md)<`T4`, `T5`, `T6`, `T7`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |
| `t3` | `T3` |

#### Returns

[`CF4`](fp.CF4.md)<`T4`, `T5`, `T6`, `T7`, `R`\>

#### Defined in

[fp/curry.ts:47](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L47)

### CF7

▸ **CF7**(`t1`, `t2`, `t3`, `t4`): [`CF3`](fp.CF3.md)<`T5`, `T6`, `T7`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |
| `t3` | `T3` |
| `t4` | `T4` |

#### Returns

[`CF3`](fp.CF3.md)<`T5`, `T6`, `T7`, `R`\>

#### Defined in

[fp/curry.ts:48](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L48)

### CF7

▸ **CF7**(`t1`, `t2`, `t3`, `t4`, `t5`): [`CF2`](fp.CF2.md)<`T6`, `T7`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |
| `t3` | `T3` |
| `t4` | `T4` |
| `t5` | `T5` |

#### Returns

[`CF2`](fp.CF2.md)<`T6`, `T7`, `R`\>

#### Defined in

[fp/curry.ts:49](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L49)

### CF7

▸ **CF7**(`t1`, `t2`, `t3`, `t4`, `t5`, `t6`): [`CF1`](fp.CF1.md)<`T7`, `R`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |
| `t3` | `T3` |
| `t4` | `T4` |
| `t5` | `T5` |
| `t6` | `T6` |

#### Returns

[`CF1`](fp.CF1.md)<`T7`, `R`\>

#### Defined in

[fp/curry.ts:50](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L50)

### CF7

▸ **CF7**(`t1`, `t2`, `t3`, `t4`, `t5`, `t6`, `t7`, ...`args`): `R`

#### Parameters

| Name | Type |
| :------ | :------ |
| `t1` | `T1` |
| `t2` | `T2` |
| `t3` | `T3` |
| `t4` | `T4` |
| `t5` | `T5` |
| `t6` | `T6` |
| `t7` | `T7` |
| `...args` | `any`[] |

#### Returns

`R`

#### Defined in

[fp/curry.ts:51](https://gitlab.com/mtolman/hat-js/-/blob/7b8146a/src/fp/curry.ts#L51)
