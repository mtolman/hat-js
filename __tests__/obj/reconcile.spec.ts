import {abort, reconcile, ReconcileStrategy} from "../../src/obj/reconcile";

describe('reconcile', () => {
    it('Can do fast-forwards', () => {
        const prev = {a: {b: 4}}
        const cur = prev
        const next = {a: {b: 5}}
        expect(reconcile(cur, prev, next)).toBe(next)
    })

    it('Can do auto merges resolutions', () => {
        const prev = {a: {b: 4, d: 4}}
        const cur = {a: {b: 4, c: 5, d: 4}}
        const next = {a: {b: 5}}
        expect(reconcile(cur, prev, next)).toEqual({a: {b: 5, c: 5}})
    })

    it('Can handle conflicts - ABORT strategy', () => {
        const prev = {a: 5,}
        const cur = {a: 6}
        const next = {a: 7, c: 4}
        expect(reconcile(cur, prev, next)).toBe(abort)
    })

    it('Can handle conflicts - THEIRS strategy', () => {
        const prev = {a: 5,}
        const cur = {a: 6, d: 3}
        const next = {a: 7, c: 4}
        expect(reconcile(cur, prev, next, ReconcileStrategy.THEIRS)).toEqual({a: 6, c: 4, d: 3})
    })

    it('Can handle conflicts - MINE strategy', () => {
        const prev = {a: 5,}
        const cur = {a: 6, d: 3}
        const next = {a: 7, c: 4}
        expect(reconcile(cur, prev, next, ReconcileStrategy.MINE)).toEqual({a: 7, c: 4, d: 3})
    })
})