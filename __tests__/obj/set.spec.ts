import { set, setIn } from '../../src/obj/set';

describe('set', () => {
  it('Sets existing values', () => {
    expect(set('a', 6, { a: 5 })).toEqual({a: 6});
    expect(set(0, 3, [4])).toEqual([3]);
  });

  it('Sets missing values', () => {
    expect(set('b', 2, { a: 5 })).toEqual({a: 5, b: 2});
    expect(set(2, 1, [4])).toEqual([4, null, 1]);
  });

  it('non-objects', () => {
    expect(set('a', 2, 5)).toEqual({'a': 2});
  });
});

describe('setIn', () => {
  it('sets values', () => {
    expect(setIn(['a', 'b'], 4, { a: {c: 5} })).toEqual({a: {b: 4, c: 5}});
    expect(setIn(['a', 'b', 'd'], 4, { a: {c: 5} })).toEqual({a: {b: {d: 4}, c: 5}});
    expect(setIn(0, 1, [4])).toEqual([1]);
    expect(setIn(['a'], 4, { a: 5 })).toEqual({a: 4});
    expect(setIn([0, 'd', 3], 3, [4])).toEqual([{d: [null, null, null, 3]}]);

    expect(setIn(['a', 'b'], 2,  { a: { b: 5 } })).toEqual({ a: { b: 2 } });
    expect(setIn([0, 'c'], 1, [{ c: 4 }])).toEqual([{c:1}]);
  });

});