import {havePathsInCommon, paths} from '../../src/obj'

describe('paths', () => {
    it('can detect path for single-nested node', () => {
        expect(paths({a: {b: {c: {x: {y: 'z'}}}}})).toEqual([['a', 'b', 'c', 'x', 'y']])
    })

    it('can detect path for double-nested node', () => {
        expect(paths({a: {b: {c: {x: {y: 'z', f: 3}}}}})).toEqual([['a', 'b', 'c', 'x', 'y'], ['a', 'b', 'c', 'x', 'f']])
    })

    it('can detect path for arrays', () => {
        expect(paths([{a: {b: {c: {x: {y: 'z'}}}}}, {a: {b: {c: {z: {y: 'z'}}}}}])).toEqual(
            [['0', 'a', 'b', 'c', 'x', 'y'], ['1', 'a', 'b', 'c', 'z', 'y']]
        )
    })
})

describe('havePathsInCommon', () => {
    it ('can detect common paths for same structure', () => {
        expect(havePathsInCommon(
            {a: {b: {c: {x: {y: 'z'}}}}},
            {a: {b: {c: {x: {y: 'z'}}}}}
        )).toBe(true)
    })

    it('can detect common paths for different structures', () => {
        expect(havePathsInCommon(
            {a: {b: {c: 4, d: {e: 5}}}},
            {a: {b: {d: {e: 6}}}}
        )).toBe(true)
    })

    it('can detect no common paths', () => {
        expect(havePathsInCommon(
            {a: {b: {c: 4}}},
            {a: {d: 3}}
        )).toBe(false)
    })
})