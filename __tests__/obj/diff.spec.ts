import {diff, removedDiff} from "../../src/obj/diff";

describe('diff', () => {
    it('detects replacements', () => {
        expect(diff({a: 3}, {a: 5})).toEqual({a: 5})
        expect(diff({a: {x: 3, y: 2}}, {a: {x: 4, y: 2}})).toEqual({a: {x: 4}})
        expect(diff([1, 2, 3], [2, 2, 3])).toEqual({0: 2})
        expect(diff([1, {a: {x: 2, y: 1}}, 3], [1, {a: {x: 2, y: 2}}, 3])).toEqual({1: {a: {y: 2}}})
    })

    it ('detects additions', () => {
        expect(diff({a: 3}, {a: 3, b: 4})).toEqual({b: 4})
        expect(diff({a: {x: 3, y: 2}}, {a: {x: 3, y: 2, z: 1}})).toEqual({a: {z: 1}})
        expect(diff([1, 2, 3], [1, 2, 3, 4])).toEqual({3: 4})
        expect(diff([1, {a: {x: 2, y: 1}}, 3], [1, {a: {x: 2, y: 1, z: 3}}, 3])).toEqual({1: {a: {z: 3}}})
    })

    it ('detects removals', () => {
        expect(diff({a: 3}, {})).toEqual({a: removedDiff})
        expect(diff({a: {x: 3, y: 2}}, {a: {x: 3}})).toEqual({a: {y: removedDiff}})
        expect(diff([1, 2, 3], [1, 2])).toEqual({2: removedDiff})
        expect(diff([1, {a: {x: 2, y: 1}}, 3], [1, {a: {y: 1}}, 3])).toEqual({1: {a: {x: removedDiff}}})
    })

    it ('detects array shifts', () => {
        expect(diff([{a: {x: 2, y: 1}}, 3], [1, {a: {x: 2, y: 1}}, 3])).toEqual({0: 1, 1: {a: {x: 2, y: 1}}, 2: 3})
        expect(diff([1, {a: {x: 2, y: 1}}, 3], [{a: {x: 2, y: 1}}, 3])).toEqual({0: {a: {x: 2, y: 1}}, 1: 3, 2: removedDiff})
    })
})