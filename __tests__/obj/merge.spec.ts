import {merge} from "../../src/obj/merge";

describe('merge', () => {
    it('can add', () => {
        expect(merge({a: {b: {c: 3}}}, {a: {b: {c: 3, d: 4}}})).toEqual(
            {a: {b: {c: 3, d: 4}}}
        )
    })

    it('can overwrite', () => {
        expect(merge({a: {b: {c: 3}}}, {a: {b: {c: 4}}})).toEqual(
            {a: {b: {c: 4}}}
        )
    })


    it('cannot remove', () => {
        expect(merge({a: {b: {c: 3, d: 2}}}, {a: {b: {c: 3}}})).toEqual(
            {a: {b: {c: 3, d: 2}}}
        )
    })
})
