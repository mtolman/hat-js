import {commitMerge} from "../../src/obj/commitMerge";

describe('commitMerge', () => {
    it('can add', () => {
        const curPrev = {a: {b: {c: 3}}}
        expect(commitMerge(curPrev, curPrev, {a: {b: {c: 3, d: 4}}})).toEqual(
            {a: {b: {c: 3, d: 4}}}
        )
    })

    it('can overwrite', () => {
        const curPrev = {a: {b: {c: 3}}}
        expect(commitMerge(curPrev, curPrev, {a: {b: {c: 4}}})).toEqual(
            {a: {b: {c: 4}}}
        )
    })

    it('can remove', () => {
        const curPrev = {a: {b: {c: 3, d: 2}}}
        expect(commitMerge(curPrev, curPrev, {a: {b: {c: 3}}})).toEqual(
            {a: {b: {c: 3}}}
        )
    })

    it('can add new fields and preserve added', () => {
        const cur = {a: {b: {c: 3, e: 8}}}
        const prev = {a: {b: {c: 3}}}
        expect(commitMerge(cur, prev, {a: {b: {c: 3, d: 4}}})).toEqual(
            {a: {b: {c: 3, d: 4, e: 8}}}
        )
    })

    it('can overwrite new fields and preserve overwritten', () => {
        const cur = {a: {b: {c: 3, e: 8}}}
        const prev = {a: {b: {c: 3, e: 4}}}
        expect(commitMerge(cur, prev, {a: {b: {c: 4, e: 4}}})).toEqual(
            {a: {b: {c: 4, e: 8}}}
        )
    })

    it('can remove new fields and preserve removed', () => {
        const cur = {a: {b: {c: 3, e: 4}}}
        const prev = {a: {b: {c: 3, e: 4, f: 6}}}
        expect(commitMerge(cur, prev, {a: {b: {c: 4, f: 6}}})).toEqual(
            {a: {b: {c: 4}}}
        )
    })
})
