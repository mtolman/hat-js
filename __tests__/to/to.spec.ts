import {toBool, toInt, toNumber, toString} from "../../src/to";

describe('toBool', () => {
    it('coerces values', () => {
        expect(toBool(2)).toBe(true)
        expect(toBool(2)).toBe(true)
        expect(toBool("34.2")).toBe(true)
        expect(toBool(0)).toBe(false)
        expect(toBool(null)).toBe(false)
    })
})

describe('toNumber', () => {
    it('coerces values', () => {
        expect(toNumber(2)).toBe(2)
        expect(toNumber([])).toBe(0)
        expect(toNumber("34.2")).toBe(34.2)
        expect(toNumber(0)).toBe(0)
        expect(toNumber(null)).toBe(0)
    })
})

describe('toInt', () => {
    it('coerces values', () => {
        expect(toInt(2)).toBe(2)
        expect(toInt([])).toBe(0)
        expect(toInt("34.2")).toBe(34)
        expect(toInt("33")).toBe(33)
        expect(toInt(0)).toBe(0)
        expect(toInt(null)).toBe(0)
    })
})

describe('toString', () => {
    it('coerces values', () => {
        expect(toString(2)).toBe('2')
        expect(toString([])).toBe('')
        expect(toString([1, 2])).toBe('1,2')
        expect(toString("34.2")).toBe('34.2')
        expect(toString("33")).toBe('33')
        expect(toString(0)).toBe('0')
        expect(toString(null)).toBe('null')
    })
})
