import { collectToArray } from '../../src/iterables/collectToArray';
import { head } from '../../src/iterables/head';

describe('head', () => {
  it('returns an array of the first element or nothing', () => {
    expect(collectToArray(head([0, 3]))).toEqual([0]);
    expect(collectToArray(head([]))).toEqual([]);
  });
});
