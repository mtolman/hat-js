import { window } from '../../src/iterables/window';
import { collectToArray } from '../../src/iterables/collectToArray';

describe('Iterable window', () => {
  it('Can get windows of an iterable', () => {
    expect(collectToArray(window(2, [1, 2, 3, 4]))).toEqual([[1, 2], [2, 3], [3, 4]]);
    expect(collectToArray(window(3, [1, 2, 3, 4]))).toEqual([[1, 2, 3], [2, 3, 4]]);
  });

  it('Can get windows of an iterable with step', () => {
    expect(collectToArray(window({size: 2, windowStep: 2}, [1, 2, 3, 4]))).toEqual([[1, 2], [3, 4]]);
  });

  it('Can skip with large step', () => {
    expect(collectToArray(window({size: 2, windowStep: 3}, [1, 2, 3, 4]))).toEqual([[2, 3]]);
  });

  it('Can get windows of an iterable with incomplete', () => {
    expect(collectToArray(window({size: 3, windowStep: 1, incompleteWindows: true}, [1, 2, 3, 4]))).toEqual([[1], [1, 2], [1, 2, 3], [2, 3, 4], [3, 4], [4]]);
  });

  it('Can get windows of an iterable with incomplete and step', () => {
    expect(collectToArray(window({size: 3, windowStep: 2, incompleteWindows: true}, [1, 2, 3, 4]))).toEqual([[1, 2], [2, 3, 4], [4]]);
  });
});
