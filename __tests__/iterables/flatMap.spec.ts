import { collectToArray } from '../../src/iterables/collectToArray';
import { flatMap } from '../../src/iterables/flatMap';
import { lazyRange } from '../../src/utils/lazyRange';

describe('flatMap', () => {
  it('Can flatten iterables', () => {
    const fm = flatMap(x => lazyRange(x));
    expect(collectToArray(fm([1, 2, 3]))).toEqual([0, 0, 1, 0, 1, 2]);
  });

  it('Can handle non-iterables', () => {
    expect(collectToArray(flatMap(x => 5, [1]))).toEqual([5]);
  });
});
