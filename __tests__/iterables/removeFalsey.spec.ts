import { removeFalsey } from '../../src/iterables/removeFalsey';
import { collectToArray } from '../../src/iterables/collectToArray';

describe('Remove Falsey', () => {
  it('removes falsey values', () => {
    expect(collectToArray(removeFalsey(['hi', 0, 1, null, undefined, '', 8]))).toEqual([
      'hi',
      1,
      8
    ]);
  });
});
