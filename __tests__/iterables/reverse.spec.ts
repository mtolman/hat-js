import { reverse } from '../../src/iterables/reverse';
import { collectToArray } from '../../src/iterables/collectToArray';

describe('Iterable reverse', () => {
  it('can reverse iterables', () => {
    expect(collectToArray(reverse([1, 2, 3, 4]))).toEqual([4, 3, 2, 1]);
  });
});
