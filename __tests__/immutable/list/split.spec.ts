import * as List from '../../../src/immutable/list';
import { split } from '../../../src/immutable/list';
import { assertEqualLists } from '../../../src/immutable/list/__test-utils';

describe('Immutable list split', () => {
  it('can split the list', () => {
    const list = List.toList([1, 2, 3, 4, 5, 6]);
    assertEqualLists(split(list, 3), [
      [1, [2, [3, []]]],
      [4, [5, [6, []]]]
    ]);
    assertEqualLists(split(list), [
      [1, []],
      [2, [3, [4, [5, [6, []]]]]]
    ]);
    assertEqualLists(split([]), [[], []]);
    assertEqualLists(split(null), [[], []]);
  });
});
