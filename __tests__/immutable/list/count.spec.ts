import { count } from '../../../src/immutable/list/count';
import * as List from '../../../src/immutable/list';

describe('Immutable list count', () => {
  it('can count the list length', () => {
    expect(count(List.toList([1, 2, 3, 4, 5]))).toBe(5);
    expect(count(List.toList([1, 2, 3]))).toBe(3);
  });
});
