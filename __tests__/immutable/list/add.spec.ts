import * as List from '../../../src/immutable/list';
import { add } from '../../../src/immutable/list';
import { assertEqualLists } from '../../../src/immutable/list/__test-utils';

describe('Immutable List add', () => {
  it('can add an element', () => {
    const list = List.toList([1, 2, 3, 4]);
    assertEqualLists(add(list, 0), [0, list]);
  });
});
