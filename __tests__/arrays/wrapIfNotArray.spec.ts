import { wrapIfNotArray } from '../../src/arrays/wrapIfNotArray';

describe('wrapIfNotArray', () => {
  it('passes arrays through', () => {
    const arr = [1, 2, 3];
    expect(wrapIfNotArray(arr)).toBe(arr);
  });

  it('returns an array of items for non-arrays', () => {
    expect(wrapIfNotArray(1)).toEqual([1]);
    expect(wrapIfNotArray('1')).toEqual(['1']);
    const f = () => 1;
    expect(wrapIfNotArray(f)).toEqual([f]);
    expect(wrapIfNotArray({ l: 1 })).toEqual([{ l: 1 }]);
    expect(wrapIfNotArray(null)).toEqual([null]);
    expect(wrapIfNotArray(undefined)).toEqual([undefined]);
  });
});
