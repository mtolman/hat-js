import { zipLoop } from '../../src/arrays/ziploop';

describe('zipLoop', () => {
  it('works with two arrays', () => {
    expect(zipLoop([1, 2, 3, 4], [10, -1, 3])).toEqual([
      [1, 10],
      [2, -1],
      [3, 3],
      [4, 10]
    ]);
  });

  it('works with two arrays reverse size', () => {
    expect(zipLoop([1, 2], [10, -1, 3, 5, 6, 7])).toEqual([
      [1, 10],
      [2, -1],
      [1, 3],
      [2, 5],
      [1, 6],
      [2, 7]
    ]);
  });

  it('works with many arrays', () => {
    expect(zipLoop([1, 2, 3, 4], [1, -1, 3], [0, 10, null, 3])).toEqual([
      [1, 1, 0],
      [2, -1, 10],
      [3, 3, null],
      [4, 1, 3]
    ]);
  });
});
