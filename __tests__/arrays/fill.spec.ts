import {fill} from '../../src/arrays'

describe('fill', () => {
    it('fills middle', () => {
        expect(fill(1, 3, 5, [1, 2, 3, 4])).toEqual([1, 5, 5, 4])
    })
})