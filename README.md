# Hat-JS

This is a collection of useful JavaScript functions, similar to Ramda or Lodash.
It is focused primarily on functional programming.
It also has the iterables module for lazily iterating any iterable.

**Note**: The iterable module does detect infinite sequences! It is possible to get an infinite loop using most (if not all)
if the functions in that module with an infinite sequence! Use at you're own discretion!

The immut module contains some immutable data structures as well.

See the docs folder for documentation.
