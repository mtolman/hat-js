import { curry } from '../fp/curry';

import { Predicate } from '../commonTypes';
import { toArrayOrEmpty } from './toArrayOrEmpty';

/**
 * @module arrays:filter
 *
 */

export interface iFindOr {
  (defaultValue: any, predicate: Predicate<any>, arr: any[]): any;
  (defaultValue: any, predicate: Predicate<any>): (arr: any[]) => any;
  (defaultValue: any): (predicate: Predicate<any>, arr: any[]) => any;
  (defaultValue: any): (predicate: Predicate<any>) => (arr: any[]) => any;
}

/**
 * Returns either an array of a single element of the first match or an empty array if there is no match
 *
 * @kind function
 */
export const findOr: iFindOr = curry(function (
  defaultValue: any,
  predicate: Predicate<any>,
  array: any[]
): [any] | [] {
  for (const elem of toArrayOrEmpty(array) as any[]) {
    if (predicate(elem)) {
      return elem;
    }
  }
  return defaultValue;
});
