import { curry } from '../fp/curry';
import { concat } from './concat';
/**
 * @module arrays:zip
 *
 */

export interface iFillAll {
  (val: any, arr: any[]): any[];
  (val: any): (arr: any[]) => any[];
}

/**
 * Takes an array and fills it with a value
 *
 * E.g.
 * `fill(4, [1, 2, 3]) => [4, 4, 4]`
 *
 * @kind function
 *
 * @param {any[]} val The value to fill
 * @param {any[]} arr The array to fill
 * @returns {any[][]} The resulting array
 */
export const fillAll: iFillAll = curry((val, arr): any => {
  return [...arr].fill(val);
});
