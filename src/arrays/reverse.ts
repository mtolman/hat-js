/**
 * Reverses an array
 *
 * @kind function
 * @param {Array<any>} array The array to grab the first element of
 * @returns {Array<any>}
 */
export function reverse<T>(array: T[]): T[] {
  return [...array].reverse();
}
