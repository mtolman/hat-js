import { curry } from '../fp/curry';
import { toArrayOrEmpty } from './toArrayOrEmpty';

/**
 * @module arrays:chunk
 *
 */

export interface iChunk {
  (size: number, array: any): any[][];
  (size: number): (array: any) => any[][];
}

/**
 * Chunks an array into arrays of a size
 *
 * @kind function
 * @param {number} size Chunk size
 * @param {any[]} array Array to chunk
 * @returns {any[][]} The chunked array
 */
export const chunk: iChunk = curry((size: number, array: any): any[][] => {
  const arr = toArrayOrEmpty(array);
  const output = [];
  let chnk = [];
  for (const elem of arr) {
    if (chnk.length >= size) {
      output.push(chnk);
      chnk = [];
    }
    chnk.push(elem);
  }
  if (chnk.length) {
    output.push(chnk);
  }
  return output;
});
