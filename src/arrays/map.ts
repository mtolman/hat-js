import { curry } from '../fp/curry';
import { toArrayOrEmpty } from './toArrayOrEmpty';

/**
 * @module arrays:map
 *
 */

export interface iMap {
  (func: (val: any) => any, array: any[]): any[];
  (func: (val: any) => any): (array: any[]) => any[];
}

/**
 * Call a function for each element of the array and return the results in a new array
 * @kind function
 *
 * @param {function} func The function to call on each element
 * @param {any[]} array Array to operate on
 * @returns {any[]} The resulting array
 */
export const map: iMap = curry(function <T, G>(func: (T) => G, array: T[]): G[] {
  return toArrayOrEmpty(array).map(func);
});
