import { curry } from '../fp/curry';
import { toArrayOrEmpty } from './toArrayOrEmpty';

import { Predicate } from '../commonTypes';

/**
 * @module arrays:filter
 *
 */

export interface iFilter {
  (predicate: Predicate<any>, arr: any[]): any[];
  (predicate: Predicate<any>): (arr: any[]) => any[];
}

/**
 * Filters out the elements of an array where the predicate returns false
 *
 * @kind function
 * @param {Predicate} func Function to filter by
 * @param {any[]} array Array to filter
 * @returns {any[]} The resulting array
 */
export const filter: iFilter = curry(function (predicate: Predicate<any>, array: any[]): any[] {
  return toArrayOrEmpty(array).filter(predicate);
});
