import { take } from './take';

/**
 * Returns an array of the last element in an array or an empty array if the provided array is empty
 *
 * @kind function
 * @param {Array<any>} array The array to grab the first element of
 * @returns {Array<any>} The first element of an array if it is not empty or an empty array otherwwise
 */
export function last<T>(array: T[]): [T] | [] {
  return array.length ? [array[array.length - 1]] : [];
}
