/**
 * @module arrays:toArrayOrEmpty
 *
 */

/**
 * If the element is an array, it returns the array; otherwise it returns an empty array
 * @kind function
 *
 * @param {any} obj Object to check
 * @returns {any[]} The resulting array
 */
export function wrapIfNotArray<T>(obj: T[] | T): T[] {
  if (Array.isArray(obj)) return obj;
  return [obj];
}
