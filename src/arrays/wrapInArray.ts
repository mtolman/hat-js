/**
 * Wraps a value in an array
 * @param val
 */
export function wrapInArray<T>(val: T): [T] {
  return [val];
}
