import { curry } from '../fp/curry';
import { any } from './any';
import { isEqual } from '../is/isEqual';
import { iContains } from './contains';
/**
 * @module arrays:zip
 *
 */

/**
 * Returns whether an array contains a value. Checks using isEqual
 *
 * E.g.
 * `containsEquals([1, 2, 3], 2) => true`
 * `containsEquals([1, 2, 3], 4) => false`
 *
 * @kind function
 * @param {any} val The value to search for
 * @param {Array<any>} arr The array to iterate over
 * @returns {boolean} returns true if the value exists in the array
 * @see isEqual
 */
export const containsEquals: iContains = curry((val, arr): any => {
  return any((v) => isEqual(v, val), arr);
});
