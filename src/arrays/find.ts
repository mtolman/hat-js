import { curry } from '../fp/curry';

import { Predicate } from '../commonTypes';
import { toArrayOrEmpty } from './toArrayOrEmpty';

/**
 * @module arrays:filter
 *
 */

export interface iFind {
  (predicate: Predicate<any>, arr: any[]): [any] | [];
  (predicate: Predicate<any>): (arr: any[]) => [any] | [];
}

/**
 * Returns either an array of a single element of the first match or an empty array if there is no match
 *
 * @kind function
 * @param {Predicate} func Function to filter by
 * @param {any[]} array Array to filter
 * @returns {any[]} The resulting array
 */
export const find: iFind = curry(function (predicate: Predicate<any>, array: any[]): [any] | [] {
  for (const elem of toArrayOrEmpty(array)) {
    if (predicate(elem)) {
      return [elem];
    }
  }
  return [];
});
