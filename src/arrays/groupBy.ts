/**
 * @module iterables:groupBy
 *
 */
import { curry } from '../fp/curry';
import { isArray } from '../is/isArray';
import { isFunction } from '../is/isFunction';
import { isNil } from '../is/isNil';
import { isObject } from '../is/isObject';
import { toArrayOrEmpty } from './toArrayOrEmpty';

export interface iGroupBy {
  (func: (val: any) => string | number, arr: any[]): { [key: string]: any[]; [key: number]: any[] };
  (func: (val: any) => string | number): (arr: any[]) => {
    [key: string]: any[];
    [key: number]: any[];
  };
}

/**
 * Takes an array of values and groups them by the result of calling a function
 *
 * @generator
 * @kind function
 * @param {function} func A function to call to group elements by
 * @param {Array<any>} array An array to iterate over
 * @returns {Object} An object where the result of func is the key and an array of elements that provided that key is the value
 */
export const groupBy: iGroupBy = curry(function (
  func: (val: any) => string | number,
  arr: any[]
): { [key: string]: any[]; [key: number]: any[] } {
  const result = {};
  for (const val of toArrayOrEmpty(arr)) {
    const key = func(val);
    result[key] = result[key] || [];
    result[key].push(val);
  }
  return result;
});

const get = curry((key: string | number, obj: any) =>
  (isArray(obj) || isObject(obj) || isFunction(obj)) && !isNil(obj) && key in obj ? obj[key] : null
);

export interface iGroupByKey {
  (key: string | number, arr: any[]): { [key: string]: any[]; [key: number]: any[] };
  (key: string | number): (arr: any[]) => { [key: string]: any[]; [key: number]: any[] };
}

/**
 * Takes an array of values and groups them by a key
 *
 * @generator
 * @kind function
 * @param {string|number} key The key to group by
 * @param {Array<any>} array An array to iterate over
 * @returns {Object} An object where the result of func is the key and an array of elements that provided that key is the value
 */
export const groupByKey: iGroupByKey = curry(
  (
    key: string | number,
    arr: any[]
  ): {
    [key: string]: any[];
    [key: number]: any[];
  } => groupBy(get(key), arr)
);

export interface iIndexBy {
  (func: (val: any) => string | number, arr: any[]): { [key: string]: any; [key: number]: any };
  (func: (val: any) => string | number): (arr: any[]) => { [key: string]: any; [key: number]: any };
}

/**
 * Takes an array of values and indexes them by the result of a function
 *
 * @generator
 * @kind function
 * @param {function} func The function to index by
 * @param {Array<any>} array An array to iterate over
 * @returns {Object} An object where the result of func is the key and an array of elements that provided that key is the value
 */
export const indexBy: iIndexBy = curry(
  (
    func: (any) => string | number,
    array: Array<any>
  ): {
    [key: string]: any;
    [key: number]: any;
  } => {
    const result = {};
    for (const val of toArrayOrEmpty(array)) {
      const key = func(val);
      result[key] = val;
    }
    return result;
  }
);

export interface iIndexByKey {
  (key: string | number, arr: any[]): { [key: string]: any; [key: number]: any };
  (key: string | number): (arr: any[]) => { [key: string]: any; [key: number]: any };
}

/**
 * Takes an array of values and indexes them by a key
 *
 * @generator
 * @kind function
 * @param {string|number} key The key to index by
 * @param {Array<any>} array An array to iterate over
 * @returns {Object} An object where the result of func is the key and an array of elements that provided that key is the value
 */
export const indexByKey: iIndexByKey = curry(
  (
    key: string,
    array: Array<any>
  ): {
    [key: string]: any[];
    [key: number]: any[];
  } => indexBy(get(key), array)
);
