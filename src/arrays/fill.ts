import { curry } from '../fp/curry';
import { concat } from './concat';
/**
 * @module arrays:zip
 *
 */

export interface iFill {
  (start: number, end: number, val: any, arr: any[]): any[];
  (start: number, end: number, val: any): (arr: any[]) => any[];
  (start: number, end: number): (val: any) => (arr: any[]) => any[];
  (start: number, end: number): (val: any, arr: any[]) => any[];
  (start: number): (end: number, val: any, arr: any[]) => any[];
  (start: number): (end: number, val: any) => (arr: any[]) => any[];
  (start: number): (end: number) => (val: any, arr: any[]) => any[];
  (start: number): (end: number) => (val: any) => (arr: any[]) => any[];
}

/**
 * Takes an array and fills it with a value between [start, end)
 *
 * E.g.
 * `fill(1, 3, 5, [1, 2, 3, 4]) => [1, 5, 5, 4]`
 *
 */
export const fill: iFill = curry((start, end, val, arr): any => {
  return concat(arr.slice(0, start), [...arr.slice(start, end)].fill(val), arr.slice(end));
});
