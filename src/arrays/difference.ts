/**
 * @module iterables:difference
 *
 */
import { curry } from '../fp/curry';
import { flatten } from './flatten';

export interface iDifference {
  (arrLeft: any[], arrRight: any[], ...moreArrays: any[][]): any[];
  (arrLeft: any[], arrRight: any[]): any[];
  (arrLeft: any[]): (arrRight: any[], ...moreArrays: any[][]) => any[];
  (arrLeft: any[]): (arrRight: any[]) => any[];
}

/**
 * Returns the items in the first iterable that are not in the other iterables
 * Can be thought of as performing the Set difference operator (A - B)
 *
 * Note: It uses the Set has function to find the difference
 *
 *
 * @generator
 * @kind function
 * @param arr1 The iterable that will provide values
 * @param arr2 An iterable with values to remove from iterable1
 * @param arrs Other iterables with values to remove from iterable1
 * @returns values from iterable1 not in the other iterables
 */
export const difference: iDifference = curry(function (arr1, arr2, ...arrs) {
  const set = new Set(flatten([arr2, ...arrs]));
  const res = [];
  for (const val of new Set(arr1)) {
    if (!set.has(val)) {
      res.push(val);
    }
  }
  return res;
});
