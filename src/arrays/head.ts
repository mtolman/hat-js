import { take } from './take';

/**
 * Returns an array of the first element in an array or an empty array if the provided array is empty
 *
 *
 * @kind function
 * @param {Array<any>} array The array to grab the first element of
 * @returns {Array<any>} The first element of an array if it is not empty or an empty array otherwwise
 */
export const head = take(1);
