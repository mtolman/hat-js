import { toArrayOrEmpty } from './toArrayOrEmpty';

/**
 * @module arrays:fromPairs
 *
 */

type StrOrNum = string | number;

/**
 * Converts an array of key/value pairs into an object
 * @kind function
 * @returns {Object} The resulting object
 * @param pairs Key/value pairs
 */
export const fromPairs = function <T>(pairs: [StrOrNum, T][]): {
  [key: string]: T;
  [key: number]: T;
} {
  return toArrayOrEmpty(pairs)
    .map(([key, val]) => ({ [key]: val }))
    .reduce((a, c) => Object.assign(a, c), {});
};
