/**
 * @module arrays:all
 *
 */

import { curry } from '../fp/curry';
import { Predicate } from '../commonTypes';
import { toArrayOrEmpty } from './toArrayOrEmpty';

export interface iAny {
  (predicate: Predicate<any>, arr: any[]): boolean;
  (predicate: Predicate<any>): (arr: any[]) => boolean;
}

/**
 * Applies a predicate to all elements of an array and checks if the predicate returns true for any of them
 * Will return true as soon as the predicate returns true for any element
 *
 * @kind function
 * @param {Predicate} predicate The predicate to execute for each element
 * @param {Array<any>} arr The array to iterate over
 * @returns {boolean} returns true if the predicate returns true for any elements of the iterable
 */
export const any: iAny = curry(function (predicate: Predicate<any>, arr: any[]): boolean {
  for (const val of toArrayOrEmpty(arr)) {
    if (predicate(val)) return true;
  }
  return false;
});
