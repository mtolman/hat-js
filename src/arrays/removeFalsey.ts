import { filter } from './filter';
import { toBool } from '../to/toBool';

/**
 * Returns either an array with all falsey elements removed
 *
 * @kind function
 */
export const removeFalsey = filter(toBool);
