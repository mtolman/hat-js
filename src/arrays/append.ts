import { curry } from '../fp/curry';
import { concat } from './concat';
/**
 * @module arrays:zip
 *
 */

export interface iAppend {
  (arr: any[], val: any, ...moreVals: any[]): any[];
  (arr: any[], val: any): any[];
  (arr: any[]): (val: any, ...moreVals: any[]) => any[];
  (arr: any[]): (val: any) => any[];
}

/**
 * Adds one or more items to the end of the array
 *
 * E.g.
 * `append([1, 2, 3], 4, 5, 6) => [1, 2, 3, 4, 5, 6]`
 *
 * @kind function
 *
 * @param {any[]} arr The array to append to
 * @param {any[]} val The value to append
 * @param {...any[]} moreVals Other values to append
 * @returns {any[][]} The resulting array
 */
export const append: iAppend = curry((arr, val, ...moreVals): any => {
  return concat(arr, [val], moreVals);
});
