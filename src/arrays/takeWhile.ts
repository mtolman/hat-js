import { toArrayOrEmpty } from './toArrayOrEmpty';
import { Predicate } from '../commonTypes';
import { curry } from '../fp/curry';
import { isArray } from '../is/isArray';
import { toBool } from '../to/toBool';

/**
 * @module arrays:takeWhile
 *
 */

export interface iTakeWhile {
  (whileFunc: Predicate<any> | any[], arr: any[]): any[];
  (whileFunc: Predicate<any> | any[]): (arr: any[]) => any[];
}

/**
 * Takes elements from the array so long as the predicate returns true for each element
 * @kind function
 *
 * @param {Predicate} whileFunc Predicate to determine when to stop taking elements
 * @param {any[]} array Array to operate on
 * @returns {any[]} The resulting array
 */
export const takeWhile: iTakeWhile = curry(function (
  whileFunc: Predicate<any> | any[],
  array: any[]
): any[] {
  const arr = toArrayOrEmpty(array);
  const res = [];
  if (isArray(whileFunc)) {
    for (let i = 0; i < arr.length && i < whileFunc.length && toBool(whileFunc[i]); ++i) {
      res.push(array[i]);
    }
  } else {
    for (const val of arr) {
      if (whileFunc(val)) res.push(val);
      else return res;
    }
  }
  return res;
});
