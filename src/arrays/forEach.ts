import { curry } from '../fp/curry';
import { toArrayOrEmpty } from './toArrayOrEmpty';

export interface iForEach {
  (func: (val: any) => void, array: any[]): void;
  (func: (val: any) => void): (array: any[]) => void;
}

export const forEach = curry(function (func: (any) => void, array: any[]): void {
  toArrayOrEmpty(array).forEach(func);
});
