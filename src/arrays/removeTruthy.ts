import { filter } from './filter';
import { negate } from '../fp/index';
import { toBool } from '../to/toBool';

/**
 * Returns either an array with all truthy elements removed
 *
 * @kind function
 */
export const removeTruthy = filter(negate(toBool));
