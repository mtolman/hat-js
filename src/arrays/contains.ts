import { curry } from '../fp/curry';
import { any } from './any';
/**
 * @module arrays:zip
 *
 */

export interface iContains {
  (val: any, arr: any[]): boolean;
  (val: any): (arr: any[]) => boolean;
}

/**
 * Returns whether an array contains a value
 *
 * E.g.
 * `contains([1, 2, 3], 2) => true`
 * `contains([1, 2, 3], 4) => false`
 *
 * @kind function
 * @param {any} val The predicate to execute for each element
 * @param {Array<any>} arr The array to iterate over
 * @returns {boolean} returns true if the value exists in the array
 */
export const contains: iContains = curry((val, arr): any => {
  return any((v) => v === val, arr);
});
