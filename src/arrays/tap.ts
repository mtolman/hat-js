import { curry } from '../fp/curry';
import { toArrayOrEmpty } from './toArrayOrEmpty';

/**
 * @module arrays:tap
 *
 */

export interface iTap {
  (func: (val: any) => void, array: any[]): any[];
  (func: (val: any) => void): (array: any[]) => any[];
}

/**
 * Calls a function for each element in the array and returns the array
 * @kind function
 *
 * @param {function} func Function to call on the array
 * @param {any[]} array Array to operate on
 * @returns {any[]} The original array
 */
export const tap: iTap = curry(function (func: (any) => void, array: any[]): any[] {
  toArrayOrEmpty(array).forEach(func);
  return array;
});
