import { findOr } from './findOr';

/**
 * Returns either an array of a single element of the first match or an empty array if there is no match
 *
 * @kind function
 */
export const findOrNull = findOr(null);
