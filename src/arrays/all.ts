/**
 * @module arrays:all
 *
 */

import { curry } from '../fp/curry';
import { Predicate } from '../commonTypes';
import { toArrayOrEmpty } from './toArrayOrEmpty';

export interface iAll {
  (predicate: Predicate<any>, arr: any[]): boolean;
  (predicate: Predicate<any>): (arr: any[]) => boolean;
}

/**
 * Applies a predicate to all elements of an array and ensures they all return true
 * Will return false as soon as the predicate returns false for any element
 *
 * @kind function
 * @param {Predicate} predicate The predicate to execute for each element
 * @param {Array<any>} arr The array to iterate over
 * @returns {boolean} returns true if the predicate returns true for all elements of the iterable
 */
export const all: iAll = curry(function (predicate: Predicate<any>, arr: any[]): boolean {
  for (const val of toArrayOrEmpty(arr)) {
    if (!predicate(val)) return false;
  }
  return true;
});
