import { curry } from '../fp/curry';
import { toArrayOrEmpty } from './toArrayOrEmpty';
/**
 * @module arrays:skip
 *
 */

export interface iSkip {
  (n: number, array: any[]): any[];
  (n: number): (array: any[]) => any[];
}

/**
 * Skips the first n items of the array
 * @kind function
 *
 * @param {number} n Number of elements to skip
 * @param {any[]} array Array to operate on
 * @returns {any[]} The resulting array
 */
export const skip: iSkip = curry(function (n: number, array: any[]): any[] {
  return toArrayOrEmpty(array).splice(n);
});
