import { curry } from '../fp';
import { collectToArray } from '../iterables/collectToArray';
import { window as itWindow } from '../iterables/window';

export interface WindowOptions {
  size?: number;
  incompleteWindows?: boolean;
  windowStep?: number;
}

export interface iWindowOperation {
  (opts: number | WindowOptions, array: any[]): Array<any[]>;
  (opts: number | WindowOptions): (array: any[]) => Array<any[]>;
}

/**
 * Returns an array of a sliding windows of size n
 *
 * @generator
 * @kind function
 * @param {size|WindowOptions} size The size of the sliding window, or an object of more detailed options if desired
 * @param {Iterable<any>} iterable The array to do a sliding window over
 * @returns {Iterable<any[]>} An array of the sliding windows
 */
export const window: iWindowOperation = curry(function (
  size: number | WindowOptions,
  array: any[]
): Array<any[]> {
  return collectToArray(itWindow(size, array));
});
