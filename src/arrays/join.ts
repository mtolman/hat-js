import { toArrayOrEmpty } from './toArrayOrEmpty';
import { curry } from '../fp/curry';

/**
 * @module arrays:join
 *
 */

export interface iJoin {
  (separator: string, arr: any[]): string;
  (separator: string): (arr: any[]) => string;
}

/**
 * Joins elements of an array with a string
 * @kind function
 *
 * @param {string} separator String to join elements with
 * @param {any[]} array Array to operate on
 * @returns {string} The resulting string
 */
export const join: iJoin = curry(function (separator: string, arr: any[]): string {
  return toArrayOrEmpty(arr).join(separator);
});
