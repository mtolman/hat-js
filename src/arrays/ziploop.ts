import { curry } from '../fp/curry';
import { iZip } from './zip';
/**
 * @module arrays:zip
 *
 */

/**
 * Takes two or more arrays and groups elements by index from each array
 * If arrays are different lengths, it will fill up missing spots by wrapping around
 *
 * E.g.
 * `zip([1, 2, 3], [4, 5, 6]) => [[1, 4], [2, 5], [3, 6]]`
 * `zip([1, 2], [4, 5, 6]) => [[1, 4], [2, 5], [1, 6]]`
 *
 * @kind function
 *
 * @param {any[]} arrLeft The first array to zip
 * @param {any[]} arrRight The second array to zip
 * @param {...any[]} moreArrays Other arrays to zip
 * @returns {any[][]} The resulting array
 */
export const zipLoop: iZip = curry((arrLeft, arrRight, ...moreArrays): any => {
  const arrays = [arrLeft, arrRight, ...moreArrays];
  const maxLen = Math.max(...arrays.map((a) => a.length));

  const res = [];
  for (let i = 0; i < maxLen; ++i) {
    res.push(arrays.map((a) => a[i % a.length]));
  }
  return res;
});
