/**
 * Returns an array of the all the items past the first item
 *
 * @kind function
 * @param {Array<any>} array The array to grab the first element of
 * @returns {Array<any>}
 */
export function rest<T>(array: T[]): T[] {
  return array.slice(1);
}
