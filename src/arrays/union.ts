/**
 * @module arrays:union
 *
 */
import { curry } from '../fp/curry';
import { flatten } from './flatten';

export interface iUnion {
  (arrLeft: any[], arrRight: any[], ...moreArrays: any[][]): any[];
  (arrLeft: any[], arrRight: any[]): any[];
  (arrLeft: any[]): (arrRight: any[], ...moreArrays: any[][]) => any[];
  (arrLeft: any[]): (arrRight: any[]) => any[];
}

/**
 * Returns only the items that appear in all arrays
 *
 * Note: It uses the Set.has function to find matches
 *
 *
 * @generator
 * @kind function
 * @param {Array<any>} array1 The first array to get values from
 * @param {Array<any>} array2 The second array to get values from
 * @param {...Array<any>} arrays Other arrays
 * @returns {Array<any>} values that only occur in all provided arrays
 */
export const union: iUnion = curry(function (
  array1: any[],
  array2: any[],
  ...arrays: any[][]
): any[] {
  return [...new Set(flatten([array1, array2, ...arrays])).values()];
});
