import { curry } from '../fp/curry';
import { pipe } from '../fp/pipe';
import { toArrayOrEmpty } from './toArrayOrEmpty';
import { map } from './map';
import { flatten } from './flatten';

/**
 * @module arrays:flatMap
 *
 */

export interface iFlatMap {
  (func: (T) => any[] | any, array: any[]): any[];
  (func: (T) => any[] | any): (array: any[]) => any[];
}

/**
 * Calls map and then flatten on the array
 * @kind function
 *
 * @param {function} func Function to call map with
 * @param {any[]} array Array to operate on
 * @returns {any[]} The resulting array
 */
export const flatMap: iFlatMap = curry(function <T, G>(func: (T) => G, array: T): G[] {
  return pipe(toArrayOrEmpty(array), map(func), flatten);
});
