import { curry } from '../fp/curry';
import { concat } from './concat';
import { fill } from './fill';
/**
 * @module arrays:zip
 *
 */

/**
 * Takes an array and fills it with a value between [0, end)
 *
 * E.g.
 * `fillStart(3, 5, [1, 2, 3, 4]) => [5, 5, 5, 4]`
 *
 */
export const fillStart = fill(0);
