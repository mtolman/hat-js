import { curry } from '../fp/curry';
import { concat } from './concat';
/**
 * @module arrays:zip
 *
 */

export interface iFillEnd {
  (start: number, val: any, arr: any[]): any[];
  (start: number, val: any): (arr: any[]) => any[];
  (start: number): (val: any, arr: any[]) => any[];
  (start: number): (val: any) => (arr: any[]) => any[];
}

/**
 * Takes an array and fills everything after start with the provided value
 *
 * E.g.
 * `fill(1, 5, [1, 2, 3, 4]) => [1, 5, 5, 5]`
 *
 */
export const fillEnd: iFillEnd = curry((start, val, arr): any => {
  return concat(arr.slice(0, start), [...arr.slice(start)].fill(val));
});
