import { curry } from '../fp/curry';
/**
 * @module arrays:zip
 *
 */

export interface iConcat {
  (arrLeft: any[], arrRight: any[], ...moreArrays: any[][]): any[];
  (arrLeft: any[], arrRight: any[]): any[];
  (arrLeft: any[]): (arrRight: any[], ...moreArrays: any[][]) => any[];
  (arrLeft: any[]): (arrRight: any[]) => any[];
}

/**
 * Takes two or more arrays and concatenates them
 *
 * E.g.
 * `concat([1, 2, 3], [4, 5, 6]) => [1, 2, 3, 4, 5, 6]`
 *
 * @param {any[]} arrLeft The first array to concatenate
 * @param {any[]} arrRight The second array to concatenate
 * @param {...any[]} moreArrays Other arrays to concatenate
 * @returns {any[][]} The resulting array
 */
export const concat: iConcat = curry((arrLeft, arrRight, ...moreArrays): any => {
  return arrLeft.concat(arrRight, ...moreArrays);
});
