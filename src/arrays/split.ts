import { curry } from '../fp/curry';
import { take } from './take';
import { skip } from './skip';

export interface iSplit {
  (n: number, arr: any[]): [any[], any[]];
  (n: number): (arr: any[]) => [any[], any[]];
}

/**
 * Splits an array into 2 arrays after n elements
 */
export const split: iSplit = curry(function (n: number, arr: any[]): [any[], any[]] {
  return [take(n, arr), skip(n, arr)];
});
