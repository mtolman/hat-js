import { curry } from '../fp/curry';
import { toArrayOrEmpty } from './toArrayOrEmpty';

/**
 * @module arrays:limit
 *
 */

export interface iLimit {
  (max: number, array: any[]): any[];
  (max: number): (array: any[]) => any[];
}

/**
 * Limits the number of elements in an array
 * @kind function
 *
 * @param {number} max Maximum number of elements in the array
 * @param {any[]} array Array to operate on
 * @returns {any[]} The resulting array
 */
export const limit: iLimit = curry(function (max: number, array: any[]): any[] {
  return toArrayOrEmpty(array).splice(0, max);
});
