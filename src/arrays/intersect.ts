/**
 * @module arrays:intersect
 *
 */
import { curry } from '../fp/curry';
import { all } from './all';

export interface iIntersect {
  (arrLeft: any[], arrRight: any[], ...moreArrays: any[][]): any[];
  (arrLeft: any[], arrRight: any[]): any[];
  (arrLeft: any[]): (arrRight: any[], ...moreArrays: any[][]) => any[];
  (arrLeft: any[]): (arrRight: any[]) => any[];
}

/**
 * Returns only the items that appear in all arrays
 *
 * Note: It uses the Set.has function to find matches
 *
 *
 * @generator
 * @kind function
 * @param {Array<any>} array1 The first array to get values from
 * @param {Array<any>} array2 The second array to get values from
 * @param {...Array<any>} arrays Other arrays
 * @returns {Array<any>} values that only occur in all provided arrays
 */
export const intersect: iIntersect = curry(function (
  array1: any[],
  array2: any[],
  ...arrays: any[][]
): any[] {
  const sets = [array2, ...arrays].map((s) => new Set(s));
  const res = [];
  for (const val of new Set(array1)) {
    if (all((s) => s.has(val), sets)) {
      res.push(val);
    }
  }
  return res;
});
