import { isObject } from './isObject';
import { isArray } from './isArray';
import { entries } from '../obj/entries';
import { isString } from './isString';
import { isMap } from './isMap';
import { isSet } from './isSet';
import { isListLike, isEmpty as isListEmpty } from '../immutable/list';
import { isStack, isEmpty as isStackEmpty } from '../immutable/stack';

/**
 * Returns whether a collection is empty or not
 * @param obj
 */
export function isEmpty(obj: any): boolean {
  if (isArray(obj) || isString(obj)) {
    return obj.length === 0;
  } else if (isObject(obj)) {
    return entries(obj).length === 0;
  } else if (isMap(obj) || isSet(obj)) {
    return obj.size === 0;
  } else if (isListLike(obj)) {
    return isListEmpty(obj);
  } else if (isStack(obj)) {
    return isStackEmpty(obj);
  } else {
    return false;
  }
}
