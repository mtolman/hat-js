import { isArray, isObject, isUndefined } from '../is';
import { reduce, union } from '../arrays';
import { keys } from './keys';
import { getOr, getPath } from './get';
import { isEmpty } from '../is/isEmpty';
import { setPath } from './set';
import { diff } from './diff';
import { paths } from './paths';
import { removePath } from './remove';
import { hasPath } from './has';

/**
 * Recursively merges deltas into original (including deletes).
 * Deltas are detected with diff().
 *
 *  @see diff
 * @param current Current object
 * @param previous Previous object
 * @param next Next object
 */
export function commitMerge(
  current: Object | any[],
  previous: Object | any[],
  next: Object | any[]
) {
  if (current == next) {
    return current;
  }
  const changePaths = paths(diff(previous, next));
  return reduce(
    (acc, path) => {
      if (!hasPath(path, next) && hasPath(path, acc)) {
        return removePath(path, acc);
      }
      return setPath(path, getPath(path, next), acc);
    },
    current,
    changePaths
  );
}
