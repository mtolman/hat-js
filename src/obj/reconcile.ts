import { diff } from './diff';
import { havePathsInCommon } from './paths';
import { commitMerge } from './commitMerge';

/**
 * Symbol indicating should abort
 */
export const abort = Symbol('abort');

export enum ReconcileStrategy {
  ABORT,
  THEIRS,
  MINE,
}

/**
 * Reconciles a version bump. Takes the current state, the change's previous state, and the change's next state
 * If the previous and current states are the same object in-memory, it will do a fast forward
 * If they are different objects, it will check to see if there are any structural conflicts.
 * If there are no structural conflicts, it will do a commitMerge.
 * Otherwise, it will fallback to the reconcile strategy. The default strategy is to abort.
 *
 * The strategies are:
 * - ABORT (0) - Abort by returning the symbol "abort"
 * - THEIRS (1) - Overwrite any conflicts using the fields in "current"
 * - MINE (2) - Overwrite any conflicts using the fields in "mine"
 *
 * @see abort
 * @param current
 * @param previous
 * @param next
 * @param strategy
 */
export function reconcile(
  current: Object,
  previous: Object,
  next: Object,
  strategy = ReconcileStrategy.ABORT
): Object | typeof abort {
  if (previous == current) {
    return next;
  }
  const previousToCurrent = diff(previous, current);
  const previousToNext = diff(previous, next);
  if (havePathsInCommon(previousToCurrent, previousToNext)) {
    // ABORT strategy - default. Allows the program to kick back to the user or do its own reconciliation
    if (strategy === ReconcileStrategy.ABORT) {
      return abort;
    }
    // First-write-wins strategy. Preserve any changed history
    else if (strategy === ReconcileStrategy.THEIRS) {
      return commitMerge(next, previous, current);
    }
    // Last-write-wins strategy. Overwrite whatever we don't care about
    else if (strategy === ReconcileStrategy.MINE) {
      return commitMerge(current, previous, next);
    }
  }
  return commitMerge(current, previous, next);
}
