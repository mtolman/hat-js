import { isArray, isNumber, isObject, isString, isUndefined } from '../is/index';
import { curry } from '../fp/curry';
import { collectToArray } from '../iterables/collectToArray';
import { get, getPath } from './get';
import { wrapInArray } from '../arrays/wrapInArray';
import { set } from './set';

export interface iRemove {
  (key: string | number, obj: any): any[] | any;
  (key: string | number): (obj: any) => any[] | any;
}

/**
 * Sets a value in an array or object and returns a new copy of the array or object
 * @param {string|number} key Key to grab
 * @param {any} value Value to set
 * @param {any} obj Object to grab a value out of
 */
export const remove: iRemove = curry((key: string | number, obj: any) => {
  if (isArray(obj) && isNumber(key)) {
    return [
      ...obj.slice(0, key),
      ...(key > obj.length ? new Array(key - obj.length) : []).fill(null),
      ...obj.slice(key + 1),
    ];
  } else if (isObject(obj)) {
    const copy = { ...obj };
    delete copy[key];
    return copy;
  } else {
    return obj;
  }
});

export interface iRemovePath {
  (key: Iterable<string | number> | string | number, obj: any): any[] | any;
  (key: Iterable<string | number> | string | number): (obj: any) => any[] | any;
}

/**
 * Sets a value in an array or object.
 * If a segment is null, then it adds a new empty object/array and continues. Returns the resulting object.
 *
 * @param path Path of keys to follow
 * @param {any} value Value to set
 * @param {any} obj Object to grab a value out of
 */
export const removePath: iRemovePath = curry(
  (path: Iterable<string | number> | string | number, obj: any) => {
    let res = isArray(obj) ? [...obj] : { ...obj };
    let parent = null;
    let root = null;
    const newPath = collectToArray(isString(path) || isNumber(path) ? wrapInArray(path) : path);
    for (let i = 0; i < newPath.length; ++i) {
      const piece = newPath[i];
      if (i === newPath.length - 1) {
        res = remove(piece, res);
      } else {
        res = set(piece, get(piece, res), res);
      }
      if (parent) {
        parent[newPath[i - 1]] = res;
      } else {
        root = res;
      }
      parent = res;
      res = res[piece];
    }
    return root || res;
  }
);

/**
 * Sets a value in an array or object only if the path exists.
 * If the path does not exist, then it returns the original object
 *
 * @param path Path of keys to follow
 * @param {any} value Value to set
 * @param {any} obj Object to grab a value out of
 */
export const removePathIfPresent: iRemovePath = curry(
  (path: Iterable<string | number> | string | number, obj: any) => {
    if (isUndefined(getPath(path, obj))) {
      return obj;
    }
    return removePath(path, obj);
  }
);
