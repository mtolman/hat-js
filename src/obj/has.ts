import { isArray, isFunction, isIterable, isNil, isObject } from '../is/index';
import { curry } from '../fp/curry';

export interface iHas {
  (path: string | number, obj: any): boolean;
  (path: string | number): (obj: any) => boolean;
}

/**
 * Returns whether a key exists in an object
 * @param {string|number} key Key to check
 * @param {any} obj Object to check
 */
export const has: iHas = curry(
  (key: string | number, obj: any) =>
    (isArray(obj) || isObject(obj) || isFunction(obj)) && !isNil(obj) && key in obj
);

export interface iHasPathOr {
  (path: Iterable<string | number> | string | number, obj: any): boolean;
  (path: Iterable<string | number> | string | number): (obj: any) => boolean;
}

/**
 * Returns whether a path exists in an object
 *
 * @param path Path of keys to follow
 * @param {any} obj Object to check
 */
export const hasPath: iHasPathOr = curry(
  (path: Iterable<string | number> | string | number, obj: any) => {
    let curObj = obj;
    const p = isIterable(path) ? path : [path];
    for (const segment of p) {
      if (!has(segment, curObj)) {
        return false;
      }
      curObj = curObj[segment];
    }
    return true;
  }
);
