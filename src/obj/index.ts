/**
 * Functions for operating on objects
 * @module obj
 */

export * from './toPairs';
export * from './get';
export * from './entries';
export * from './copyInto';
export * from './keys';
export * from './set';
export * from './diff';
export * from './paths';
