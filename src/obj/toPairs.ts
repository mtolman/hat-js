/**
 * @module obj:toPairs'
 *
 */

import { entries } from './entries';

/**
 * Splits an object into pairs
 */
export const toPairs = entries;
