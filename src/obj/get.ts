import { isArray } from '../is/isArray';
import { isFunction } from '../is/isFunction';
import { isIterable } from '../is/isIterable';
import { isNil } from '../is/isNil';
import { isObject } from '../is/isObject';
import { forEach } from '../iterables/forEach';
import { curry } from '../fp/curry';

export interface iGetOr {
  (path: string | number, defaultValue: any, obj: any): any | undefined;
  (path: string | number, defaultValue: any): (obj: any) => any | undefined;
  (path: string | number): (defaultValue: any, obj: any) => any | undefined;
  (path: string | number): (defaultValue: any) => (obj: any) => any | undefined;
}

/**
 * Gets a value in an array or object. Returns the default value if the key is not present or the entity is not an object
 * @param {string|number} key Key to grab
 * @param {any} defaultValue Default value to return
 * @param {any} obj Object to grab a value out of
 */
export const getOr: iGetOr = curry((key: string | number, defaultValue: any, obj: any) =>
  (isArray(obj) || isObject(obj) || isFunction(obj)) && !isNil(obj) && key in obj
    ? obj[key]
    : defaultValue
);

export interface iGet {
  (path: string | number, obj: any): any | undefined;
  (path: string | number): (obj: any) => any | undefined;
}

/**
 * Gets a value in an array or object. Returns null if the key is not present or the entity is not an object
 * @param {string|number} key Key to grab
 * @param {any} obj Object to grab a value out of
 */
export const get: iGet = curry((key: string | number, obj: any) => getOr(key, undefined, obj));

export interface iGetPathOr {
  (path: Iterable<string | number> | string | number, defaultValue: any, obj: any): any | undefined;
  (path: Iterable<string | number> | string | number, defaultValue: any): (
    obj: any
  ) => any | undefined;
  (path: Iterable<string | number> | string | number): (
    defaultValue: any,
    obj: any
  ) => any | undefined;
  (path: Iterable<string | number> | string | number): (
    defaultValue: any
  ) => (obj: any) => any | undefined;
}

/**
 * Gets a value in an array or object. Does this by following a path and doing a null check at each segment.
 * Returns the default value if the key is not present or the path is invalid (e.g. not an object/array)
 *
 * E.g.
 * getInOr(['a', 'b', 1], {a: {b: [12, 5, 14]}}) will return 5
 *
 * @param path Path of keys to follow
 * @param {any} defaultValue Default value to return
 * @param {any} obj Object to grab a value out of
 */
export const getPathOr: iGetPathOr = curry(
  (path: Iterable<string | number> | string | number, defaultValue: any, obj: any) => (
    forEach(
      (segment) => (obj = getOr(segment, defaultValue, obj)),
      isIterable(path) ? path : [path]
    ),
    obj
  )
);

/**
 * Gets a value in an array or object. Does this by following a path and doing a null check at each segment.
 * Returns the default value if the key is not present or the path is invalid (e.g. not an object/array)
 *
 * E.g.
 * getInOr(['a', 'b', 1], {a: {b: [12, 5, 14]}}) will return 5
 *
 * @param path Path of keys to follow
 * @param {any} defaultValue Default value to return
 * @param {any} obj Object to grab a value out of
 * @deprecated
 * @see getPathOr
 */
export const getInOr = getPathOr;

export interface iGetPath {
  (path: Iterable<string | number> | string | number, obj: any): any | undefined;
  (path: Iterable<string | number> | string | number): (obj: any) => any | undefined;
}
/**
 * Gets a value in an array or object. Does this by following a path and doing a null check at each segment.
 * Returns undefined if the key is not present or the path is invalid (e.g. not an object/array)
 *
 * E.g.
 * getIn(['a', 'b', 1], {a: {b: [12, 5, 14]}}) will return 5
 *
 * @param path Path of keys to follow
 * @param {any} obj Object to grab a value out of
 */
export const getPath: iGetPath = curry(
  (path: Iterable<string | number> | string | number, obj: any) => getPathOr(path, undefined, obj)
);

/**
 * Gets a value in an array or object. Does this by following a path and doing a null check at each segment.
 * Returns null if the key is not present or the path is invalid (e.g. not an object/array)
 *
 * E.g.
 * getIn(['a', 'b', 1], {a: {b: [12, 5, 14]}}) will return 5
 *
 * @param ath Path of keys to follow
 * @param {any} obj Object to grab a value out of
 * @deprecated
 * @see getPath
 */
export const getIn = curry((path: Iterable<string | number> | string | number, obj: any) =>
  getPathOr(path, null, obj)
);
