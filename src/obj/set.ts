import { isArray } from '../is/isArray';
import { isString } from '../is/isString';
import { isUndefined } from '../is/isUndefined';
import { isNumber } from '../is/isNumber';
import { isObject } from '../is/isObject';
import { curry } from '../fp/curry';
import { collectToArray } from '../iterables/collectToArray';
import { get, getPath } from './get';
import { wrapInArray } from '../arrays/wrapInArray';

export interface iSet {
  (key: string | number, value: any, obj: any): any[] | any;
  (key: string | number, value: any): (obj: any) => any[] | any;
  (key: string | number): (value: any, obj: any) => any[] | any;
  (key: string | number): (value: any) => (obj: any) => any[] | any;
}

/**
 * Sets a value in an array or object and returns a new copy of the array or object
 * @param {string|number} key Key to grab
 * @param {any} value Value to set
 * @param {any} obj Object to grab a value out of
 */
export const set: iSet = curry((key: string | number, value: any, obj: any) => {
  if (isArray(obj) && isNumber(key)) {
    return [
      ...obj.slice(0, key),
      ...(key > obj.length ? new Array(key - obj.length) : []).fill(null),
      value,
      ...obj.slice(key + 1),
    ];
  } else if (isObject(obj)) {
    return { ...obj, [key]: value };
  } else if (isString(key) || key < 0) {
    return set(key, value, {});
  } else {
    return set(key, value, []);
  }
});

export interface iSetPath {
  (key: Iterable<string | number> | string | number, value: any, obj: any): any[] | any;
  (key: Iterable<string | number> | string | number, value: any): (obj: any) => any[] | any;
  (key: Iterable<string | number> | string | number): (value: any, obj: any) => any[] | any;
  (key: Iterable<string | number> | string | number): (value: any) => (obj: any) => any[] | any;
}

/**
 * Sets a value in an array or object.
 * If a segment is null, then it adds a new empty object/array and continues. Returns the resulting object.
 *
 * @param path Path of keys to follow
 * @param {any} value Value to set
 * @param {any} obj Object to grab a value out of
 */
export const setPath: iSetPath = curry(
  (path: Iterable<string | number> | string | number, value: any, obj: any) => {
    let res = isArray(obj) ? [...obj] : { ...obj };
    let parent = null;
    let root = null;
    const newPath = collectToArray(isString(path) || isNumber(path) ? wrapInArray(path) : path);
    for (let i = 0; i < newPath.length; ++i) {
      const piece = newPath[i];
      res = set(piece, i === newPath.length - 1 ? value : get(piece, res), res);
      if (parent) {
        parent[newPath[i - 1]] = res;
      } else {
        root = res;
      }
      parent = res;
      res = res[piece];
    }
    return root || res;
  }
);

/**
 * Sets a value in an array or object.
 * If a segment is null, then it adds a new empty object/array and continues. Returns the resulting object.
 *
 * @param path Path of keys to follow
 * @param {any} value Value to set
 * @param {any} obj Object to grab a value out of
 * @deprecated
 * @see setPath
 */
export const setIn = setPath;

/**
 * Sets a value in an array or object only if the path exists.
 * If the path does not exist, then it returns the original object
 *
 * @param path Path of keys to follow
 * @param {any} value Value to set
 * @param {any} obj Object to grab a value out of
 */
export const setPathIfPresent: iSetPath = curry(
  (path: Iterable<string | number> | string | number, value: any, obj: any) => {
    if (isUndefined(getPath(path, obj))) {
      return obj;
    }
    return setIn(path, value, obj);
  }
);

/**
 * Sets a value in an array or object only if the path exists.
 * If the path does not exist, then it returns the original object
 *
 * @param path Path of keys to follow
 * @param {any} value Value to set
 * @param {any} obj Object to grab a value out of
 * @deprecated
 * @see setPathIfPresent
 */
export const setInIfPresent = setPathIfPresent;
