/**
 * Calculates all information paths for an object
 * @param obj
 */
import { append } from '../arrays/append';
import { concat } from '../arrays/concat';
import { intersect } from '../arrays/intersect';
import { join } from '../arrays/join';
import { map } from '../arrays/map';
import { reduce } from '../arrays/reduce';
import { isEmpty } from '../is/isEmpty';
import { isObject } from '../is/isObject';

export function paths(obj: Object | any[], existingPath = []): string[] {
  return reduce(
    (acc, [key, value]) => {
      const subPath = append(existingPath, key);
      if (isObject(value)) {
        return concat(acc, paths(value, subPath));
      } else {
        return concat(acc, [subPath]);
      }
    },
    [],
    Object.entries(obj)
  );
}

export function havePathsInCommon(objLeft: Object | any[], objRight: Object | any[]): boolean {
  const sepKey = map(join('.||.'));
  return !isEmpty(intersect(sepKey(paths(objLeft)), sepKey(paths(objRight))));
}
