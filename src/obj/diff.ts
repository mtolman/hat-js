import { isArray } from '../is/isArray';
import { isObject } from '../is/isObject';
import { isUndefined } from '../is/isUndefined';
import { reduce } from '../arrays/reduce';
import { union } from '../arrays/union';
import { keys } from './keys';
import { getOr } from './get';
import { isEmpty } from '../is/isEmpty';
import { setPath } from './set';

export interface Diff {
  [key: string | number]: Diff | any;
}

const noDiff = Symbol('no-diff');

/**
 * Symbol indicating that a key was removed
 */
export const removedDiff = Symbol('removed');

type DiffNode = typeof noDiff | typeof removedDiff | Diff | Object;

function diffNode(orig: Object | any[], changed: Object | any[]): DiffNode {
  if (isObject(orig) && isObject(changed)) {
    return diff(orig, changed);
  } else if (orig !== changed) {
    if (isUndefined(changed) && !isUndefined(orig)) {
      return removedDiff;
    }
    return changed;
  } else {
    return noDiff;
  }
}

/**
 * Calculates the structural difference between two objects
 * For removed keys, will return symbol in removedDiff (@see removedDiff)
 * @param orig      Original object
 * @param changed   Changed object
 */
export function diff(orig: Object | any[], changed: Object | any[]): Diff {
  const empty = isArray(orig) ? [] : {};
  if (orig == changed) {
    return empty;
  }
  const allKeys = union(keys(orig), keys(changed));
  return reduce(
    (acc, curKey) => {
      const getVal = getOr(curKey, undefined);
      const origVal = getVal(orig);
      const changedVal = getVal(changed);
      const nodeDiff = diffNode(origVal, changedVal);
      if ((isObject(nodeDiff) && isEmpty(nodeDiff)) || nodeDiff === noDiff) {
        return acc;
      }
      return setPath([curKey], nodeDiff, acc);
    },
    empty,
    allKeys
  );
}
