/**
 * @module obj:copyInto
 *
 */

import { curry } from '../fp/index';
import { cloneDeep } from '../utils/clone';

export interface iCopyInto {
  (source: Object, target: Object): Object;
  (source: Object): (target: Object) => Object;
}

/**
 * Copies attributes from source into a target
 *
 * @kind function
 * @param {obj} source The object to copy attributes from
 * @param {obj} target The object to copy attributes into
 */
export const copyInto: iCopyInto = curry((source, target) => {
  for (const prop in source) {
    if (source.hasOwnProperty(prop)) {
      target[prop] = source[prop];
    }
  }
  return target;
});

/**
 * Copies attributes from source into a target
 * Values are deeply cloned when copied
 *
 * @kind function
 * @param {obj} source The object to copy attributes from
 * @param {obj} target The object to copy attributes into
 */
export const copyIntoDeep: iCopyInto = curry((source, target) => {
  for (const prop in source) {
    if (source.hasOwnProperty(prop)) {
      target[prop] = cloneDeep(source[prop]);
    }
  }
  return target;
});
