import { isArray, isObject, isUndefined } from '../is';
import { difference, join, map, reduce, union } from '../arrays';
import { keys } from './keys';
import { getOr, getPath } from './get';
import { isEmpty } from '../is/isEmpty';
import { setPath } from './set';
import { diff } from './diff';
import { paths } from './paths';
import { removePath } from './remove';
import { hasPath } from './has';

/**
 * Recursively merges new or changed fields into orig. Cannot remove fields
 *
 * @param orig      Original object
 * @param changed   Changed object
 */
export function merge(orig: Object | any[], changed: Object | any[]) {
  if (orig == changed) {
    return orig;
  }
  const newPaths = paths(changed);
  return reduce(
    (acc, path) => {
      const cVal = getPath(path, changed);
      const aVal = getPath(path, acc);
      if (cVal != aVal) {
        return setPath(path, getPath(path, changed), acc);
      } else {
        return acc;
      }
    },
    orig,
    newPaths
  );
}
