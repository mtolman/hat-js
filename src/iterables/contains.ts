/**
 * @module iterables:contains
 *
 */
import { curry } from '../fp/curry';
import { toIterableOrEmpty } from './toIterableOrEmpty';

export interface iContains {
  (value: any, iterable: Iterable<any>): boolean;
  (value: any): (iterable: Iterable<any>) => boolean;
}

/**
 * Returns true if the value is identical to an element in the iterable
 *
 * @kind function
 * @param {any} value The value to search for
 * @param {Iterable<any>} iterable The iterable to iterate over
 * @returns {boolean} whether or not the value was found
 */
export const contains: iContains = curry(function (value: any, iterable: Iterable<any>) {
  for (const val of toIterableOrEmpty(iterable)) {
    if (val === value) return true;
  }
  return false;
});
