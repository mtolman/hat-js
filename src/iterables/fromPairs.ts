/**
 * @module iterables:fromPairs
 *
 */
import { toIterableOrEmpty } from './toIterableOrEmpty';

/**
 * @type A type indicating a value is a string or a number (often used for typing keys)
 */
type StrOrNum = string | number;

/**
 * Takes an iterable of key value pairs and returns an object formed from those key-value pairs
 *
 * E.g.
 * [['a', 5], ['b', 6]] becomes [{a: 5, b: 6}]
 *
 * @generator
 * @kind function
 * @param pairs An iterable of key-value pairs
 * @returns {Object} An object
 */
export const fromPairs = function <T>(pairs: Iterable<[StrOrNum, T]>): {
  [key: string]: T;
  [key: number]: T;
} {
  const p = toIterableOrEmpty<[StrOrNum, T]>(pairs);
  let obj = {};
  for (const [key, val] of p) {
    obj = Object.assign(obj, { [key]: val });
  }
  return obj;
};
