/**
 * @module iterables:fillEnd
 *
 */
import { curry } from '../fp/curry';
import { fill } from './fill';

export interface iFillEnd {
  (end: number, value: any, iterable: Iterable<any>): Iterable<any>;
  (end: number, value: any): (iterable: Iterable<any>) => Iterable<any>;
  (end: number): (value: any, iterable: Iterable<any>) => Iterable<any>;
  (end: number): (value: any) => (iterable: Iterable<any>) => Iterable<any>;
}
/**
 * Replaces all values in an iterable with a specified value after a provided index
 *
 *
 * @generator
 * @kind function
 * @param {number} start The index to start replacing values
 * @param {value} value The value to fill the iterable with
 * @param {Iterable<any>} iterable The iterable to fill
 * @returns {Iterable<any>} The resulting iterable
 */
export const fillEnd: iFillEnd = curry(function (
  start: number,
  value: any,
  iterable: Iterable<any>
): Iterable<any> {
  return fill(start, Infinity, value, iterable);
});
