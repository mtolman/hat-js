/**
 * @module iterables:join
 *
 */
import { toIterableOrEmpty } from './toIterableOrEmpty';
import { curry } from '../fp/curry';
import { collectToArray } from './collectToArray';

export interface iJoin {
  (separator: string, iterable: Iterable<any>): string;
  (separator: string): (iterable: Iterable<any>) => string;
}

/**
 * Returns the elements of an iterable as a string joined by the separator
 *
 *
 * @kind function
 * @param {string} separator The string to join elements with
 * @param {Iterable<any>} iterable The iterable to iterate over
 * @returns {string} The resulting string
 */
export const join: iJoin = curry(function (separator: string, iterable: Iterable<any>): string {
  return collectToArray(toIterableOrEmpty(iterable)).join(separator);
});
