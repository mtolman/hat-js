/**
 * @module iterables:map
 *
 */
import { curry } from '../fp/curry';
import { toIterableOrEmpty } from './toIterableOrEmpty';

export interface iPrepend {
  (element: any, iterable: Iterable<any>): Iterable<any>;
  (element: any): (iterable: Iterable<any>) => Iterable<any>;
}

/**
 * Calls a function on each element of an iterable and returns an iterable of those results
 *
 *
 * @generator
 * @kind function
 * @param {any} element The element to prepend
 * @param {Iterable<any>} iterable The iterable to iterate over
 * @returns {Iterable<any>} The resulting iterable
 */
export const prepend: iPrepend = curry(function* (
  element: any,
  iterable: Iterable<any>
): Iterable<any> {
  yield element;
  yield* toIterableOrEmpty(iterable);
});
