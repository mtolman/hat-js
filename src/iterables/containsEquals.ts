/**
 * @module iterables:containsEquals
 *
 */
import { curry } from '../fp/curry';
import { toIterableOrEmpty } from './toIterableOrEmpty';
import { isEqual } from '../is/index';
import { iContains } from './contains';

/**
 * Returns true if the value is in the iterable by checking using isEqual
 *
 * @kind function
 * @param {any} value The value to search for
 * @param {Iterable<any>} iterable The iterable to iterate over
 * @returns {boolean} whether or not the value was found
 */
export const containsEquals: iContains = curry(function (
  value: any,
  iterable: Iterable<any>
): boolean {
  for (const val of toIterableOrEmpty(iterable)) {
    if (isEqual(val, value)) return true;
  }
  return false;
});
