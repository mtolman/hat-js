/**
 * @module iterables:find
 *
 */
import { curry } from '../fp/curry';
import { toIterableOrEmpty } from './toIterableOrEmpty';
import { first } from './first';
import { filter } from './filter';
import { Predicate } from '../commonTypes';

export interface iFind {
  (predicate: Predicate<any>, iterable: Iterable<any>): Iterable<any>;
  (predicate: Predicate<any>): (iterable: Iterable<any>) => Iterable<any>;
}

/**
 * Returns an iterable holding first element in an iterable for which a predicate returns true or an empty iterable if none is found
 *
 *
 * @generator
 * @kind function
 * @param {Predicate} predicate The predicate to test values with
 * @param {Iterable<any>} iterable The iterable to filter
 * @returns {Iterable<any>} An iterable of one element if a match is found or an empty iterable otherwise
 */
export const find: iFind = curry(function* (
  predicate: Predicate<any>,
  iterable: Iterable<any>
): Iterable<any> {
  for (const v of first(filter(predicate, toIterableOrEmpty(iterable)))) yield v;
});
