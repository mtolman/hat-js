/**
 * @module iterables:any
 *
 */

import { curry } from '../fp/curry';
import { toIterableOrEmpty } from './toIterableOrEmpty';
import { Predicate } from '../commonTypes';

export interface iAny {
  (predicate: Predicate<any>, iterable: Iterable<any>): boolean;
  (predicate: Predicate<any>): (iterable: Iterable<any>) => boolean;
}

/**
 * Applies a predicate to elements of an iterable and returns true if any element returns true
 * Will return true as soon as the predicate returns true for any element
 *
 * @kind function
 * @param predicate The predicate to execute for each element
 * @param iterable The iterable to iterate over
 * @returns {boolean} returns true if the predicate returns true for any elements of the iterable
 */
export const any: iAny = curry(function (
  predicate: Predicate<any>,
  iterable: Iterable<any>
): boolean {
  for (const val of toIterableOrEmpty(iterable)) {
    if (predicate(val)) return true;
  }
  return false;
});
