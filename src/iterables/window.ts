/**
 * @module iterables:window
 *
 */
import { curry } from '../fp/curry';

export interface WindowOptions {
  size?: number;
  incompleteWindows?: boolean;
  windowStep?: number;
}

export interface iWindowOperation {
  (opts: number | WindowOptions, iterable: Iterable<any>): Iterable<any[]>;
  (opts: number | WindowOptions): (iterable: Iterable<any>) => Iterable<any[]>;
}

/**
 * Returns an iterable of a sliding window of size n
 *
 * @generator
 * @kind function
 * @param {size|WindowOptions} size The size of the sliding window, or an object of more detailed options if desired
 * @param {Iterable<any>} iterable The iterable to do a sliding window over
 * @returns {Iterable<any[]>} An iterable of the sliding windows
 */
export const window: iWindowOperation = curry(function* (
  opts: number | WindowOptions,
  iterable: Iterable<any>
) {
  let finalOpts: WindowOptions = { size: 2, incompleteWindows: false, windowStep: 1 };
  if (typeof opts === 'number') {
    finalOpts.size = Math.max(1, opts);
  } else {
    finalOpts = { ...finalOpts, ...opts };
    finalOpts.size = Math.max(1, finalOpts.size);
    finalOpts.windowStep = Math.max(1, finalOpts.windowStep);
  }

  const curWindow = [];
  let stepsTaken = 0;
  for (const elem of iterable) {
    curWindow.length >= finalOpts.size && curWindow.shift();
    curWindow.push(elem);
    ++stepsTaken;
    if (stepsTaken >= finalOpts.windowStep) {
      stepsTaken = 0;
      if (curWindow.length == finalOpts.size || finalOpts.incompleteWindows) {
        yield [...curWindow];
      }
    }
  }
  if (finalOpts.incompleteWindows) {
    if (curWindow.length < finalOpts.size && stepsTaken == finalOpts.windowStep) {
      yield [...curWindow];
    }
    while (curWindow.length) {
      for (let i = stepsTaken; i < finalOpts.windowStep; ++i) {
        curWindow.shift();
      }
      stepsTaken = 0;
      if (curWindow.length) {
        yield [...curWindow];
      }
    }
  }
});
