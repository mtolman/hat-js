/**
 * @module iterables:first
 *
 */
import { head } from './head';

/**
 * Alias for head
 */
export const first = head;
