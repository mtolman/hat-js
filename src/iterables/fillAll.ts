/**
 * @module iterables:fillAll
 *
 */
import { curry } from '../fp/curry';
import { toIterableOrEmpty } from './toIterableOrEmpty';

export interface iFillAll {
  (value: any, iterable: Iterable<any>): Iterable<any>;
  (value: any): (iterable: Iterable<any>) => Iterable<any>;
}
/**
 * Fills all values of an iterable with a provided value
 *
 *
 * @generator
 * @kind function
 * @param {value} value The value to fill the iterable with
 * @param {Iterable<any>} iterable The iterable to fill
 * @returns {Iterable<any>} An iterable with it's values filled
 */
export const fillAll: iFillAll = curry(function* (
  value: any,
  iterable: Iterable<any>
): Iterable<any> {
  const iter = toIterableOrEmpty(iterable);
  for (const _ of iter) {
    yield value;
  }
});
