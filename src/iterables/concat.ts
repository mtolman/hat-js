/**
 * @module iterables:concat
 *
 */
import { curry } from '../fp/curry';
import { toIterableOrEmpty } from './toIterableOrEmpty';

export interface iConcat {
  (
    iterableLeft: Iterable<any>,
    iterableRight: Iterable<any>,
    ...moreIterables: Iterable<any>[]
  ): Iterable<any>;
  (iterableLeft: Iterable<any>, iterableRight: Iterable<any>): Iterable<any>;
  (iterableLeft: Iterable<any>): (
    iterableRight: Iterable<any>,
    ...moreIterables: Iterable<any>[]
  ) => Iterable<any>;
  (iterableLeft: Iterable<any>): (iterableRight: Iterable<any>) => Iterable<any>;
}

/**
 * Concatenates the provided iterables
 *
 * @generator
 * @kind function
 * @param {Iterable<any>} iterable1 The first iterable
 * @param {Iterable<any>} iterable2 The second iterable*
 * @param {...Iterable<any>} iterables Other iterables
 * @returns {Iterable<any>} the resulting concatenation of the provided iterables
 */
export const concat: iConcat = curry(function* (iterable1, iterable2, ...iterables) {
  for (const iterable of [iterable1, iterable2, ...iterables]) {
    yield* iterable;
  }
});
