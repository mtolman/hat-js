/**
 * @module iterables:tap
 *
 */
import { curry } from '../fp/curry';
import { toIterableOrEmpty } from './toIterableOrEmpty';

export interface iTap {
  (func: (val: any) => void, array: Iterable<any>): Iterable<any>;
  (func: (val: any) => void): (array: Iterable<any>) => Iterable<any>;
}

/**
 * Calls a function for each element of an iterable and then passes the elmenet on
 *
 * Often used for creating side effects (such as logging) while in the middle of processing an iterable
 *
 *
 * @generator
 * @kind function
 * @param {function} func Function to call on each element
 * @param {Iterable<any>} iterable The iterable to iterate over
 * @returns {Iterable<any>} An iterable with the values passed through
 */
export const tap: iTap = curry(function* (
  func: (any) => void,
  iterable: Iterable<any>
): Iterable<any> {
  const iter = toIterableOrEmpty(iterable);
  for (const val of iter) {
    func(val);
    yield val;
  }
});
