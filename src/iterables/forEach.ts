/**
 * @module iterables:forEach
 *
 */
import { curry } from '../fp/curry';
import { toIterableOrEmpty } from './toIterableOrEmpty';

export interface iForEach {
  (func: (val: any) => void, array: Iterable<any>): void;
  (func: (val: any) => void): (array: Iterable<any>) => void;
}

/**
 * Calls a function on each element of an iterable
 *
 * @kind function
 * @param {function} func The function to call for each element
 * @param {Iterable<any>} iterable The iterable to flatten
 * @returns {void} Does not return
 */
export const forEach = curry(function (func: (v: any) => void, iterable: Iterable<any>) {
  for (const val of toIterableOrEmpty(iterable)) {
    func(val);
  }
});
