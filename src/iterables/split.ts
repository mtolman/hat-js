import { curry } from '../fp/curry';
import { skip } from './skip';
import { toIterableOrEmpty } from './toIterableOrEmpty';
import { take } from './take';
import { collectToArray } from './collectToArray';

export interface iSplit {
  (n: number, iterable: Iterable<any>): [Iterable<any>, Iterable<any>];
  (n: number): (iterable: Iterable<any>) => [Iterable<any>, Iterable<any>];
}

/**
 * Splits an iterable into two iterables after n items
 */
export const split: iSplit = curry(function (
  n: number,
  iterable: Iterable<any>
): [Iterable<any>, Iterable<any>] {
  const iter = collectToArray(toIterableOrEmpty(iterable));
  return [take(n, iter), skip(n, iter)];
});
