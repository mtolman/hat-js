/**
 * @module iterables:scan
 *
 */
import { curry } from '../fp/curry';
import { toIterableOrEmpty } from './toIterableOrEmpty';

export interface iScan {
  (
    func: (accumulation: any, currentElement: any) => any,
    startAccumulator: any,
    iterable: Iterable<any>
  ): Iterable<any>;
  (func: (accumulation: any, currentElement: any) => any, startAccumulator: any): (
    iterable: Iterable<any>
  ) => Iterable<any>;
  (func: (accumulation: any, currentElement: any) => any): (
    startAccumulator: any,
    iterable: Iterable<any>
  ) => Iterable<any>;
  (func: (accumulation: any, currentElement: any) => any): (
    startAccumulator: any
  ) => (iterable: Iterable<any>) => Iterable<any>;
}

/**
 * Accumulates the values of an iterable by calling a function and returns all intermediate results
 *
 *
 * @generator
 * @kind function
 * @param {function} func The reducer function
 * @param {any} start The initial accumulation value
 * @param {Iterable<any>} iterable The iterable to iterate over
 * @returns {Iterable<any>} An iterable of all intermediate accumulations
 */
export const scan: iScan = curry(function* (
  func: (a: any, c: any) => any,
  start: any,
  iterable: Iterable<any>
): Iterable<any> {
  const iter = toIterableOrEmpty(iterable);
  let accumulated = start;
  for (const val of iter) {
    accumulated = func(accumulated, val);
    yield accumulated;
  }
});
