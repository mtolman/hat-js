/**
 * @module iterables:flatMap
 *
 */
import { curry } from '../fp/curry';
import { isIterable } from '../is/isIterable';
import { toIterableOrEmpty } from './toIterableOrEmpty';

export interface iFlatMap {
  (func: (v: any) => Iterable<any> | any, iterable: Iterable<any>): Iterable<any>;
  (func: (v: any) => Iterable<any> | any): (iterable: Iterable<any>) => Iterable<any>;
}

/**
 * Performs map(func, iterable) followed by flatten(iterable) on an iterable
 *
 * @generator
 *
 * @kind function
 * @param {function} func A function to apply to each element of the iterable
 * @param {Iterable<any>} iterable The iterable to iterate over
 * @returns {Iterable<any>} The resulting iterable
 */
export const flatMap: iFlatMap = curry(function* (
  func: (any) => Iterable<any>,
  iterable: Iterable<any>
): Iterable<any> {
  const iter = toIterableOrEmpty(iterable);
  for (const val of iter) {
    const newIterable = func(val);
    if (isIterable(newIterable)) {
      for (const newVal of newIterable) yield newVal;
    } else {
      yield newIterable;
    }
  }
});
