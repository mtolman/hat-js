/**
 * @module iterables:zip
 *
 */
import { curry } from '../fp/curry';
import { toIterableOrEmpty } from './toIterableOrEmpty';

export interface iZip {
  (
    iterableLeft: Iterable<any>,
    iterableRight: Iterable<any>,
    ...moreIterables: Iterable<any>[]
  ): Iterable<any[]>;
  (iterableLeft: Iterable<any>, iterableRight: Iterable<any>): Iterable<[any, any]>;
  (iterableLeft: Iterable<any>): (
    iterableRight: Iterable<any>,
    ...moreIterables: Iterable<any>[]
  ) => Iterable<any[]>;
  (iterableLeft: Iterable<any>): (iterableRight: Iterable<any>) => Iterable<[any, any]>;
}

/**
 * Takes the corresponding elements of each iterable based on index and groups them
 *
 * E.g.
 * `zip([1, 2, 3], ['a', 'b', 'c'], [4, 5, 6]) => [[1, 'a', 4], [2, 'b', 5], [3, 'c', 6]]`
 *
 *
 * @generator
 * @kind function
 * @param {Iterable<any>} iterable1 The first iterable to iterate over
 * @param {Iterable<any>} iterable2 The second iterable to iterate over
 * @param {...Iterable<any>} iterables Other iterables to iterate over
 * @returns {Iterable<any>} values from all provided iterables grouped by index
 */
export const zip: iZip = curry(function* (
  iterable1: Iterable<any>,
  iterable2: Iterable<any>,
  ...iterables: Iterable<any>[]
): Iterable<any> {
  const iterators = [iterable1, iterable2]
    .concat(iterables)
    .map(toIterableOrEmpty)
    .map((iterable) => iterable[Symbol.iterator]());

  while (true) {
    const next = iterators.map((iterator) => iterator.next());
    const items = next.map(({ value, done }) => (done ? null : value));
    if (next.reduce((acc, cur) => acc && cur.done, true)) return;
    yield items;
  }
});
