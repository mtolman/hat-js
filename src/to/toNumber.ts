/**
 * Coerces a value to a number
 * @param value
 */
export function toNumber(value: any): number {
  return +value;
}
