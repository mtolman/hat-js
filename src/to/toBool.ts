/**
 * Coerces a value to a boolean
 * @param value
 */
export function toBool(value: any): boolean {
  return !!value;
}
