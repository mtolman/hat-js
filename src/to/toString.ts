/**
 * Coerces a value to a string
 * @param value
 */
export function toString(value: any): string {
  return '' + value;
}
