/**
 * Coerces a value to an integer
 * @param value
 */
export function toInt(value: any): number {
  return value | 0;
}
